/***
 * This file contains constants of various settings used within the application.
 ***/

/*****************
 * Path Settings *
 *****************/
// Server URL
var API_URL = '//wantbus.api.dev/api/user/processForm'; //local
//var API_URL = '//staging.api.wantbus.com/api/user/processForm'; //staging
//var API_URL = '//staging.api.wantbus.com/api/customer-api/processForm'; //staging
//var API_URL = 'https://www.wantbus.com/api/user/processForm';  //live