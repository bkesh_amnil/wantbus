// Master Slider
var slider = new MasterSlider();
slider.setup('masterslider', {
    width: 800,
    autoHeight: true,
    speed: 20,
    preload: 0
});
slider.control('bullets', {autohide: false});

$("#section2-container > div > a").on('click', function(e) {
    e.preventDefault();
    var i = 0;
    var previndex = slider.api.index();

    // highlighting the buttons
    if ($(this).attr("id") == "step1") {
        i = 0;
        $(this).addClass("active");
    }
    else if ($(this).attr("id") == "step2") {
        i = 1;
        $(this).addClass("active");
    }
    else if ($(this).attr("id") == "step3") {
        i = 2;
        $(this).addClass("active");
    }
    else if ($(this).attr("id") == "step4") {
        i = 3;
        $(this).addClass("active");
    }

    slider.api.gotoSlide(i);

    // unhighlighting the buttons
    if (previndex == "0" && previndex != i) {
        $("#step1").removeClass("active");
    }
    else if (previndex == "1" && previndex != i) {
        $("#step2").removeClass("active");
    }
    else if (previndex == "2" && previndex != i) {
        $("#step3").removeClass("active");
    }
    else if (previndex == "3" && previndex != i) {
        $("#step4").removeClass("active");
    }

});


slider.api.addEventListener(MSSliderEvent.CHANGE_START, function() {
    // dispatches when the slider's current slide change starts.

    // first removing the class for each elements
    $("#section2-container > div > a").removeClass("active");

    // highlighting the buttons
    var i = slider.api.index();

    if (i == 0) {
        $("#step1").addClass("active");
    }
    else if (i == 1) {
        $("#step2").addClass("active");
    }
    else if (i == 2) {
        $("#step3").addClass("active");
    }
    else if (i == 3) {
        $("#step4").addClass("active");
    }

    // first removing the class for each elements
    $("#navigation-pins > a").removeClass("current-pin");
    // $("#pin1").removeClass("current-pin");
    // $("#pin2").removeClass("current-pin");
    // $("#pin3").removeClass("current-pin");
    // $("#pin4").removeClass("current-pin");
    $("#pin1 > img").attr('src', 'img/icons/icon-pin1-320.svg');
    $("#pin2 > img").attr('src', 'img/icons/icon-pin2-320.svg');
    $("#pin3 > img").attr('src', 'img/icons/icon-pin3-320.svg');
    $("#pin4 > img").attr('src', 'img/icons/icon-pin4-320.svg');

    $(".step-contents").removeClass("current");

    // highlighting the buttons
    var j = slider.api.index();

    if (j == 0) {
        $("#pin1").addClass("current-pin");
        $("#step1-contents").addClass("current");
    }
    else if (j == 1) {
        $("#pin2").addClass("current-pin");
        $("#step2-contents").addClass("current");
    }
    else if (j == 2) {
        $("#pin3").addClass("current-pin");
        $("#step3-contents").addClass("current");
    }
    else if (j == 3) {
        $("#pin4").addClass("current-pin");
        $("#step4-contents").addClass("current");
    }
});




//<!-- Stellar Js -->

//     "stellar" parallax --------------------------------------------------//

$(document).ready(function() {
    react_to_window();
});

//      only activate stellar for window widths above or equal to 1024
var stellarActivated = false;

$(window).resize(function() {
    react_to_window();
});

function react_to_window() {
    if ($(window).width() <= 1023) {
        if (stellarActivated == true) {
            $(window).data('plugin_stellar').destroy();
            stellarActivated = false;
        }
    } else {
        if (stellarActivated == false) {

            $.stellar({
                positionProperty: 'position',
                horizontalScrolling: false,
                responsive: false

            });

            $(window).data('plugin_stellar').init();
            stellarActivated = true;
        }
    }
}


// var height = $("#section1-banner").height();
// var total = height/2;
// console.log(total);
// $(window).scroll(function () {
//     var scroll = $(window).scrollTop();

//     if(scroll > total) {
//       $("#step1").removeClass("hide");
//     } else {
//       $("#step1").addClass("hide");
//     }
// });



// <Select2

$('select').select2({
    placeholder: "Type of Partnership*",
    allowClear: true
});

$("#type-of-partnership").select2({
    placeholder: "Type of Partnership*",
    allowClear: true
});



// Document Load function
$(window).load(function() {
    /*
     * Replace all SVG images with inline SVG
     */
    jQuery('img.svg').each(function() {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
});

// Owner Registration Form

$(document).ready(function() {
    //#routes not necessary as of now >>
//    var myhash = document.location.hash;
//    if(myhash =="#privacy-policy"){
//        $("#privacy-policy-modal").addClass("active");
//        $("body").css("overflow", "hidden");
//    }
//    if(myhash =="#terms-and-conditions"){
//        $("#terms-and-conditions-modal").addClass("active");
//        $("body").css("overflow", "hidden");
//    }
//    if(myhash =="#software-licenses"){
//        $("#software-licenses-modal").addClass("active");
//        $("body").css("overflow", "hidden");
//    }
    //#routes not necessary as of now <<

    //#submit reservation form
    $('body').on('submit', '#owner-registration-form form', function(e) {
        e.preventDefault();
        var _this = $(this);
        var output = _this.find('output');
        var partnership = _this.find('[name="partnerships"]').val();
        var first_name = _this.find('[name="first_name"]').val();
        var last_name = _this.find('[name="last_name"]').val();
        var mobile = _this.find('[name="mobile"]').val();
        var email = _this.find('[name="email"]').val();
        var company = _this.find('[name="company"]').val();
        var designation = _this.find('[name="designation"]').val();
        var designation = _this.find('[name="designation"]').val();
        var website = _this.find('[name="website"]').val();
        var err = 0;

        function showErrSpan(outputClass, msg) {
            _this.find('span.' + outputClass).text(msg).show();
            err = 1;
        }
        function isNumber(str) {
            if (typeof str !== 'number' && typeof str !== 'string') {
                return false;
            }

            return !isNaN(parseFloat(str)) && isFinite(str);

            // jQuery
            //return (str - parseFloat(str) + 1) >= 0;
        }
        function isPhone(str) {
            return isNumber(str) && str.length === 8;
        }
        function isEmail(str) {
            if (typeof str !== 'string') {
                return false;
            }

            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(str);
        } //isEmail()
        if (partnership === '') {
            showErrSpan('partnership', 'Please Select A Partnership');
        }
        if (first_name === '') {
            showErrSpan('first_name', 'First Name required');
        }
        if (last_name === '') {
            showErrSpan('last_name', 'Last Name required');
        }
        if (mobile === '') {
            showErrSpan('mobile', 'Mobile Number required');
        } else if (!isPhone(mobile) || (!mobile.startsWith('9') && !mobile.startsWith('8'))) {
            showErrSpan('mobile', 'Invalid Mobile Number');
        }

        if (email === '') {
            showErrSpan('email', 'Email Address required');
        } else
        if (!isEmail(email)) {
            showErrSpan('email', 'Invalid Email Address');
        }
        if (err === 1) {
            return false;
        }
        var formData = _this.serialize();
        $.ajax({
            type: "POST",
            url: API_URL,
//            url: "//staging.api.wantbus.com/api/user/processForm",
//            url: "http://wantbus.api.dev/api/user/processForm",

            data: formData,
            dataType: 'json',
            success: function(data) {
                $('#section3-owner-registration').addClass('success');
                $('#user-form').hide();
                $('#success-msg').css('display', 'inline-block');
                _this.find('input:text').val('');
                _this.find('select').val('');
            },
            error: function(data) {
                $('#section3-owner-registration').addClass('server-error');
                $('#user-form').hide();
                $('#server-error-msg').css('display', 'inline-block');
                _this.find('input:text').val('');
                _this.find('select').val('');
            }
        });
    });
    $('body').on('click', 'span.error', function() {
        $(this).hide().prev('input').focus();
    });
    $('body').on('focus', 'input', function() {
        $(this).next('span.error').hide();
    });
    $('body').on('click', '#form-success-button', function() {
        $('#section3-owner-registration').removeClass('success');
        $('#user-form').show();
        $('#success-msg').css('display', '');
    });
    $('body').on('click', '#form-success-button', function() {
        $('#section3-owner-registration').removeClass('server-error');
        $('#user-form').show();
        $('#server-error-msg').css('display', '');
    });
    //$('#section4-app-download')

});

//Change opacity on scroll START 
var header = $('#title-text');
var range = 400;

$(window).on('scroll', function() {
    if ($(window).width() > 1024) {
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height / 2;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({'opacity': calc});

        //console.log(calc);

        if (calc > '1') {
            header.css({'opacity': 1});
        } else if (calc < '0') {
            header.css({'opacity': 0});
        }
    }
});
//Change opacity on scroll END 

// Popup


//$("#copyright").click(function (e) {
//    e.preventDefault();    
//    $("#copyright-modal").addClass("active");
//    $("body").css("overflow", "hidden");
//});

$("#terms-and-conditions").click(function(e) {
    e.preventDefault();
    document.location = "#terms-and-conditions";
    $("#terms-and-conditions-modal").addClass("active");
    $("body").css("overflow", "hidden");
});
$("#privacy-policy").click(function(e) {
    e.preventDefault();
    document.location = "#privacy-policy";
    $("#privacy-policy-modal").addClass("active");
    $("body").css("overflow", "hidden");
});
$("#software-licenses").click(function(e) {
    e.preventDefault();
    document.location = "#software-licenses";
    $("#software-licenses-modal").addClass("active");
    $("body").css("overflow", "hidden");
});

//$("#google-map-licenses").click(function (e) {
//    e.preventDefault();    
//    $("#google-map-licenses-modal").addClass("active");
//    $("body").css("overflow", "hidden");
//});


$(".footer-modal-close").click(function() {
    $("body").css("overflow", "auto");
//    if($("#copyright-modal").hasClass("active")) {
//        $("#copyright-modal").removeClass("active");
//    }
    if ($("#terms-and-conditions-modal").hasClass("active")) {
        $("#terms-and-conditions-modal").removeClass("active");
    }
    if ($("#privacy-policy-modal").hasClass("active")) {
        $("#privacy-policy-modal").removeClass("active");
    }
    if ($("#software-licenses-modal").hasClass("active")) {
        $("#software-licenses-modal").removeClass("active");
    }

//    if($("#google-map-licenses-modal").hasClass("active")) {
//        $("#google-map-licenses-modal").removeClass("active");
//    }
});



// Get the App Scroll

// $("#get-the-app").on("click", function() {
//     $("body").scrollTop(2000);
// });

$(function() {
    $('a[href*=#]').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: $($(this).attr('href')).offset().top}, 1000);
    });
});


// Small Screen 320px

$("#pin1").click(function() {

    if (!($("#pin1").hasClass("current-pin"))) {

        $("#navigation-pins a").removeClass("current-pin");
        $(".step-contents").removeClass("current");
        $("#pin1").addClass("current-pin");
        slider.api.gotoSlide(0);
    }

});

$("#pin2").click(function() {
    if (!($("#pin2").hasClass("current-pin"))) {

        $("#navigation-pins a").removeClass("current-pin");
        $(".step-contents").removeClass("current");
        $("#pin2").addClass("current-pin");
        $("#step2-contents").addClass("current");
        slider.api.gotoSlide(1);
    }

});

$("#pin3").click(function() {
    if (!($("#pin3").hasClass("current-pin"))) {

        $("#navigation-pins a").removeClass("current-pin");
        $(".step-contents").removeClass("current");
        $("#pin3").addClass("current-pin");
        $("#step3-contents").addClass("current");
        slider.api.gotoSlide(2);
    }

});

$("#pin4").click(function() {
    if (!($("#pin4").hasClass("current-pin"))) {

        $("#navigation-pins a").removeClass("current-pin");
        $(".step-contents").removeClass("current");
        $("#pin4").addClass("current-pin");
        $("#step4-contents").addClass("current");
        slider.api.gotoSlide(3);
    }

});
