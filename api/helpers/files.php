<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Helper functions for files
 */

/**
 * Creates a zip file from a file / directory, e.g. zip("/folder/to/compress/", "./compressed.zip") 
 * @param: {string} file/folder path of source
 *         {string} filepath of output file
 * @return: {bool} success
 */
function zip($source, $destination) {
	// Check source exists
	if (!file_exists($source)) {
		return FALSE;
	}

	// Create zip
	$zip = new ZipArchive();
	if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
		return FALSE;
	}

	$source = str_replace('\\', '/', realpath($source));
	if (is_dir($source) === true) {
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

		foreach ($files as $file) {
			$file = str_replace('\\', '/', $file);

			//ignore "." and ".." folders
			if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) ) {
				continue;
			}
			
			//Add files
			$file = realpath($file);
			if (is_dir($file) === true) {
				$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
			}
			else if (is_file($file) === true) {
				$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
			}
		}
	} //source is directory
	else if (is_file($source) === true) {
		$zip->addFromString(basename($source), file_get_contents($source));
	} //source is file

	return $zip->close();
} //zip()

/**
 * Creates a file at a path, creating any necessary directories as it goes along
 * - Note: uses exclusive lock during file writing
 * @param: {string} file_path, e.g. "/var/www/www.example.com/humans.txt"
 *         {string} contents, e.g. "Hello World!"
 *         {int} permissions, e.g. 0770
 */
function createFileRecursive($file_path, $contents, $permissions=0770) {
	$dir_path = dirname($file_path);
	if (!file_exists($dir_path)) {
		if (!mkdir($dir_path, $permissions, TRUE)) {
			throw new Exception('createFileRecursive() - Unable to create dir: '. $dir_path);
		}
	}
	
	if (!file_put_contents($file_path, $contents, LOCK_EX)) {
		throw new Exception('createFileRecursive() - unable to create file: '. $file_path);
	}
} //createFileRecursive()

/**
 * Scans a directory for files.
 * @param: {string} path - directory to scan
 *         {string | array} - CSV or array of extensions
 * @return: {array} of paths
 */
function scanDirRecursive($path, $extensions=NULL) {
	if (!is_dir($path)) {
		throw new Exception('scanDirRecursive() - Path must be a directory');
	}
	if (isset($extensions)) {
		if (is_string($extensions)) {
			$extensions = csv2array($extensions);
		}
		else if (is_array($extensions)) {
			foreach ($extensions as $ext) {
				if (!is_string($ext)) {
					throw new Exception('scanDirRecursive() - Extensions must contain only strings');
				}
			}
		}
		else {
			throw new Exception('scanDirRecursive() - Extensions must be an array or string');
		}
	}

	$files = array();

	// Iterate through dir
	$directory = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS);
	$iterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::LEAVES_ONLY);
	foreach ($iterator as $fileinfo) {
		if (isset($extensions)) {
			if (in_array($fileinfo->getExtension(), $extensions)) {
				$files[] = $fileinfo->getPathname();
			}
		} // need to check extension
		else {
			$files[] = $fileinfo->getPathname();
		}
	}

	return $files;
} //scanDirRecursive()

/**
 * Deletes a directory and all its contents, including sub-directories
 * @param: {string} path - directory to delete
 * - Note: With great power comes great responsibility
 */
function removeDirRecursive($path) {
	if (!is_dir($path)) {
		throw new Exception('removeDirRecursive() - Path must be a directory');
	}

	$directory = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS);
	$iterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::CHILD_FIRST);
	foreach ($iterator as $fileinfo) {
		if ($fileinfo->isDir()) {
			if (!rmdir($fileinfo->getRealPath())) {
				throw new Exception('removeDirRecursive() - Unable to remove directory ' . $fileinfo->getRealPath());
			}
		}
		else {
			if (!unlink($fileinfo->getRealPath())) {
				throw new Exception('removeDirRecursive() - Unable to unlink file ' . $fileinfo->getRealPath());	        	
			}
		}
	}

	if (!rmdir($path)) {
		throw new Exception('removeDirRecursive() - Unable to remove base directory ' . $path);
	}
} // removeDirRecursive()
