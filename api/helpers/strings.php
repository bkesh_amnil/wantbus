<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Helper functions for strings.
 * - Contains only pure functions
 */

/**
 * Makes HTML output safe, i.e.
 *     & (ampersand) becomes &amp;
 *     " (double quote) becomes &quot;
 *     ' (single quote) becomes &#x27; (or &apos;)
 *     < (less than) becomes &lt;
 *     > (greater than) becomes &gt;
 *     / (slash) becomes &#x2F
 * @param: {string} original string
 * @return: {string}
 */
function escapeHTML($a) {
	if (!isset($a) || !is_string($a)) {
		throw new Exception('escapeHTML() - param must be string. Got ' . gettype($a));
	}

	return str_replace('/', "&#x2F;", str_replace("'", "&#x27;", 
		htmlspecialchars($a, ENT_COMPAT, 'UTF-8')));
} //escapeHTML()

/**
 * Makes filename write-safe by stripping illegal characters e.g. < > : \ / | ?
 * @param: {string} original string
 * @return: {string}
 */
function escapeFileName($n) {
	if (!isset($n) || !is_string($n)) {
		throw new Exception('escapeFileName() - param must be string. Got ' . gettype($n));
	}

	$bad = array_merge(
        array_map('chr', range(0,31)),
        array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));
	return str_replace($bad, "", $n);
} //escapeFileName()

/**
 * Converts any non-alphanumeral / non-underscore / non-dot to dash, and ensures file does not begin/end with dash
 * - Note: output is a lower-case ASCII string
 * @param (string)
 * @return (string)
 */
function encodeFileName($n) {
	if (!isset($n) || !is_string($n)) {
		throw new Exception('encodeFileName() - param must be string. Got ' . gettype($n));
	}

	$n = utf82ascii($n);
	$n = preg_replace('/[^0-9a-zA-Z_\.]/', '-', $n);
	$n = preg_replace('/-+/', '-', $n);
	return strtolower(trim($n, '-'));

} //encodeFileName()

/**
 * Makes an input string camelCase:
 *  - alphanumeric
 *  - starts with lower alphabetical letter (preceding numerals are removed)
 *  - remaining letters in each word is left intact
 * @param: {string} original string
 * @return: {string}
 */
function formatCamelCase($s) {
	if (!isset($s) || !is_string($s)) {
		throw new Exception('formatCamelCase() - param must be string. Got ' . gettype($s));
	}

	// Split by non-alphanumeral
	$tmp = preg_split('/[^a-zA-Z0-9]+/', $s, NULL, PREG_SPLIT_NO_EMPTY);

	// Make each alphanumeral uppercase
	$toRet = '';
	foreach ($tmp as $i) {
		$toRet .= ucfirst($i);
	}
	
	// Remove preceding numerals
	$toRet = ltrim($toRet, '0..9');
	
	// Make first letter lowecase
	return lcfirst($toRet);
} //formatCamelCase()

/**
 * Makes an input string PascalCase:
 *  - alphanumeric
 *  - starts with upper alphabetical letter (preceding numerals are removed)
 *  - remaining letters in each word is left intact
 * @param: {string} original string
 * @return: {string}
 */
function formatPascalCase($s) {
	if (!isset($s) || !is_string($s)) {
		throw new Exception('formatPascalCase() - param must be string. Got ' . gettype($s));
	}

	// Split by non-alphanumeral
	$tmp = preg_split('/[^a-zA-Z0-9]+/', $s, NULL, PREG_SPLIT_NO_EMPTY);
	
	// Make each alphanumeral uppercase
	$toRet = '';
	foreach ($tmp as $i) {
		$toRet .= ucfirst($i);
	}
	
	// Remove preceding numerals
	return ltrim($toRet, '0..9');
} //formatPascalCase()

/**
 * Strips input of all non-numeric chars and return it as an unsigned int
 * - Note: empty strings or strings containing no digits return 0.
 * @param: {string}
 * @return: {int} unsigned int
 */
function str2uint($a) {
	if (!isset($a) || !is_string($a)) {
		throw new Exception('str2uint() - param must be string. Got ' . gettype($a));
	}
	
	return (int) preg_replace('/[^0-9.]+/', '', $a);
} //str2uint()

/**
 * Converts CSV strings into array.
 * Better than native "str_getcsv" in compliance RFC-4180, and better distinction between empty and non-strings.
 * - Note: returns empty array if input is an empty string input
 * - Note: does not fully conform to RFC-4180 (https://tools.ietf.org/html/rfc4180), which allows multiple records
 *         this function only parses a CSV and returns a one-dimensional array, i.e. only works on one record
 * @param: {string} data - CSV string
 *         {object} options (optional)
 *                  - {string} delimiter (defaults to comma. Must be at least 1 char)
 *                  - {string} quote (defaults to ". Must be at least 1 char)
 *                  - {boolean} trim (defaults to true. Operates on individual result)
 * @return {array} indexed array containing the fields read. 
 */
function csv2array($data, $options=array()) {
	if (!isset($data) || !is_string($data)) {
		throw new Exception('csv2array() - data must be string. Got ' . gettype($data));
	}

	if (!is_array($options)) {
		$options = array(
			'delimiter' => ',',
			'quote' => '"',
			'trim' => TRUE
		);
	}
	if (!isset($options['delimiter']) || !is_string($options['delimiter']) || mb_strlen($options['delimiter']) < 1) {
		$options['delimiter'] = ',';
	}
	if (!isset($options['quote']) || !is_string($options['quote']) || mb_strlen($options['quote']) < 1) {
		$options['quote'] = '"';
	}
	if (!isset($options['trim']) || !is_bool($options['trim'])) {
		$options['trim'] = TRUE;
	}

	// Empty string input should produce one empty string element
	if ($data === '') {
		return array('');
	}

	$quote_len = mb_strlen($options['quote']);
	$delimiter_len = mb_strlen($options['delimiter']);

	$result = array(); // result to return
	$value = ''; // current value parsed so far
	$inside_enclosure = FALSE;
	$expect_delimiter_or_eof = FALSE; // after exiting quotes, we only expect delimiter or EOF

	// Go through each character
	$i = 0;
	$len = mb_strlen($data);
	while ($i < $len) {
		if (mb_substr($data, $i, $quote_len) === $options['quote']) {
			if ($inside_enclosure) {
				if (mb_substr($data, $i+$quote_len, $quote_len) === $options['quote']) {
					$value .= $options['quote'];
					$i += $quote_len;
				} // escaped quote
				else {
					$inside_enclosure = FALSE;
					$expect_delimiter_or_eof = TRUE;
				} // terminate enclosure
			} // inside enclosure
			else {
				if (mb_strlen($value) > 0) {
					throw new Exception('csv2array() - data is not valid csv. Quotes cannot appear here: char ' . ($i+1));
				} // check that no characters appear before/after quotes
				else {
					$inside_enclosure = TRUE;
				} // start enclosure
			} // not in enclosure

			$i += $quote_len;
		} // Case: quote
		else if (mb_substr($data, $i, $delimiter_len) === $options['delimiter']) {
			if ($inside_enclosure) {
				$value .= $options['delimiter'];
			} // inside enclosure
			else {
				// push this value into result array
				if ($options['trim']) {
					$result[] = trim($value);
				}
				else {
					$result[] = $value;
				}

				// reset to original state
				$value = '';
				$expect_delimiter_or_eof = FALSE;
			} // not in enclosure

			$i += $delimiter_len;
		} // Case: delimiter
		else {
			$c = mb_substr($data, $i, 1);
			if ($expect_delimiter_or_eof) {
				if (!$options['trim'] || $c !== ' ') {
					throw new Exception('csv2array() - data is not a csv (Unexpected data after double-quotes) char ' . ($i+1));
				}
			}
			$value .= $c;

			$i++;
		} // Case: regular char
	} // while we still have chars

	// check that quotes are closed
	if ($inside_enclosure) {
		throw new Exception('csv2array() - data is not valid csv. Unclosed quote detected.');
	}
	
	// push last value into result array
	if ($options['trim']) {
		$value = trim($value);
	}
	$result[] = $value;
	
	return $result;
} //csv2array()

/**
 * Parse a CSV string into a 2D array of strings.
 * conform to RFC-4180 (https://tools.ietf.org/html/rfc4180), which allows multiple records (each a CSV)
 * - Note: returns an array of empty array if input is an empty string input
 * - Note: records with length different from header will be ignored.
 * @param: {string} data,
 *         {object} options (optional)
 *                  - {string} delimiter (defaults to comma. Must be 1 char)
 *                  - {string} lineEnding (defaults to \n. Must be 1 or 2 chars)
 *                  - {string} quote {defaults to ". Must be 1 char)
 *                  - {boolean} parseHeader (defaults to true, i.e. 1st row is returned as an array at index 0.)
 *                  - {boolean} trim (defaults to true)
 * @return: {2D Array of strings}
 */
function csv2multiarrays($data, $options=array()) {
	if (!isset($data) || !is_string($data)) {
		throw new Exception('csv2multiarrays() - data must be string. Got ' . gettype($data));
	}

	if (!is_array($options)) {
		$options = array(
			'delimiter' => ',',
			'quote' => '"',
			'trim' => TRUE
		);
	}
	if (!isset($options['delimiter']) || !is_string($options['delimiter']) || mb_strlen($options['delimiter']) < 1) {
		$options['delimiter'] = ',';
	}
	if (!isset($options['quote']) || !is_string($options['quote']) || mb_strlen($options['quote']) < 1) {
		$options['quote'] = '"';
	}
	if (!isset($options['lineEnding']) || !is_string($options['lineEnding']) || mb_strlen($options['lineEnding']) < 1) {
		$options['lineEnding'] = "\n";
	}
	if (!isset($options['trim']) || !is_bool($options['trim'])) {
		$options['trim'] = TRUE;
	}
	if (!isset($options['parseHeader']) || !is_bool($options['parseHeader'])) {
		$options['parseHeader'] = TRUE;
	}

	// Empty string input should produce one empty string element
	if ($data === '') {
		return array(array(''));
	}

	// Cache lengths
	$quote_len = mb_strlen($options['quote']);
	$delimiter_len = mb_strlen($options['delimiter']);
	$line_ending_len = mb_strlen($options['lineEnding']);

	$result = array(array(''));
	$row = 0;
	$col = 0;
	$inside_enclosure = FALSE;
	$expect_delimiter_or_eof = FALSE;
		
	// Go through each character
	$i=0;
	$len=mb_strlen($data); // tmp variables for loops
	while ($i<$len) {
		if (mb_substr($data, $i, $quote_len) === $options['quote']) {
			if ($inside_enclosure) {
				if (mb_substr($data, $i+$quote_len, $quote_len) === $options['quote']) {
					$result[$row][$col] .= $options['quote'];
					$i += $quote_len;
				} // escaped quote
				else {
					$inside_enclosure = FALSE;
					$expect_delimiter_or_eof = TRUE;
				} // terminate enclosure
			} // inside enclosure
			else {
				if (mb_strlen($result[$row][$col]) > 0) {
					throw new Exception('csv2multiarray() - data is not valid csv. Quotes cannot appear here: Line ' . ($row+1) . ', char ' . ($i+1)); 
				} // check that no characters appear before/after quotes
				else {
					$inside_enclosure = true;
				} // start enclosure
			} // not in enclosure

			$i += $quote_len;
		} // Case: quote
		else if (mb_substr($data, $i, $delimiter_len) === $options['delimiter']) {
			if ($inside_enclosure) {
				$result[$row][$col] .= $options['delimiter'];
			} // inside enclosure
			else {
				if ($options['trim']) {
					$result[$row][$col] = trim($result[$row][$col]);
				}

				// Go to next word
				$col++;
				$result[$row][$col] = '';
				$expect_delimiter_or_eof = FALSE;
			} // not in enclosure

			$i += $delimiter_len;
		} // Case: delimiter
		else if (mb_substr($data, $i, $line_ending_len) === $options['lineEnding']) {
			if ($inside_enclosure) {
				$result[$row][$col] .= $options['lineEnding'];
			} // inside enclosure
			else {
				if ($options['trim']) {
					$result[$row][$col] = trim($result[$row][$col]);
				}
				$row++;
				$col = 0;
				$result[$row] = array('');
				$expect_delimiter_or_eof = FALSE;
			} // not in enclosure

			$i += $line_ending_len;
		} // Case: line ending
		else {
			if ($expect_delimiter_or_eof) {
				throw new Exception('csv2multiarray() - data is not valid csv. Expecting delimiter at char ' . $i . ' (row: ' . $row . ', col: ' . $col);
			}
			$result[$row][$col] .= mb_substr($data, $i, 1);

			$i++;
		} // Case: regular char
	} // while there are more chars
	
	// check that quotes are closed
	if ($inside_enclosure) {
		throw new Exception('csv2multiarray() - data is not valid csv. Unclosed quote detected.');
	}
	
	// Last item may not have been trimmed
	if ($options['trim']) {
		$result[$row][$col] = trim($result[$row][$col]);
	}
	
	if (!$options['parseHeader']) {
		$result = array_shift($result);
	} // no headers necessary
	else {
		// Check that each row contains the same number of fields
		for ($i=1; $i<count($result); $i++) { // note: cannot cache length
			if (count($result[$i]) === count($result[0])) {
				continue;
			}

			// Remove this row
			array_splice($result, $i, 1);
			$i--; // compensate index due to splice
		}
	} // header is present

	return $result;
} //csv2multiarrays()

/**
 * Turns an array into a CSV string
 * - Note: 2D arrays with records of consistent, positive length will be result in a multi-line CSV.
 * - Note: elements are auto-casted to string
 *         integers --> string representation, e.g. "46"
 *         floats   --> exponential notation (4.1E+6)
 *         booleans --> "1" or "",
 *         arrays   --> "Array",
 *         objects   --> "Object"
 * @param: {array} arr
 *         {object} options (optional)
 *                  - {string} delimiter (defaults to comma. Must be 1 char)
 *                  - {string} lineEnding (defaults to \n. Must be 1 or 2 chars)
 *                  - {string} quote {defaults to ". Must be 1 char)
 *                  - {boolean} parseHeader (defaults to true, i.e. 1st row is returned as an array at index 0.)
 *                  - {boolean} trim (defaults to true)
 * @return: {string} CSV
 */
function array2csv($arr, $options=array()) {
	if (!isset($arr) || !is_array($arr)) {
		throw new Exception('array2csv() - data must be array. Got ' . gettype($arr));
	}

	// Check options
	if (!is_array($options)) {
		$options = array();
	}
	if (!isset($options['delimiter']) || !is_string($options['delimiter']) || mb_strlen($options['delimiter']) < 1) {
		$options['delimiter'] = ',';
	}
	if (!isset($options['quote']) || !is_string($options['quote']) || mb_strlen($options['quote']) < 1) {
		$options['quote'] = '"';
	}
	if (!isset($options['lineEnding']) || !is_string($options['lineEnding']) || mb_strlen($options['lineEnding']) < 1) {
		$options['lineEnding'] = "\n";
	}
	if (!isset($options['parseHeader']) || !is_bool($options['parseHeader'])) {
		$options['parseHeader'] = TRUE;
	}

	// Trivial case
	if (empty($arr)) {
		return '';
	}
	$result = '';

	// Check if we are dealing with a 2D array
	$is_multi_array = TRUE;
	if (!isset($arr[0]) || !is_array($arr[0])) {
		$is_multi_array = FALSE;
	} // check that header row exists
	if ($is_multi_array) {
		$size_header = count($arr[0]);
		foreach ($arr as $key => $val) {
			$size = count($val);
			if (!is_array($val) || $size !== $size_header || $size === 0) {
				$is_multi_array = FALSE;
				break;
			}
		}
	} // check that each subsequent row is same size as header row
	if ($is_multi_array) {
		if (array_keys($arr) !== range(0, count($arr) - 1)) {
			$is_multi_array = FALSE;
		} // 2D arrays must be indexed array of arrays
	}

	// Handle 2D array
	if ($is_multi_array) {
		foreach ($arr as $idx => $val) {
			if ($idx === 0 && !$options['parseHeader']) {
				continue;
			}
			$result .= array2csv($val, $options) . $options['lineEnding'];
		}

		return rtrim($result, $options['lineEnding']);
	} // multi array

	foreach ($arr as $key => $data) {
		$data = (string) $data; // may generate E_NOTICE if it's an array
		if (mb_strpos($data, $options['delimiter']) !== FALSE || mb_strpos($data, $options['quote']) !== FALSE || mb_strpos($data, $options['lineEnding']) !== FALSE) {
			$result .=
				$options['quote'] . 
				str_replace($options['quote'], $options['quote'].$options['quote'], $data) . // escape quotes
				$options['quote'];
		} // values containing line endings, quotes and commas must be enclosed
		else {
			$result .= $data . $options['delimiter'];
		}
	}

	return rtrim($result, $options['delimiter']);
} //array2csv()

/**
 * Note: lightly unit-tested... use case is very very limited.
 * Converts an array to XML
 * @param: {array}
 *         {string} indentation - string to preprend at this level. Each recursion will add one indent char
 *         {string} indent char(s)
 *         {string} newline char(s)
 * @return: {string} of XML
 */
function array2xml($data, $curr_indent="", $indent="\t", $newline="\n") {
	if (!isset($data) || !is_array($data)) {
		throw new Exception('array2xml() - data must be array. Got ' . gettype($data));
	}
	$xml = '';
	foreach ($data as $key => $value) {
		if (is_array($value)) {
			$xml .= $curr_indent.'<'.$key.'>'.$newline;
			$xml .= array2xml($value, $curr_indent.$indent, $indent, $newline);
			$xml .= $curr_indent.'</'.$key.'>'.$newline;
		}
		else {
            $xml .= $curr_indent.'<'.$key.'>'.$value.'</'.$key.'>'.$newline;
		}
	}
	return $xml;
} //array2xml()

/**
 * Turns a string into a slug by replacing non-alphanumeric (English alphabet) characters with dash, or an English letter.
 * @param: {string} original string
 *         {string} separator (optional. DEFAULT: ,)
 * @return: {string | NULL}
 */
function slugify($string, $separator = '-') {
	if (!isset($string) || !is_string($string)) {
		throw new Exception('slugify() - data must be string. Got ' . gettype($string));
	}

	$string = mb_strtolower(utf82ascii($string)); // to ASCII, to lower-case
	$string = strtr($string, ' ', $separator); // spaces to string
	$string = preg_replace('/([^a-z0-9' . $separator . '])+/', '', $string); // remove non-word and non-separator chars
	while (mb_strstr($string, $separator.$separator)) { // collapse multiple separators
		$string = str_replace($separator.$separator, $separator, $string);
	}
	return trim($string, $separator);
} //slugify()

/**
 * Converts UTF-8 strings to ASCII by translating non-ASCII characters to the closest counterpart.
 * - If no suitable replacement exists, a character outside of ASCII range is mapped to underscore.
 * @param {string}
 * @return (string)
 */
function utf82ascii($n) {
	if (!isset($n) || !is_string($n)) {
		throw new Exception('utf82ascii() - data must be string. Got ' . gettype($n));
	}

/*
	$rules = array(
		// Numeric characters
		'¹' => 1, '²' => 2, '³' => 3,

		// Latin
		'°' => 0, 'æ' => 'ae', 'ǽ' => 'ae', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Å' => 'A', 'Ǻ' => 'A', 'Ă' => 'A', 'Ǎ' => 'A', 'Æ' => 'AE',
		'Ǽ' => 'AE', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'å' => 'a', 'ǻ' => 'a', 'ă' => 'a', 'ǎ' => 'a', 'ª' => 'a', '@' => 'at',
		'Ĉ' => 'C', 'Ċ' => 'C', 'ĉ' => 'c', 'ċ' => 'c', '©' => 'c', 'Ð' => 'Dj', 'Đ' => 'D', 'ð' => 'dj', 'đ' => 'd',
		'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ĕ' => 'e', 'ė' => 'e',
		'ƒ' => 'f', 'Ĝ' => 'G', 'Ġ' => 'G', 'ĝ' => 'g', 'ġ' => 'g', 'Ĥ' => 'H', 'Ħ' => 'H', 'ĥ' => 'h', 'ħ' => 'h',
		'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Ǐ' => 'I', 'Į' => 'I', 'Ĳ' => 'IJ', 'ì' => 'i', 'í' => 'i', 'î' => 'i',
		'ï' => 'i', 'ĩ' => 'i', 'ĭ' => 'i', 'ǐ' => 'i', 'į' => 'i', 'ĳ' => 'ij', 'Ĵ' => 'J', 'ĵ' => 'j', 'Ĺ' => 'L', 'Ľ' => 'L', 'Ŀ' => 'L', 'ĺ' => 'l',
		'ľ' => 'l', 'ŀ' => 'l', 'Ñ' => 'N', 'ñ' => 'n', 'ŉ' => 'n', 'Ò' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ō' => 'O', 'Ŏ' => 'O', 'Ǒ' => 'O', 'Ő' => 'O',
		'Ơ' => 'O', 'Ø' => 'O', 'Ǿ' => 'O', 'Œ' => 'OE', 'ò' => 'o', 'ô' => 'o', 'õ' => 'o', 'ō' => 'o', 'ŏ' => 'o', 'ǒ' => 'o', 'ő' => 'o', 'ơ' => 'o',
		'ø' => 'o', 'ǿ' => 'o', 'º' => 'o', 'œ' => 'oe', 'Ŕ' => 'R', 'Ŗ' => 'R', 'ŕ' => 'r', 'ŗ' => 'r', 'Ŝ' => 'S', 'Ș' => 'S', 'ŝ' => 's', 'ș' => 's',
		'ſ' => 's', 'Ţ' => 'T', 'Ț' => 'T', 'Ŧ' => 'T', 'Þ' => 'TH', 'ţ' => 't', 'ț' => 't', 'ŧ' => 't', 'þ' => 'th', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U',
		'Ũ' => 'U', 'Ŭ' => 'U', 'Ű' => 'U', 'Ų' => 'U', 'Ư' => 'U', 'Ǔ' => 'U', 'Ǖ' => 'U', 'Ǘ' => 'U', 'Ǚ' => 'U', 'Ǜ' => 'U', 'ù' => 'u', 'ú' => 'u',
		'û' => 'u', 'ũ' => 'u', 'ŭ' => 'u', 'ű' => 'u', 'ų' => 'u', 'ư' => 'u', 'ǔ' => 'u', 'ǖ' => 'u', 'ǘ' => 'u', 'ǚ' => 'u', 'ǜ' => 'u',
		'Ŵ' => 'W', 'ŵ' => 'w', 'Ý' => 'Y', 'Ÿ' => 'Y', 'Ŷ' => 'Y', 'ý' => 'y', 'ÿ' => 'y', 'ŷ' => 'y',
		// Russian
		'Ъ' => '', 'Ь' => '', 'А' => 'A', 'Б' => 'B', 'Ц' => 'C', 'Ч' => 'Ch', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'E', 'Э' => 'E', 'Ф' => 'F', 'Г' => 'G',
		'Х' => 'H', 'И' => 'I', 'Й' => 'J', 'Я' => 'Ja', 'Ю' => 'Ju', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R',
		'С' => 'S', 'Ш' => 'Sh', 'Щ' => 'Shch', 'Т' => 'T', 'У' => 'U', 'В' => 'V', 'Ы' => 'Y', 'З' => 'Z', 'Ж' => 'Zh', 'ъ' => '','ь' => '','а' => 'a',
		'б' => 'b','ц' => 'c','ч' => 'ch','д' => 'd','е' => 'e','ё' => 'e','э' => 'e','ф' => 'f','г' => 'g','х' => 'h','и' => 'i','й' => 'j','я' => 'ja',
		'ю' => 'ju','к' => 'k','л' => 'l','м' => 'm','н' => 'n','о' => 'o','п' => 'p','р' => 'r','с' => 's','ш' => 'sh','щ' => 'shch','т' => 't','у' => 'u',
		'в' => 'v','ы' => 'y','з' => 'z','ж' => 'zh',
		// German characters
		'Ä' => 'AE','Ö' => 'OE','Ü' => 'UE','ß' => 'ss','ä' => 'ae','ö' => 'oe','ü' => 'u',
		// Turkish characters
		'Ç' => 'C','Ğ' => 'G','İ' => 'I','Ş' => 'S','ç' => 'c','ğ' => 'g','ı' => 'i','ş' => 's',
		// Latvian
		'Ā' => 'A','Ē' => 'E','Ģ' => 'G','Ī' => 'I','Ķ' => 'K','Ļ' => 'L','Ņ' => 'N','Ū' => 'U','ā' => 'a','ē' => 'e','ģ' => 'g','ī' => 'i','ķ' => 'k',
		'ļ' => 'l','ņ' => 'n','ū' => 'u',
		// Ukrainian
		'Ґ' => 'G','І' => 'I','Ї' => 'Ji','Є' => 'Ye','ґ' => 'g','і' => 'i','ї' => 'ji','є' => 'ye',
		// Czech
		'Č' => 'C','Ď' => 'D','Ě' => 'E','Ň' => 'N','Ř' => 'R','Š' => 'S','Ť' => 'T','Ů' => 'U','Ž' => 'Z','č' => 'c','ď' => 'd','ě' => 'e','ň' => 'n',
		'ř' => 'r','š' => 's','ť' => 't','ů' => 'u','ž' => 'z',
		// Polish
		'Ą' => 'A','Ć' => 'C','Ę' => 'E','Ł' => 'L','Ń' => 'N','Ó' => 'O','Ś' => 'S','Ź' => 'Z','Ż' => 'Z','ą' => 'a','ć' => 'c','ę' => 'e','ł' => 'l',
		'ń' => 'n','ó' => 'o','ś' => 's','ź' => 'z','ż' => 'z',
		// Greek
		'Α' => 'A','Β' => 'B','Γ' => 'G','Δ' => 'D','Ε' => 'E','Ζ' => 'Z','Η' => 'E','Θ' => 'Th','Ι' => 'I','Κ' => 'K','Λ' => 'L','Μ' => 'M','Ν' => 'N',
		'Ξ' => 'X','Ο' => 'O','Π' => 'P','Ρ' => 'R','Σ' => 'S','Τ' => 'T','Υ' => 'Y','Φ' => 'Ph','Χ' => 'Ch','Ψ' => 'Ps','Ω' => 'O','Ϊ' => 'I','Ϋ' => 'Y',
		'ά' => 'a','έ' => 'e','ή' => 'e','ί' => 'i','ΰ' => 'Y','α' => 'a','β' => 'b','γ' => 'g','δ' => 'd','ε' => 'e','ζ' => 'z','η' => 'e','θ' => 'th',
		'ι' => 'i','κ' => 'k','λ' => 'l','μ' => 'm','ν' => 'n','ξ' => 'x','ο' => 'o','π' => 'p','ρ' => 'r','ς' => 's','σ' => 's','τ' => 't','υ' => 'y',
		'φ' => 'ph','χ' => 'ch','ψ' => 'ps','ω' => 'o','ϊ' => 'i','ϋ' => 'y','ό' => 'o','ύ' => 'y','ώ' => 'o','ϐ' => 'b','ϑ' => 'th','ϒ' => 'Y',
		// Arabic
		'أ' => 'a','ب' => 'b','ت' => 't','ث' => 'th','ج' => 'g','ح' => 'h','خ' => 'kh','د' => 'd','ذ' => 'th','ر' => 'r','ز' => 'z','س' => 's','ش' => 'sh',
		'ص' => 's','ض' => 'd','ط' => 't','ظ' => 'th','ع' => 'aa','غ' => 'gh','ف' => 'f','ق' => 'k','ك' => 'k','ل' => 'l','م' => 'm','ن' => 'n','ه' => 'h',
		'و' => 'o','ي' => 'y',
		// Vietnamese
		'ạ' => 'a','ả' => 'a','ầ' => 'a','ấ' => 'a','ậ' => 'a','ẩ' => 'a','ẫ' => 'a','ằ' => 'a','ắ' => 'a','ặ' => 'a','ẳ' => 'a','ẵ' => 'a','ẹ' => 'e',
		'ẻ' => 'e','ẽ' => 'e','ề' => 'e','ế' => 'e','ệ' => 'e','ể' => 'e','ễ' => 'e','ị' => 'i','ỉ' => 'i','ọ' => 'o','ỏ' => 'o','ồ' => 'o','ố' => 'o',
		'ộ' => 'o','ổ' => 'o','ỗ' => 'o','ờ' => 'o','ớ' => 'o','ợ' => 'o','ở' => 'o','ỡ' => 'o','ụ' => 'u','ủ' => 'u','ừ' => 'u','ứ' => 'u','ự' => 'u',
		'ử' => 'u','ữ' => 'u','ỳ' => 'y','ỵ' => 'y','ỷ' => 'y','ỹ' => 'y','Ạ' => 'A','Ả' => 'A','Ầ' => 'A','Ấ' => 'A','Ậ' => 'A','Ẩ' => 'A','Ẫ' => 'A',
		'Ằ' => 'A','Ắ' => 'A','Ặ' => 'A','Ẳ' => 'A','Ẵ' => 'A','Ẹ' => 'E','Ẻ' => 'E','Ẽ' => 'E','Ề' => 'E','Ế' => 'E','Ệ' => 'E','Ể' => 'E','Ễ' => 'E',
		'Ị' => 'I','Ỉ' => 'I','Ọ' => 'O','Ỏ' => 'O','Ồ' => 'O','Ố' => 'O','Ộ' => 'O','Ổ' => 'O','Ỗ' => 'O','Ờ' => 'O','Ớ' => 'O','Ợ' => 'O','Ở' => 'O',
		'Ỡ' => 'O','Ụ' => 'U','Ủ' => 'U','Ừ' => 'U','Ứ' => 'U','Ự' => 'U','Ử' => 'U','Ữ' => 'U','Ỳ' => 'Y','Ỵ' => 'Y','Ỷ' => 'Y','Ỹ' => 'Y',
	);
*/
	
	$transliteration = array(
		'Ĳ' => 'I', 'Ö' => 'O','Œ' => 'O','Ü' => 'U','ä' => 'a','æ' => 'a','ĳ' => 'i','ö' => 'o','œ' => 'o','ü' => 'u','ß' => 's','ſ' => 's',
		'À' => 'A','Á' => 'A','Â' => 'A','Ã' => 'A','Ä' => 'A','Å' => 'A','Æ' => 'A','Ā' => 'A','Ą' => 'A','Ă' => 'A','Ç' => 'C','Ć' => 'C',
		'Č' => 'C','Ĉ' => 'C','Ċ' => 'C','Ď' => 'D','Đ' => 'D','È' => 'E','É' => 'E','Ê' => 'E','Ë' => 'E','Ē' => 'E','Ę' => 'E','Ě' => 'E',
		'Ĕ' => 'E','Ė' => 'E','Ĝ' => 'G','Ğ' => 'G','Ġ' => 'G','Ģ' => 'G','Ĥ' => 'H','Ħ' => 'H','Ì' => 'I','Í' => 'I','Î' => 'I','Ï' => 'I',
		'Ī' => 'I','Ĩ' => 'I','Ĭ' => 'I','Į' => 'I','İ' => 'I','Ĵ' => 'J','Ķ' => 'K','Ľ' => 'K','Ĺ' => 'K','Ļ' => 'K','Ŀ' => 'K','Ł' => 'L',
		'Ñ' => 'N','Ń' => 'N','Ň' => 'N','Ņ' => 'N','Ŋ' => 'N','Ò' => 'O','Ó' => 'O','Ô' => 'O','Õ' => 'O','Ø' => 'O','Ō' => 'O','Ő' => 'O',
		'Ŏ' => 'O','Ŕ' => 'R','Ř' => 'R','Ŗ' => 'R','Ś' => 'S','Ş' => 'S','Ŝ' => 'S','Ș' => 'S','Š' => 'S','Ť' => 'T','Ţ' => 'T','Ŧ' => 'T',
		'Ț' => 'T','Ù' => 'U','Ú' => 'U','Û' => 'U','Ū' => 'U','Ů' => 'U','Ű' => 'U','Ŭ' => 'U','Ũ' => 'U','Ų' => 'U','Ŵ' => 'W','Ŷ' => 'Y',
		'Ÿ' => 'Y','Ý' => 'Y','Ź' => 'Z','Ż' => 'Z','Ž' => 'Z','à' => 'a','á' => 'a','â' => 'a','ã' => 'a','ā' => 'a','ą' => 'a','ă' => 'a',
		'å' => 'a','ç' => 'c','ć' => 'c','č' => 'c','ĉ' => 'c','ċ' => 'c','ď' => 'd','đ' => 'd','è' => 'e','é' => 'e','ê' => 'e','ë' => 'e',
		'ē' => 'e','ę' => 'e','ě' => 'e','ĕ' => 'e','ė' => 'e','ƒ' => 'f','ĝ' => 'g','ğ' => 'g','ġ' => 'g','ģ' => 'g','ĥ' => 'h','ħ' => 'h',
		'ì' => 'i','í' => 'i','î' => 'i','ï' => 'i','ī' => 'i','ĩ' => 'i','ĭ' => 'i','į' => 'i','ı' => 'i','ĵ' => 'j','ķ' => 'k','ĸ' => 'k',
		'ł' => 'l','ľ' => 'l','ĺ' => 'l','ļ' => 'l','ŀ' => 'l','ñ' => 'n','ń' => 'n','ň' => 'n','ņ' => 'n','ŉ' => 'n','ŋ' => 'n','ò' => 'o',
		'ó' => 'o','ô' => 'o','õ' => 'o','ø' => 'o','ō' => 'o','ő' => 'o','ŏ' => 'o','ŕ' => 'r','ř' => 'r','ŗ' => 'r','ś' => 's','š' => 's',
		'ť' => 't','ù' => 'u','ú' => 'u','û' => 'u','ū' => 'u','ů' => 'u','ű' => 'u','ŭ' => 'u','ũ' => 'u','ų' => 'u','ŵ' => 'w','ÿ' => 'y',
		'ý' => 'y','ŷ' => 'y','ż' => 'z','ź' => 'z','ž' => 'z','Α' => 'A','Ά' => 'A','Ἀ' => 'A','Ἁ' => 'A','Ἂ' => 'A','Ἃ' => 'A','Ἄ' => 'A',
		'Ἅ' => 'A','Ἆ' => 'A','Ἇ' => 'A','ᾈ' => 'A','ᾉ' => 'A','ᾊ' => 'A','ᾋ' => 'A','ᾌ' => 'A','ᾍ' => 'A','ᾎ' => 'A','ᾏ' => 'A','Ᾰ' => 'A',
		'Ᾱ' => 'A','Ὰ' => 'A','ᾼ' => 'A','Β' => 'B','Γ' => 'G','Δ' => 'D','Ε' => 'E','Έ' => 'E','Ἐ' => 'E','Ἑ' => 'E','Ἒ' => 'E','Ἓ' => 'E',
		'Ἔ' => 'E','Ἕ' => 'E','Ὲ' => 'E','Ζ' => 'Z','Η' => 'I','Ή' => 'I','Ἠ' => 'I','Ἡ' => 'I','Ἢ' => 'I','Ἣ' => 'I','Ἤ' => 'I','Ἥ' => 'I',
		'Ἦ' => 'I','Ἧ' => 'I','ᾘ' => 'I','ᾙ' => 'I','ᾚ' => 'I','ᾛ' => 'I','ᾜ' => 'I','ᾝ' => 'I','ᾞ' => 'I','ᾟ' => 'I','Ὴ' => 'I','ῌ' => 'I',
		'Θ' => 'T','Ι' => 'I','Ί' => 'I','Ϊ' => 'I','Ἰ' => 'I','Ἱ' => 'I','Ἲ' => 'I','Ἳ' => 'I','Ἴ' => 'I','Ἵ' => 'I','Ἶ' => 'I','Ἷ' => 'I',
		'Ῐ' => 'I','Ῑ' => 'I','Ὶ' => 'I','Κ' => 'K','Λ' => 'L','Μ' => 'M','Ν' => 'N','Ξ' => 'K','Ο' => 'O','Ό' => 'O','Ὀ' => 'O','Ὁ' => 'O',
		'Ὂ' => 'O','Ὃ' => 'O','Ὄ' => 'O','Ὅ' => 'O','Ὸ' => 'O','Π' => 'P','Ρ' => 'R','Ῥ' => 'R','Σ' => 'S','Τ' => 'T','Υ' => 'Y','Ύ' => 'Y',
		'Ϋ' => 'Y','Ὑ' => 'Y','Ὓ' => 'Y','Ὕ' => 'Y','Ὗ' => 'Y','Ῠ' => 'Y','Ῡ' => 'Y','Ὺ' => 'Y','Φ' => 'F','Χ' => 'X','Ψ' => 'P','Ω' => 'O',
		'Ώ' => 'O','Ὠ' => 'O','Ὡ' => 'O','Ὢ' => 'O','Ὣ' => 'O','Ὤ' => 'O','Ὥ' => 'O','Ὦ' => 'O','Ὧ' => 'O','ᾨ' => 'O','ᾩ' => 'O','ᾪ' => 'O',
		'ᾫ' => 'O','ᾬ' => 'O','ᾭ' => 'O','ᾮ' => 'O','ᾯ' => 'O','Ὼ' => 'O','ῼ' => 'O','α' => 'a','ά' => 'a','ἀ' => 'a','ἁ' => 'a','ἂ' => 'a',
		'ἃ' => 'a','ἄ' => 'a','ἅ' => 'a','ἆ' => 'a','ἇ' => 'a','ᾀ' => 'a','ᾁ' => 'a','ᾂ' => 'a','ᾃ' => 'a','ᾄ' => 'a','ᾅ' => 'a','ᾆ' => 'a',
		'ᾇ' => 'a','ὰ' => 'a','ᾰ' => 'a','ᾱ' => 'a','ᾲ' => 'a','ᾳ' => 'a','ᾴ' => 'a','ᾶ' => 'a','ᾷ' => 'a','β' => 'b','γ' => 'g','δ' => 'd',
		'ε' => 'e','έ' => 'e','ἐ' => 'e','ἑ' => 'e','ἒ' => 'e','ἓ' => 'e','ἔ' => 'e','ἕ' => 'e','ὲ' => 'e','ζ' => 'z','η' => 'i','ή' => 'i',
		'ἠ' => 'i','ἡ' => 'i','ἢ' => 'i','ἣ' => 'i','ἤ' => 'i','ἥ' => 'i','ἦ' => 'i','ἧ' => 'i','ᾐ' => 'i','ᾑ' => 'i','ᾒ' => 'i','ᾓ' => 'i',
		'ᾔ' => 'i','ᾕ' => 'i','ᾖ' => 'i','ᾗ' => 'i','ὴ' => 'i','ῂ' => 'i','ῃ' => 'i','ῄ' => 'i','ῆ' => 'i','ῇ' => 'i','θ' => 't','ι' => 'i',
		'ί' => 'i','ϊ' => 'i','ΐ' => 'i','ἰ' => 'i','ἱ' => 'i','ἲ' => 'i','ἳ' => 'i','ἴ' => 'i','ἵ' => 'i','ἶ' => 'i','ἷ' => 'i','ὶ' => 'i',
		'ῐ' => 'i','ῑ' => 'i','ῒ' => 'i','ῖ' => 'i','ῗ' => 'i','κ' => 'k','λ' => 'l','μ' => 'm','ν' => 'n','ξ' => 'k','ο' => 'o','ό' => 'o',
		'ὀ' => 'o','ὁ' => 'o','ὂ' => 'o','ὃ' => 'o','ὄ' => 'o','ὅ' => 'o','ὸ' => 'o','π' => 'p','ρ' => 'r','ῤ' => 'r','ῥ' => 'r','σ' => 's',
		'ς' => 's','τ' => 't','υ' => 'y','ύ' => 'y','ϋ' => 'y','ΰ' => 'y','ὐ' => 'y','ὑ' => 'y','ὒ' => 'y','ὓ' => 'y','ὔ' => 'y','ὕ' => 'y',
		'ὖ' => 'y','ὗ' => 'y','ὺ' => 'y','ῠ' => 'y','ῡ' => 'y','ῢ' => 'y','ῦ' => 'y','ῧ' => 'y','φ' => 'f','χ' => 'x','ψ' => 'p','ω' => 'o',
		'ώ' => 'o','ὠ' => 'o','ὡ' => 'o','ὢ' => 'o','ὣ' => 'o','ὤ' => 'o','ὥ' => 'o','ὦ' => 'o','ὧ' => 'o','ᾠ' => 'o','ᾡ' => 'o','ᾢ' => 'o',
		'ᾣ' => 'o','ᾤ' => 'o','ᾥ' => 'o','ᾦ' => 'o','ᾧ' => 'o','ὼ' => 'o','ῲ' => 'o','ῳ' => 'o','ῴ' => 'o','ῶ' => 'o','ῷ' => 'o','А' => 'A',
		'Б' => 'B','В' => 'V','Г' => 'G','Д' => 'D','Е' => 'E','Ё' => 'E','Ж' => 'Z','З' => 'Z','И' => 'I','Й' => 'I','К' => 'K','Л' => 'L',
		'М' => 'M','Н' => 'N','О' => 'O','П' => 'P','Р' => 'R','С' => 'S','Т' => 'T','У' => 'U','Ф' => 'F','Х' => 'K','Ц' => 'T','Ч' => 'C',
		'Ш' => 'S','Щ' => 'S','Ы' => 'Y','Э' => 'E','Ю' => 'Y','Я' => 'Y','а' => 'A','б' => 'B','в' => 'V','г' => 'G','д' => 'D','е' => 'E',
		'ё' => 'E','ж' => 'Z','з' => 'Z','и' => 'I','й' => 'I','к' => 'K','л' => 'L','м' => 'M','н' => 'N','о' => 'O','п' => 'P','р' => 'R',
		'с' => 'S','т' => 'T','у' => 'U','ф' => 'F','х' => 'K','ц' => 'T','ч' => 'C','ш' => 'S','щ' => 'S','ы' => 'Y','э' => 'E','ю' => 'Y',
		'я' => 'Y','ð' => 'd','Ð' => 'D','þ' => 't','Þ' => 'T','ა' => 'a','ბ' => 'b','გ' => 'g','დ' => 'd','ე' => 'e','ვ' => 'v','ზ' => 'z',
		'თ' => 't','ი' => 'i','კ' => 'k','ლ' => 'l','მ' => 'm','ნ' => 'n','ო' => 'o','პ' => 'p','ჟ' => 'z','რ' => 'r','ს' => 's','ტ' => 't',
		'უ' => 'u','ფ' => 'p','ქ' => 'k','ღ' => 'g','ყ' => 'q','შ' => 's','ჩ' => 'c','ც' => 't','ძ' => 'd','წ' => 't','ჭ' => 'c','ხ' => 'k',
		'ჯ' => 'j','ჰ' => 'h'
	);

	$n = strtr($n, $transliteration);
	return preg_replace('/[^\x01-\x7F]/','_',$n);
} // utf82ascii()

/**
 * Gets the first letter of every word
 * - Note: if a string is shorter than min length, an error will be thrown.
 * - Note: if a string has fewer words than min length, letters from the first words will be used.
 * - Note: if a string has more words than max length, the first and last words will be used.
 * @param: {string}
 *         {object} options (optional)
 *             - {string} delimiter - default: ' ' (SPACE)
 *             - {int} minLength - defualt: 0 (no min)
 *             - {int} maxLength - defualt: str.length (no max)
 * @return: {string}
 */
function getInitials($str, $options = array()) {
	if (!isset($str) || !is_string($str)) {
		throw new Exception('Error in getInitials() - param is not a string: ' . gettype($str));
	}


	// Check Options
	if (!is_array($options) || count($options) === 0) {
		$options = array(
			'delimiter' => ' '
		);
	}
	if (!isset($options['delimiter']) || !is_string($options['delimiter'])) {
		$options['delimiter'] = ' ';
	}
	$words = explode($options['delimiter'], $str);
	$letter_count = mb_strlen($str) - count($words) + 1;

	if (!isset($options['maxLength']) || !isUnsignedInt($options['maxLength']) || $options['maxLength'] > $letter_count) {
		$options['maxLength'] = $letter_count;
	}
	if (!isset($options['minLength']) || !isUnsignedInt($options['minLength']) || $options['minLength'] < 0) {
		$options['minLength'] = 0;
	}
	if ($options['minLength'] > $letter_count) {
		throw new Exception('getInitials() - not enough letters to produce initials of minLength ' . $options['minLength'] . ' for: ' . $str . '(' . $letter_count . ')');
	}
	if ($options['minLength'] > $options['maxLength']) {
		throw new Exception('getInitials() - options minLength ' . $options['minLength'] . ' cannot be more than maxLength ' . $options['maxLength']);
	}

	// Trivial case
	if ($str === '') {
		return '';
	}

	// Prepare initials and indices
	$front_initials = mb_substr($words[0], 0, 1); // init with first letter
	$back_initials = '';
	$front_word_index = 0; // index to run loop from front to back
	$back_word_index = count($words) - 1; // index to run loop from back to front

	// Check if we need more than just the first letters
	$additional_needed = $options['minLength'] - count($words);
	$curr_word_length = mb_strlen($words[$front_word_index]);
	$curr_char_index = 1;

	// Add additional letters
	while ($additional_needed > 0) {
		if ($curr_char_index === $curr_word_length) {
			// go to next word
			$front_word_index++;
			$additional_needed++; // remember to increment for each word we go past
			$curr_word_length = mb_strlen($words[$front_word_index]);
			$curr_char_index = 0;
		} // word has no more letters

		$front_initials = $front_initials . mb_substr($words[$front_word_index], $curr_char_index, 1);
		$additional_needed--;
		$curr_char_index++;
	}

	$front_word_index++; // remember to move on since we have already "consumed" this word

	// Find initials until we hit maxLength
	$take_from_front = FALSE; // toggling flag
	while ($front_word_index <= $back_word_index && mb_strlen($front_initials) + mb_strlen($back_initials) < $options['maxLength']) {
		if ($take_from_front) {
			$front_initials = $front_initials . mb_substr($words[$front_word_index], 0, 1);
			$front_word_index++;
		}
		else {
			$back_initials = mb_substr($words[$back_word_index], 0, 1) . $back_initials;
			$back_word_index--;
		}
		$take_from_front = !$take_from_front;
	} // while

	return $front_initials . $back_initials;
} //getInitials()

/**
 * Renders a template by replacing all {{ }} and {{{ }}} occurrences with actual data
 * - Note: Any template string enclosed in double curly braces {{  }} will be automatically escaped.
 * - Note: Any template string enclosed in triple curly braces {{{  }}} will NOT be escaped.
 * @param: {string} template
 *         {array} data - associative array of key-value pairs
 * @return: {string}
 */
function renderTemplate($template, $data) {
	if (!isset($template) || !is_string($template)) {
		throw new Exception('Error in renderTemplate() - template is not a string.');
	}
	if (is_string($data)) {
		$data = json_decode($data, TRUE);
	}
	if (!isset($data) || !is_array($data)) {
		throw new Exception('Error in renderTemplate() - data is not an array.');
	}

	// Replace all templates found in data
	foreach ($data as $key => $value) {
		// Only render strings, numbers and bools
		if (!is_string($value) && !is_numeric($value) && !is_bool($value)) {
			continue;
		}

		$escaped = escapeHTML((string) $value);
		$template = preg_replace('/{{{' . $key . '}}}/', $value, $template);
		$template = preg_replace('/{{' . $key . '}}/', $escaped, $template);
	}

	// Remove all unused templates
	$template = preg_replace('/{{{(?:(?!}}).)*}}}/', '', $template);
	$template = preg_replace('/{{(?:(?!}}).)*}}/', '', $template);
	
	return $template;
} //renderTemplate()

/**
 * Get info of a variable to string. Similar to var_dump() except output is to a var
 * @param: {varies}
 * @return: {string}
 */
function dump($a) {
	ob_start();
	var_dump($a);
	$dumped = ob_get_clean();
	return $dumped;
} // dump()

/********************
 *   String Checks  *
 ********************/ 
/**
 * Checks if a string starts / ends with another string
 * - Note: empty needle always evaluates to TRUE.
 * @param: {string} haystack
 *         {string} needle
 * @return: {string}
 */
function startsWith($haystack, $needle) {
	if (!isset($haystack) || !is_string($haystack)) {
		throw new Exception('Error in startsWith() - haystack is not a string.');
	}
	if (!isset($needle) || !is_string($needle)) {
		throw new Exception('Error in startsWith() - needle is not a string.');
	}
    return $needle === "" || mb_strpos($haystack, $needle) === 0;
}
function endsWith($haystack, $needle) {
	if (!isset($haystack) || !is_string($haystack)) {
		throw new Exception('Error in endsWith() - haystack is not a string.');
	}
	if (!isset($needle) || !is_string($needle)) {
		throw new Exception('Error in endsWith() - needle is not a string.');
	}
    return $needle === "" || mb_substr($haystack, -mb_strlen($needle)) === $needle;
}

/**
 * Checks if a var is an integer, string representation of integers, or floats that are whole numbers.
 * - NOTE: float values greater than PHP_INT_MAX may return TRUE due to precision limitations
 * @param: {int / float / string}
 * @return: {bool}
 */
function isInt($a) {
	if (!isset($a)) {
		return FALSE;
	}
	if (is_int($a)) { // Note: values greater than PHP_INT_MAX are considered floats
		return TRUE;
	}
	else if (is_float($a)) {
		return ($a == round($a));
	} //type: float
	else if (is_string($a)) {
		return preg_match("/^-?[0-9]+$/", $a) === 1;
	}
	return FALSE;
}

/**
 * Checks if a var is an unsigned integer
 * - NOTE: includes string representations of unsigned integers
 * - NOTE: includes floats that are zero or positive whole numbers
 * - NOTE: float values greater than PHP_INT_MAX may return TRUE due to precision limitations
 * e.g. string '0' returns TRUE, string '-1' returns FALSE.
 * @param: {varies}
 * @return: {bool}
 */
function isUnsignedInt($a) {
	if (is_int($a)) { // Note: values greater than PHP_INT_MAX are considered floats
		return ($a >= 0);
	}
	else if (is_float($a)) {
		return ($a == round($a) && $a >= 0);
	}
	else if (is_string($a)) {
		return ctype_digit($a);
	}
	return FALSE;
} //isUnsignedInt()
function isUInt($a) {
	return isUnsignedInt($a);
};

function isPositiveInt($a) {
	if (is_int($a)) { // Note: values greater than PHP_INT_MAX are considered floats
		return ($a > 0);
	}
	else if (is_float($a)) {
		return ($a == round($a) && $a > 0);
	}
	else if (is_string($a)) {
		return ctype_digit($a) && ltrim($a,'0') !== '';
	}
	return FALSE;
} //isPositiveInt()
