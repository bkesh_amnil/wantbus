<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Helper functions to enhance core PHP
 */

/***
 * Gets current system time accurate to milliseconds
 * @return: {int}
 */
//function getTimeInMs() {
//	list($ms, $s) = explode(' ', microtime());
//
//	if (PHP_INT_SIZE !== 8) {
//		throw new Exception('getTimeInMs() - unable to return integer larger than '.PHP_INT_MAX);
//	} // 32-bit machines
//	else {
//		$ms = round($ms * 1000);
//		return (int) ($s*1000 + $ms);
//	} // 64-bit machines
//} //getTimeInMs()

//#added/commented by bkesh
function getTimeInMs() {
	list($ms, $s) = explode(' ',microtime());
	$ms = round($ms * 1000);
	return $s*1000 + $ms;
} //getTimeInMs()

function check_nric($nric) {
  if ( preg_match('/^[ST][0-9]{7}[JZIHGFEDCBA]$/', $nric) ) 
  { // NRIC
    $check = "JZIHGFEDCBA";
  } else if ( preg_match('/^[FG][0-9]{7}[XWUTRQPNMLK]$/', $nric) ) 
  { // FIN
    $check = "XWUTRQPNMLK";
  } else 
  {
      echo 'not';
    return false;
  }

  $total = $nric[1]*2
    + $nric[2]*7
    + $nric[3]*6
    + $nric[4]*5
    + $nric[5]*4
    + $nric[6]*3
    + $nric[7]*2;

  if ( $nric[0] == "T" OR $nric[0] == "G" ) 
  {
    // shift 4 places for after year 2000
    $total = $total + 4; 
  }

  if ( $nric[8] == $check[$total % 11] ) {
//    return TRUE;
      echo 'yes';
  } else {
//    return FALSE;
      echo 'no';
  }
}