<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Enables storage of all PHP data types, including objects, NULL and TRUE/FALSE.
 * Connectivity to in-memory cache (Redis).
 * - Note: ensure redis is enabled as an extension in PHP, i.e. "extension = redis.so"
 *
 ***/
class Cache {
	private static $redis;
	
	static function init() {
		self::$redis = new Redis();
		self::$redis->connect(REDIS_HOST, REDIS_PORT);
		self::$redis->select(1); // Note: DB index 0 is reserved for sessions
	}
	
	// Default function: make invalid method calls throw Exceptions
	static function __callStatic($name, $arguments) {
		throw new Exception ('Error in Cache class: method '.$name.'() does not exist');
	} //call()
	
	/**
	 * Checks if cache has this key
	 * @param: {string}
	 * @return: {bool}
	 */
	static function has($key) {
		try {
			if (!isset(self::$redis)) {
				self::init();
			}
			
			return self::$redis->exists($key);
		}
		catch (Exception $e) {
			Log::error(__METHOD__.'() - '.$e->getMessage());
			return FALSE;
		}
	} //has()
	
	/**
	 * Gets an item from cache
	 * @param: {string}
	 * @return: {varies} value, or NULL on failure (e.g. no such key)
	 */
	static function get($key) {
		try {
			if (!isset(self::$redis)) {
				self::init();
			}
			
			if (!self::$redis->exists($key)) {
				return NULL;
			}
			
			return unserialize(self::$redis->get($key));
		}
		catch (Exception $e) {
			Log::error(__METHOD__.'() - '.$e->getMessage());
			return NULL;
		}
	} //get()
	
	/**
	 * Sets an item in cache. Optionally, set it to expire.
	 * - Note: if expiry is set to -1 (DEFAULT), item won't expire
	 * - Note: if expiry is set to 0, item won't be set at all
	 * @param: {string} key
	 *         {varies} value
	 *         {int} ttl in seconds (optional)
	 * @return: {bool} success
	 */
	static function set($key, $value, $ttl=-1) {
		try {
			if (!isset(self::$redis)) {
				self::init();
			}
			
			if ($ttl === -1) {
				if (!self::$redis->set($key, serialize($value))) {
					throw new Exception('Unable to store '.$key.' in Cache (SET)');
				}
			}
			else if ($ttl > 0) {
				if (!self::$redis->setEx($key, $ttl, serialize($value))) {
					throw new Exception('Unable to store '.$key.' in Cache (SETEX)');
				}
			}
			else {
				throw new Exception('cannot set ' . $key . ' to TTL '. dump($ttl));
			}

			return TRUE;
		}
		catch (Exception $e) {
			Log::error(__METHOD__.'() - '.$e->getMessage());
			return FALSE;
		}
	} //set()
	
	/**
	 * Set an item to expire some time in future
	 * - Note: setting ttl to 0 expires a key immediately (default)
	 * - Note: setting ttl to -1 persists a key indefinitely
	 * @param: {string} key
	 *         {int} ttl - in seconds (DEFAULT: 0)
	 * @return: {bool} success
	 */
	static function expire($key, $ttl=0) {
		try {
			if (!isset(self::$redis)) {
				self::init();
			}
			
			if ($ttl === -1) {
				if (!self::$redis->persist($key)) {
					throw new Exception('Unable to persist '.$key);
				}
			}
			else if ($ttl >= 0) {
				if (!self::$redis->expire($key, $ttl)) {
					throw new Exception('Unable to expire '.$key);
				}
			}
			else {
				throw new Exception('Unable to set ' . $key . ' to TTL '. dump($ttl));
			}

			return TRUE;
		}
		catch (Exception $e) {
			Log::error(__METHOD__.'() - '.$e->getMessage());
			return FALSE;
		}
	} //expire()

	/**
	 * Removes expirytimoeut of a key
	 * @param: {string} key
	 * @return: {bool} success
	 */
	static function persist($key) {
		try {
			if (!isset(self::$redis)) {
				self::init();
			}
			
			if (!self::$redis->persist($key)) {
				throw new Exception('Unable to persist '.$key);
			}

			return TRUE;
		}
		catch (Exception $e) {
			Log::error(__METHOD__.'() - '.$e->getMessage());
			return FALSE;
		}
	} //persist()

	/**
	 * Gets the TTL of an item
	 * - Note: returns -1 if key has no TTL; returns -2 if key doesn't exist.
	 * @param: {stirng} key
	 * @return: {int | NULL} - in seconds. NULL on failure
	 */
	static function ttl($key) {
		try {
			if (!isset(self::$redis)) {
				self::init();
			}
			
			return self::$redis->ttl($key);
		}
		catch (Exception $e) {
			Log::error(__METHOD__.'() - '.$e->getMessage());
			return NULL;
		}
	} //ttl()
	
	/**
	 * Removes all keys from cache.
	 */
	static function flush() {
		try {
			if (!isset(self::$redis)) {
				self::init();
			}
			
			self::$redis->flushDb();
		}
		catch (Exception $e) {
			Log::error(__METHOD__.'() - '.$e->getMessage());
			return FALSE;
		}
	} //flushAll()
	
	/**
	 * Gets all keys in the cache
	 * - Note: returns empty array on failure
	 * @return: {array} of strings
	 */
	static function keys() {
		try {
			if (!isset(self::$redis)) {
				self::init();
			}
			
			return self::$redis->keys('*');
		}
		catch (Exception $e) {
			Log::error(__METHOD__.'() - '.$e->getMessage());
			return array();
		}
	} //keys()
	static function getKeys() {
		return self::keys();
	}
	
	/**
	 * @return: {int} size of cache
	 */
	static function getSize() {
		try {
			if (!isset(self::$redis)) {
				self::init();
			}
			
			return self::$redis->dbSize();
		}
		catch (Exception $e) {
			Log::error(__METHOD__.'() - '.$e->getMessage());
			return 0;
		}
	} //getSize()
	static function size() {
		return self::getSize();
	}
} //class Cache
