<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Collection of functions for logging purposes
 ***/
class Log {
	//Default function: make invalid method calls throw Exceptions
	static function __callStatic($name, $arguments) {
		throw new Exception ('Error in Log class: method '.$name.'() does not exist');
	} //call()
	
	/*****************
	 * Log functions *
	 *****************/
	static function fatal($content) {
		self::appendLog('['.date('Y-m-d H:i:s').'] [FATAL] '.$content."\r\n");
	}
	
	static function error($content) {
		self::appendLog('['.date('Y-m-d H:i:s').'] [ERROR] '.$content."\r\n");
	}
	
	static function warning($content) {
		self::appendLog('['.date('Y-m-d H:i:s').'] [WARN] '.$content."\r\n");
	}
	
	static function info($content) {
		self::appendLog('['.date('Y-m-d H:i:s').'] [INFO] '.$content."\r\n");
	}
	
	static function test($content) {
		self::appendLog('['.date('Y-m-d H:i:s').'] [TEST] '.$content."\r\n");
	}
	
	static function debug($content) {
		self::appendLog('['.date('Y-m-d H:i:s').'] [DEBUG] '.$content."\r\n");
	}
	
	/**
	 * Appends message to log file
	 */
	private static function appendLog($content) {
		// Create logs folder if necessary
		$log_folder = API_ROOT.'/logs';
		if (!file_exists($log_folder)) {
			$old = umask(0); // temp remove any umask
			mkdir($log_folder, 0777, TRUE);
			umask($old); // put back old umask
		}

		// Write to file
		$file_name = 'server-'.date('Y-m-d').'.log';		
		if (!file_put_contents($log_folder.'/'.$file_name, $content, FILE_APPEND)) {
			throw new Exception('Unable to log to '.$log_folder.'/'.$file_name.' value: '.$content);
		}
	} //appendLog()
} //class Log
