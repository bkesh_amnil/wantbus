<?php
/***
 * Backend Framework v2.1.0 (Edited)
 * ========================
 *
 * Functions called by a Public User
 ***/
class UserAPI {
	
	/***
	 * Sends email to website contact person
	 * @input: $_POST['name', 'email', 'mobile', 'message']
	 * @output: message
	 ***/
	function processForm($response) {
		// Data check: name
		if (!isset($_POST['first_name']) || !Validator::isName($_POST['first_name'])) {
			$response->addData('error', 'Invalid first name: '.$_POST['first_name']);
			return 400;
		}else {
			$first_name = trim($_POST['first_name']);
		}
		if (!isset($_POST['last_name']) || !Validator::isName($_POST['last_name'])) {
			$response->addData('error', 'Invalid last name: '.$_POST['last_name']);
			return 400;
		}
		else {
			$last_name = trim($_POST['last_name']);
		}
		
		// Data check: email
		if (!isset($_POST['email'])) {
			$response->addData('error', 'Email is required.');
			return 400;
		}
		if (!Validator::isEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email: '.$_POST['email']);
			return 400;
		}
		else {
			$email = strtolower($_POST['email']);
		}
                
                // Data check: mobile
//		if (!isset($_POST['mobile']) || !Validator::isMobile($_POST['mobile'])) {
//			$response->addData('error', 'Illegal Mobile.');
//			return 400;
//		}
//		else {
//			$mobile = trim($_POST['mobile']);
//		}
		
                
		
		// Create email
//                $contact_email = array('nishit@amniltech.com','bikesh@amniltech.com','isen.majennt@techplusart.com');//InternalAPI::getSetting('contact-email');
                $contact_email = array('nishit@amniltech.com','isen.majennt@techplusart.com');//InternalAPI::getSetting('contact-email');//for staging
		$mail = new Mail();
		$mail->setSubject('Request For A Partnership'.' - '.$first_name.' '.$last_name);
//		$mail->setTo('admin@wantbus.com');//for admin
		$mail->setTo('bikesh@amniltech.com');//for staging
		$mail->setBcc($contact_email);
		$mail->setFrom(NO_REPLY_EMAIL,'WantBus Website Partnership Request');
		$mail->setReplyTo($email);
		$mail->addHTMLMail(API_ROOT.'/mails/site-message.html');
		$mail->addTextMail(API_ROOT.'/mails/site-message.txt');
		$mail->setVariable(array (
			'partnership' => $_POST['partnerships'],
			'firstname' => $first_name,
			'lastname' =>$last_name,
			'mobile' => $_POST['mobile'],
			'email' => $email,
			'company' =>  $_POST['company'],
			'designation' =>  $_POST['designation'],
			'website' =>  $_POST['website'],
			'siteurl' =>  PROJECT_DOMAIN,
		));
		
		// Send email
		if (!$mail->send()) {
			Log::fatal(__METHOD__.'() - unable to send out email to '.$contact_email);
			$response->addData('error', 'Server email error!');
			return 500;
		}
		
		// send message to customer
		$mail = new Mail();
		$mail->setSubject('WantBus - Thank You For Contacting Us');
		$mail->setTo('bikesh@amniltech.com');
		$mail->setBcc($_POST['email']);
		$mail->setFrom(ADMIN_REPLY_EMAIL,'The WantBus Team');
		$mail->setReplyTo('admin@wantbus.com');
		$mail->addHTMLMail(API_ROOT.'/mails/customer-message.html');
		$mail->addTextMail(API_ROOT.'/mails/customer-message.txt');
		$mail->setVariable(array (
			'partnership' => $_POST['partnerships'],
			'firstname' => $first_name,
			'lastname' =>$last_name,
			'mobile' => $_POST['mobile'],
			'email' => $_POST['email'],
			'company' =>  $_POST['company'],
			'designation' =>  $_POST['designation'],
			'website' =>  $_POST['website'],
			'siteurl' =>  PROJECT_DOMAIN,
		));
		
		// Send email
		if (!$mail->send()) {
			Log::fatal(__METHOD__.'() - unable to send out email to '.$contact_email);
			$response->addData('error', 'Server email error!');
			return 500;
		}
		
		// YAY
		return 200;
	} //sendContactMessage()
        
} //class UserAPI
