<?php
/***
 * Backend Framework v2.1.0 (Edited)
 * ========================
 *
 * Routines called only internally within the application
 * Useful for storing routines executed by multiple endpoints, potentially across multiple APIs
 ***/
class InternalAPI {
	/**
	 * Login function common across all user types
	 * The Session ID will be passed to the browser as a cookie, and will be used to identify this session for as long as the browser is open.
	 * - Note: should pass in HTTPResponse
	 * @input: $_POST['email', 'password', 'persistent']
	 *         $_POST['userType']
	 * @output: staff/member - User object,
	 *          csrf-token,
	 *          persistent-login-token (optional)
	 * if fail: password-attempts-remaining
	 */
	function login($response) {
		// Data checks
		if (!Validator::isEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email ' . $_POST['email']);
			return 400;
		}
		else {
			$_POST['email'] = strtolower($_POST['email']);
		}
		if (!Validator::isStrongPassword($_POST['password'])) {
			$response->addData('error', 'Wrong password.');
			return 400;
		}
		
		// Transaction for ACID-ity
		$committed = FALSE;
		while (!$committed) {
			$time = getTimeInMs();
			
			Database::beginTransaction();
			
			// Find this user in db
			$user = Database::readObject($_POST['userType'], array(
				array(
					array('email', '=', $_POST['email'])
				)
			), array(
				'update' => TRUE
			));
			if (!isset($user)) {
				$response->addData('error', 'Invalid email.');
				return 401;
			} // No such user
			
			// Check if account is disabled
			if (!$user->isEnabled()) {
				$response->addData('error', 'Account disabled.');
				return 403;
			}
			
			// Check if account is email verified
			if (!$user->isEmailVerified()) {
//				$response->addData('error', 'Account not verified.');
//				return 403;
			}
	
			// Check password
			if (Encryptor::checkPassword($_POST['password'], $user->getPasswordHash())) {
				$action = 'Logged In';

				// Generate persistent token if requested
				$persistent_login_hashes = $user->getPersistentLoginHashes();
				$persistent_login_hashes = Encryptor::purgeExpiredHashes($persistent_login_hashes, $time - PERSISTENT_LOGIN_DURATION);
				if (isset($_POST['persistent']) && $_POST['persistent']) {
					// Generate new token and hash it
					$token = Encryptor::generateRandomAlphanumericCode(PERSISTENT_LOGIN_TOKEN_LENGTH);
					$hash = Encryptor::getHash($token, $time);

					// Adds the newly generated hash to the user's string of hashes					
					if (strlen($persistent_login_hashes) == 0) {
						$persistent_login_hashes = $hash;
					} // first hash
					else if ((strlen($persistent_login_hashes) + strlen($hash)) > PERSISTENT_LOGIN_HASHES_LENGTH) {
						$first_comma_idx = strpos($persistent_login_hashes, ',');
						$persistent_login_hashes = substr($persistent_login_hashes, $first_comma_idx + 1);
						$persistent_login_hashes .= ','.$hash;
					} // kick out first hash
					else {
						$persistent_login_hashes .= ','.$hash;
					} // append hash

					$response->addData('persistent-login-token', $token);
				}
				else{
					$_POST['persistent'] = FALSE;
					$token = NULL;
				}
				
				// Set login time and reset authentication attempts
				$user->update(array(
					'persistentLoginHashes' => $persistent_login_hashes,
					'passwordAttemptsRemaining' => MAX_PASSWORD_ATTEMPTS,
					'lastUpdateTime' => $time,
					'lastLoginTime' => $time
				));
			} // Correct password
			else {
				$action = 'Failed Login';
				
				// Decrease authentication attempts
				$remaining_attempts = $user->getPasswordAttemptsRemaining() - 1;
				$user->update(array(
					'passwordAttemptsRemaining' => $remaining_attempts,
					'lastUpdateTime' => $time
				));
			} // Wrong password
			
			// Update user in DB
			if (!Database::update($user)) {
				Log::fatal(__METHOD__.'() - DB Unable to update ' . $user . '. Persistent: '.$_POST['persistent'].'. Token: '.$token.'. Action: '.$action);
				$response->addData('error', 'Unable to update user.');
				return 500;
			}
			
			// Commit transaction
			$committed = Database::endTransaction();
		} // while not committed

		// Store this activity
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user->getID(),
                            'userType' => $_POST['userType'],
                            'action' => $action,
                            'createdTime' => $time
                    ));
                    
                }
//		
//		// Handle login failure
		if ($action === 'Failed Login') {
			$response->addData('password-attempts-remaining', $remaining_attempts);
			$response->addData('error', 'Invalid password.');
			return 401;
		}
		
		// Set session user
		Session::regenerateID(); //remember to regenerate ID to prevent session fixation
		$_SESSION['user'] = $user;
		$_SESSION['CSRF_TOKEN'] = Encryptor::generateRandomAlphanumericCode(CSRF_TOKEN_LENGTH);
		
		// YAY!
		$response->addData(lcfirst($_POST['userType']), $user->getSanitizedArray());
		$response->addData('csrf-token', $_SESSION['CSRF_TOKEN']);
		return 200;
	} // login()
	
	/***
	 * Persistent Login function common across all user types
	 * Attempts to set the user as the session user using a persistent token. A new persistent token will be issued.
	 * The Session ID will be passed to the browser as a cookie, and will be used to identify this session for as long as the browser is open.
	 * - Note: should pass in HTTPResponse
	 * @input: $_POST['email', 'token']
	 *         $_POST['userType']
	 * @output: staff/member - User object,
	 *          csrf-token,
	 *          persistent-login-token
	 * if fail: password-attempts-remaining
	 ***/
	function persistentLogin($response) {
		// Data checks
		if (!Validator::isEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email ' . $_POST['email']);
			return 400;
		}
		else {
			$_POST['email'] = strtolower($_POST['email']);
		}

		// Transaction for ACID-ity
		$committed = FALSE;
		while (!$committed) {
			$time = getTimeInMs();
			
			Database::beginTransaction();

			// Find this user in db
			$user = Database::readObject($_POST['userType'], array(
				array(
					array('email', '=', $_POST['email'])
				)
			), array(
				'update' => TRUE
			));
			if (!isset($user)) {
				$response->addData('error', 'Invalid email address.');
				return 401;
			} // No such user
			
			// Check if account is disabled
			if (!$user->isEnabled()) {
				$response->addData('error', 'Account disabled.');
				return 403;
			}
			
			// Check if account is email verified
			if (!$user->isEmailVerified()) {
				$response->addData('error', 'Account not verified.');
				return 403;
			}
	
			// Check persistent login token
			$old_hashes = $user->getPersistentLoginHashes();
			$good_hashes = Encryptor::purgeExpiredHashes($old_hashes, $time - PERSISTENT_LOGIN_DURATION); // purge expired hashes while we're at it
			$new_token = Encryptor::generateRandomAlphanumericCode(PERSISTENT_LOGIN_TOKEN_LENGTH);
			$renewed_hashes = Encryptor::renewTimeSensitiveHash($good_hashes, $_POST['token'], $new_token, $time - PERSISTENT_LOGIN_DURATION, $time);
			if (isset($renewed_hashes)) {
				$action = 'Logged In (Persistent)';

				// Set login time and new hashes
				$user->update(array(
					'passwordAttemptsRemaining' => MAX_PASSWORD_ATTEMPTS,
					'persistentLoginHashes' => $renewed_hashes,
					'lastUpdateTime' => $time,
					'lastLoginTime' => $time,
				));

				$response->addData('persistent-login-token', $new_token);
			} // good token
			else {
				$action = 'Failed Login (Persistent)';
				
				// Decrease authentication attempts
				$remaining_attempts = $user->getPasswordAttemptsRemaining() - 1;
				$user->update(array(
					'passwordAttemptsRemaining' => $remaining_attempts,
					'persistentLoginHashes' => $good_hashes, // remember to update hashes (after purge)
					'lastUpdateTime' => $time,
				));
			} // bad token
			
			// Update user in DB
			if (!Database::update($user)) {
				Log::fatal(__METHOD__.'() - DB Unable to update ' . $user . '. Token: '. $new_token . '. Action: ' . $action);
				$response->addData('error', 'Unable to update user login status.');
				return 500;
			}
			
			$committed = Database::endTransaction();
		} // while not committed

                if(LOCAL !=1){
		// Store this activity
		Datastore::set('Activity', array(
			'userID' => $user->getID(),
			'userType' => $_POST['userType'],
			'action' => $action,
			'createdTime' => $time,
		));
                }
		
		// Handle login failure
		if (!isset($renewed_hashes)) {
			$response->addData('password-attempts-remaining', $remaining_attempts);
			$response->addData('error', 'Invalid token.');
			return 401;
		}
		
		// Set session user
		Session::regenerateID(); //remember to regenerate ID to prevent session fixation
		$_SESSION['user'] = $user;
		$_SESSION['CSRF_TOKEN'] = Encryptor::generateRandomAlphanumericCode(CSRF_TOKEN_LENGTH);
		
		// YAY!
		$response->addData(lcfirst($_POST['userType']), $user->getSanitizedArray());
		$response->addData('csrf-token', $_SESSION['CSRF_TOKEN']);
		return 200;
	} //persistentLogin()
	
	/***
	 * Logout function common to all user types
	 * Clears any existing Session ID is cleared and a new session will be started
	 * - Note: should pass in HTTPResponse
	 * @input: $_POST['userType']
	 * @output: -
	 ***/
	function logout($response) {
		// Ensure action is legal - check that the user is indeed a logged in user of this type
		if (get_class($_SESSION['user']) != $_POST['userType']) {
			$response->addData('error', 'No session '.$_POST['userType']);
			return 400;
		}
		
		// Store this activity
                if(LOCAL !=1){
                    $time = getTimeInMs();
                    Datastore::set('Activity', array(
                            'userID' => $_SESSION['user']->getID(),
                            'userType' => $_POST['userType'],
                            'action' => 'Logged Out',
                            'createdTime' => $time
                    ));
                }

		// Restart this session with new session ID
		Session::stop();
		Session::start();
		Session::regenerateID();

		// YAY!
		return 200;
	} // logout()

	/**
	 * Verify user's email with an OTP.
	 * - On success, user will be considered authenticated (logged in), with his password randomised and returned  (for changePassword)
	 * @input: $_POST['email', 'code']
	 *         $_POST['userType']
	 * @output: staff/member - User object,
	 *          password,
	 *          csrf-token
	 * if fail: otp-attempts-remaining
	 */
	function verifyEmail($response) {
		// Data checks
		if (!Validator::isEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email ' . $_POST['email']);
			return 400;
		}
		else {
			$_POST['email'] = strtolower($_POST['email']);
		}

		if (!isset($_POST['code']) || !Validator::isOTP($_POST['code'])) {
			$response->addData('error', 'Wrong code.');
			return 401;
		}

		// Transaction for ACID-ity
		$committed = FALSE;
		while (!$committed) {
			$time = getTimeInMs();

			$password = Encryptor::generateRandomPassword();
			$hash = Encryptor::getHash($password);

			Database::beginTransaction();
			
			// Read this user
			$user = Database::readObject($_POST['userType'], array(
				array(
					array('email', '=', $_POST['email'])
				)
			), array(
				'update' => TRUE
			));
			if (!isset($user)) {
				$response->addData('error', 'User not found.');
				return 400;
			}
			
			// Check if we need to do anything
			if ($user->isEmailVerified()) {
				$response->addData('error', 'Email already verified.');
				return 400;
			}

			// Check if account is banned
			if (!$user->isEnabled()) {
				$response->addData('error', 'Account disabled.');
				return 403;
			}
			
			// Check email verification code
			if (Encryptor::checkTimeSensitiveCode($_POST['code'], $user->getEmailVerificationHash(), $time - OTP_EXPIRY)) {
				$action = 'Verified Email';

				// Set to verified, set login time and reset authentication attempts
				$user->update(array(
					'otpAttemptsRemaining' => MAX_OTP_ATTEMPTS,
					'emailVerified' => TRUE,
					'emailVerificationHash' => NULL,
					'passwordHash' => $hash,
					'lastUpdateTime' => $time,
					'lastLoginTime' => $time,
				));
			} // Correct password
			else {
				$action = 'Failed Email Verification';
				
				// Decrease authentication attempts
				$remaining_attempts = $user->getOTPAttemptsRemaining() - 1;
				$user->update(array(
					'otpAttemptsRemaining' => $remaining_attempts,
					'lastUpdateTime' => $time,
				));
			} // Wrong password

			if (!Database::update($user)) {
				Log::fatal(__METHOD__.'() - DB unable to update '. $user ."\n" .dump($user));
				$response->addData('error', 'DB unable to update user.');
				return 500;
			}

			$committed = Database::endTransaction();
		} // while not committed

		// Store this activity
                if(LOCAL !=1){
                    Datastore::set('Activity', array(
                            'userID' => $user->getID(),
                            'userType' => $_POST['userType'],
                            'action' => $action,
                            'createdTime' => $time
                    ));
                }

		// Handle login failure
		if ($action == 'Failed Email Verification') {
			$response->addData('otp-attempts-remaining', $remaining_attempts);
			$response->addData('error', 'Wrong / expired code.');
			return 401;
		}
		
		// Set session user
		Session::regenerateID(); //remember to regenerate ID to prevent session fixation
		$_SESSION['user'] = $user;
		$_SESSION['CSRF_TOKEN'] = Encryptor::generateRandomAlphanumericCode(CSRF_TOKEN_LENGTH);
		
		// YAY!
		$response->addData(lcfirst($_POST['userType']), $user->getSanitizedArray());
		$response->addData('csrf-token', $_SESSION['CSRF_TOKEN']);
		$response->addData('password', $password);
		return 200;
	} // verifyEmail()
	
	/**
	 * Sends an OTP to user's email, and sets a new time-sensitive email verification hash. To be followed up with verifyEmail request.
	 * @input: $_POST['email']
	 *         $_POST['url, userType']
	 * @output: -
	 */
	function forgetPassword($response) {
		// Data checks
		if (!Validator::isEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email ' . $_POST['email']);
			return 400;
		}
		else {
			$_POST['email'] = strtolower($_POST['email']);
		}

		// Transaction for ACID-ity
		$committed = FALSE;
		while (!$committed) {
			$time = getTimeInMs();

			// Generate email verification code
			$code = Encryptor::generateRandomNumericCode(OTP_LENGTH);
			$hash = Encryptor::getHash($code, $time);

			Database::beginTransaction();

			// Read user from, DB
			$user = Database::readObject($_POST['userType'], array(
				array(
					array('email', '=', $_POST['email'])
				)
			), array(
				'update' => TRUE
			));
			if (!isset($user)) {
				$response->addData('error', 'No such email in our records.');
				return 403;
			}

			// Update user
			$user->update(array(
				'emailVerified' => FALSE,
				'emailVerificationHash' => $hash,
				'lastUpdateTime' => $time
			));
			if (!Database::update($user)) {
				Log::fatal(__METHOD__.'() - DB unable to update user.');
				$response->addData('error', 'DB unable to update user.');
				return 500;
			}

			$committed = Database::endTransaction();
		} // while not committed

                if(LOCAL !=1){
		// Store this activity
                    Datastore::set('Activity', array(
                            'userID' => $user->getID(),
                            'userType' => $_POST['userType'],
                            'action' => 'Forgot Password',
                            'createdTime' => $time
                    ));
                }

		// Send email
		$mail = new Mail();
		$mail->setSubject(PROJECT_NAME.' Forget Password');
		$mail->setTo($_POST['email']);
		$mail->addHTMLMail(API_ROOT.'/mails/forget-password.html');
		$mail->addTextMail(API_ROOT.'/mails/forget-password.txt');
		$mail->setVariable(array (
			'code' => $code,
			'url' => $_POST['url'].'?email='.$_POST['email'].'&code='.$code
		));
		if (!$mail->send()) {
			Log::fatal(__METHOD__.'() - unable to send mail to '.$user.' ('.$_POST['email'].')');
			Log::debug(__METHOD__.'() - code '.$code. ' URL: '.$_POST['url'].'?email='.$_POST['email'].'&code='.$code);
			$response->addData('error', 'Server mail system error!');
			return 500;
		}

		// DEBUG
		//$response->addData('code', $code);

		// YAY
		return 200;
	} //forgetPassword()
	
	/**
	 * Changes password of session user
	 * @input: $_POST['old-password, new-password']
	 *         $_POST['userType']
	 * @output: -
	 */
	function changePassword($response) {
		// Ensure action is legal - check that the user is indeed a logged in user of this type
		if (get_class($_SESSION['user']) != $_POST['userType']) {
			$response->addData('error', 'No session '.$_POST['userType']);
			return 400;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check old password
		if (!isset($_POST['old-password']) || !Validator::isStrongPassword($_POST['old-password'])) {
			$response->addData('error', 'Wrong password.');
			return 401;
		}
		if (!Encryptor::checkPassword($_POST['old-password'], $_SESSION['user']->getPasswordHash())) {
			$response->addData('error', 'Wrong password.');
			return 401;
		}

		// Check new password
		if ($_POST['new-password'] == $_POST['old-password']) {
			$response->addData('error', 'New password must be different from current password.');
			return 400;
		}
		if (!isset($_POST['new-password']) || !Validator::isStrongPassword($_POST['new-password'])) {
			$response->addData('error', 'Password must be at least '.MIN_PASSWORD_LENGTH.' characters.');
			return 400;
		}
		$hash = Encryptor::getHash($_POST['new-password']);

		// Transaction for ACID-ity
		$user_id = $_SESSION['user']->getID();
		$action = 'Changed Password';
		$committed = FALSE;
		while (!$committed) {
			$time = getTimeInMs();

			Database::beginTransaction();

			// Find this user in DB
			$user = Database::readObjectByID($_POST['userType'], $user_id, array(
				'update' => TRUE
			));
			if (!isset($user)) {
				$response->addData('error', 'No such staff.');
				return 404;
			}
			
			// Update user
			$user->update(array(
				'passwordHash' => $hash,
				'lastUpdateTime' => $time
			));
			if (!Database::update($user)) {
				Log::fatal(__METHOD__.'() - DB unable to update '.$user.' password.');
				$response->addData('error', 'DB Unable to update new profile.');
				return 500;
			}

			$committed = Database::endTransaction();
		} // while not committed

                if(LOCAL !=1){
                    // Store this activity
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => $_POST['userType'],
                            'action' => $action,
                            'createdTime' => $time
                    ));
                }

		// Update session user
		$_SESSION['user'] = $user;
		
		return 200;
	} //changePassword()

	/**
	* Gets data from DB
	 * Note: conditions is read as a JSON string representing a 3D array of OR conditions. When parsed, each condition should be an array of 3 elements - representing KEY, OPERAND, VALUE.
	 * Note: order must be "ASC" or "DESC"
	 * Note: search is case insensitive by default (COLLATE utf8_unicode_ci)
	 * @input: $_GET['conditions, offset, count, sort, order, sort2, order2']
	 *         $_POST['userType', 'dataType', dataTypePlural']
	 * @putput: count,
	 *          Array of data
	 */
	function getData($response) {
		// Ensure action is legal - check that the user is indeed a logged in user of this type
		if (get_class($_SESSION['user']) != $_POST['userType']) {
			$response->addData('error', 'No session '.$_POST['userType']);
			return 400;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check conditions
		if (!isset($_GET['conditions'])) {
			$_GET['conditions'] = '[]';
		}
		$conditions = self::parseConditions($_GET['conditions']);
		if (is_null($conditions)) {
			$response->addData('error', 'Error parsing conditions.');
			return 400;
		}

		// Check offset
		$offset = isset($_GET['offset'])? $_GET['offset'] : 0;
		if (!isUnsignedInt($offset)) {
			$response->addData('error', 'offset must be an integer.');
			return 400;
		}
		
		// Check count
		$count = isset($_GET['count'])? $_GET['count'] : 10;
		if (!isUnsignedInt($count)) {
			$response->addData('error', 'count must be an integer.');
			return 400;
		}

		// Check sort
		if (!isset($_GET['sort'])) {
			$sort = 'createdTime';
			$order = 'ASC';
		}
		else {
			$sort = $_GET['sort'];
			$order = isset($_GET['order']) ? $_GET['order'] : 'ASC';
			if (!property_exists($_POST['dataType'], $sort)) {
				$response->addData('error', 'No such field: '. $sort);
				return 400;
			}
			if ($order !== 'ASC' && $order !== 'DESC') {
				$response->addData('error', 'Order must be ASC or DESC');
				return 400;
			}
		}

		// Check sort 2
		if (!isset($_GET['sort2'])) {
			$sort2 = NULL;
			$order2 = NULL;
		}
		else {
			$sort2 = $_GET['sort2'];
			if (!property_exists($_POST['dataType'], $sort2)) {
				$response->addData('error', 'No such field: '. $sort2);
				return 400;
			}

			if (!isset($_GET['order2'])) {
				$order2 = 'ASC';
			}
			else {
				$order2 = $_GET['order2'];
			}
			if ($order2 !== 'ASC' && $order2 !== 'DESC') {
				$response->addData('error', 'Order2 must be ASC or DESC');
				return 400;
			}
		}

		// Read from DB
		$total_count = Database::count($_POST['dataType'], $conditions);
		$models = Database::readObjects($_POST['dataType'], $conditions, array(
			'offset' => $offset,
			'count' => $count,
			'sort' => $sort,
			'order' => $order,
			'sort2' => $sort2,
			'order2' => $order2
		));

		// Make into data array
		$data = array();
		foreach ($models as $model) {
			if ($model instanceof User) {
				$data[] = $model->getSanitizedArray();
			}
			else {
				$data[] = $model->toArray();
			}
		}

		// YAY
		$response->addData('count', $total_count);
		$response->addData(lcfirst($_POST['dataTypePlural']), $data);
		return 200;
	} //getData()

	/**
	* Counts data from DB
	 * Note: conditions is read as a JSON string representing a 3D array of OR conditions. When parsed, each condition should be an array of 3 elements - representing KEY, OPERAND, VALUE.
	 * @input: $_GET['conditions']
	 *         $_POST['userType, dataType']
	 * @putput: count
	 */
	function getDataCount($response) {
		// Ensure action is legal - check that the user is indeed a logged in user of this type
		if (get_class($_SESSION['user']) != $_POST['userType']) {
			$response->addData('error', 'No session '.$_POST['userType']);
			return 400;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check conditions
		if (!isset($_GET['conditions'])) {
			$_GET['conditions'] = '[]';
		}
		$conditions = self::parseConditions($_GET['conditions']);
		if (is_null($conditions)) {
			$response->addData('error', 'Error parsing conditions.');
			return 400;
		}

		// Read from DB and return
		$total_count = Database::count($_POST['dataType'], $conditions);

		$response->addData('count', $total_count);
		return 200;
	} //getDataCount()

	/**
	 * Reads settings from cache or datastore
	 * - Note: whether data is refetched from datastore depends on SETTINGS_CACHE_PERIOD
	 * @param: {bool} force - whether to force refresh
	 * @return: {array} associative array of key-value pairs
	 */
	static function readSettings($force = FALSE) {
		$settings = Cache::get('settings');
		if (!isset($settings) || $force) {
			$settings = array();

			// Get from datastore
			$results = Datastore::get('Setting');
			foreach ($results as $result) {
				$settings[$result['key']] = $result['value'];
			}

			// Store in cache
			Cache::set('settings', $settings, SETTINGS_CACHE_PERIOD);
		}

		return $settings;
	} //readSettings()

	/**
	 * - Note: whether data is refetched from datastore depends on SETTINGS_CACHE_PERIOD
	 * @param: {string} name
	 *         {bool} force - whether to force refresh
	 * @return: {string} value
	 */
	static function readSetting($name, $force = FALSE) {
		$settings = self::readSettings($force);
		return $settings[$name];
	} //readSetting()

	/**
	 * - Note: whether data is refetched from datastore depends on DATALISTS_REFRESH_PERIOD
	 * @param: {string} name of datalist to fetch
	 *         {bool} force - whether to force refresh
	 * @return: {array} 2D array (indexed array of items. Each item is an associative array)
	 */
	static function readDatalist($name, $force = FALSE) {
		self::attemptRefreshDatalists('www', $force); // Note: datalists do not belong strictly to any one domain.

		$datalists_dir = self::getDatalistsDirectory('www');
		$file_path = $datalists_dir . '/' . $name . '.json';

		$json = file_get_contents($file_path);
		return json_decode($json, TRUE);
	} //readDatalist()

	/**
	 * Attempts a sync of datalists with datastore
	 * - Note: whether data is refetched from datastore depends on DATALISTS_REFRESH_PERIOD
	 * @param: {string} domain label, e.g. "www"
	 *         {bool} force - whether to force refresh
	 * @return: {array} 2D array of [is-refreshed, files-created, files-updated, files-deleted, last-refresh-time, next-refresh-time].
	 */
	static function attemptRefreshDatalists($domain_label, $force = FALSE) {
		// Check if we need to refresh now
		$time = getTimeInMs();
		$last_refresh_time = Cache::get('datalists-last-refresh-time');
		if (!isset($last_refresh_time)) {
			$last_refresh_time = 0;
		}
		$time_since_last_refresh = $time - $last_refresh_time;
		$refresh_period = DATALISTS_REFRESH_PERIOD * 1000;
		if (!$force &&  $time_since_last_refresh < $refresh_period) {
			return array(
				'is-refreshed' => FALSE,
				'files-created' => array(),
				'files-updated' => array(),
				'files-deleted' => array(),
				'last-refresh-time' => $last_refresh_time,
				'next-refresh-time' =>  $last_refresh_time + $refresh_period
			);
		} // not yet time to refresh
		//Log::debug(__METHOD__.'() - refreshing datalists...');

		// Get current files on this server
		$datalists_dir = self::getDatalistsDirectory($domain_label);
		$local_files = scandir($datalists_dir);
		$local_datalists = array();
		foreach ($local_files as $idx => $file) {
			if (is_dir($datalists_dir. '/' . $file)) {
				continue;
			}
			$local_datalists[] = $file;
		}

		// Get current datalists from Datastore
		$datalists = Datastore::get('DataList');
		
		// Create or update files that have been updated in Datastore
		$files_created = array();
		$files_updated = array();
		foreach ($datalists as $datalist) {
			$file_name = encodeFileName($datalist['name']) . '.json';
			$file_exists = in_array($file_name, $local_files);

			// Ignore existing files that have not changed since last refresh
			if ($file_exists && $datalist['lastUpdateTime'] < $last_refresh_time) {
				continue;
			}

			// Re-create datalist
			$json = json_encode(json_decode($datalist['json'], TRUE), JSON_PRETTY_PRINT); // re-encode JSON to pretty print
			createFileRecursive($datalists_dir . '/' . $file_name, $json);

			if ($file_exists) {
				$files_updated[] = $datalist['name'];
			}
			else {
				$files_created[] = $datalist['name'];
			}
		}

		// Delete datalists that have been removed in Datastore
		$files_deleted = array();
		foreach ($local_datalists as $local_datalist) {
			$exists = FALSE;
			foreach ($datalists as $datalist) {
				if ($local_datalist === encodeFileName($datalist['name']) . '.json') {
					$exists = TRUE;
					break;
				}
			}
			if (!$exists) {
				unlink($datalists_dir . '/' .$local_datalist);
				$files_deleted[] = $local_datalist;
			}
		} // foreach local datalist

		Cache::set('datalists-last-refresh-time', $time);

		return array(
			'datalists' => $datalists,
			'is-refreshed' => TRUE,
			'files-created' => $files_created,
			'files-updated' => $files_updated,
			'files-deleted' => $files_deleted,
			'last-refresh-time' => $time,
			'next-refresh-time' =>  $time + $refresh_period
		);
	} //attemptRefreshDatalists()

	/**
	 * Attempts a sync of HTML pages with datastore
	 * - Note: whether data is refetched from datastore depends on HTML_PAGES_REFRESH_PERIOD
	 * @param: {string} domain label, e.g. "www"
	 *         {bool} force - whether to force refresh
	 * @return: {array} 2D array of [is-refreshed, files-created, files-updated, files-deleted, last-refresh-time, next-refresh-time].
	 */
	static function attemptRefreshHTMLPages($domain_label, $force = FALSE) {
		$files_created = array();
		$files_updated = array();
		$files_deleted = array();

		// Check if we need to refresh now
		$time = getTimeInMs();
		$last_refresh_time = Cache::get('html-pages-last-refresh-time');
		if (!isset($last_refresh_time)) {
			$last_refresh_time = 0;
		}
		$time_since_last_refresh = $time - $last_refresh_time;
		$refresh_period = HTML_PAGES_REFRESH_PERIOD * 1000;
		if (!$force && $time_since_last_refresh < $refresh_period) {
			return array(
				'is-refreshed' => FALSE,
				'files-created' => array(),
				'files-updated' => array(),
				'files-deleted' => array(),
				'last-refresh-time' => $last_refresh_time,
				'next-refresh-time' => $last_refresh_time + $refresh_period
			);
		} // not yet time to refresh
		//Log::debug(__METHOD__.'() - refreshing HTML pages...');

		// Get current files on this server
		$html_pages_dir = self::getHTMLPagesDirectory($domain_label);
		$local_files = scandir($html_pages_dir);
		$local_pages = array();
		foreach ($local_files as $idx => $file) {
			if (is_dir($html_pages_dir. '/' . $file)) {
				continue;
			}
			$local_pages[] = $file;
		}
		//Log::debug(__METHOD__.'() - local htmls: '.array2csv($local_htmls));

		// Get current pages on Datastore
		$pages = Datastore::get('Page', array(
			array('domainLabel', '=', $domain_label)
		));

		// Create or update files that have been updated in Datastore
		foreach ($pages as $page) {
			// Check if file exists
			if (isset($page['html'])) {
				$file_exists = in_array($page['html'], $local_pages);
			}
			else {
				$file_exists = in_array($page['name'].'.html', $local_pages);
			}

			// Ignore existing files that have not changed since last refresh
			if ($file_exists && $page['lastUpdateTime'] < $last_refresh_time)  {
				continue;
			}

			// Re-create page
			$data = json_decode($page['data'], TRUE); // format into associative array
			$file_name = isset($page['html']) ? $page['html'] : $page['name'].'.html';
			$html = renderTemplate($page['template'], $data);
			createFileRecursive($html_pages_dir . '/' . $file_name, $html);

			// Append updated or created
			if ($file_exists) {
				$files_updated[] = $file_name;
			}
			else {
				$files_created[] = $file_name;	
			}
		}

		// Recreate meta (pages.json)
		$meta = self::readPagesMeta($domain_label);
		foreach ($pages as $page) {
			$page_meta = array(
				'title' => $page['title'],
				'description' => $page['description']
			);
			if (isset($page['html'])) {
				$page_meta['html'] = $page['html'];
			}
			if (isset($page['css'])) {
				$page_meta['css'] = $page['css'];
			}
			if (isset($page['js'])) {
				$page_meta['js'] = $page['js'];
			}
			if (isset($page['img'])) {
				$page_meta['img'] = $page['img'];
			}
			$meta[$page['name']] = $page_meta;
		}
		self::updatePagesMeta($domain_label, $meta);

		Cache::set('html-pages-last-refresh-time', $time);
		
		return array(
			'pages' => $pages,
			'is-refreshed' => TRUE,
			'files-created' => $files_created,
			'files-updated' => $files_updated,
			'files-deleted' => $files_deleted,
			'last-refresh-time' => $time,
			'next-refresh-time' =>  $time + $refresh_period
		);
	} //attemptRefreshHTMLPages()

	/**
	 * Attempts a sync of documents with datastore
	 * - Note: whether data is refetched from datastore depends on DOCUMENTS_REFRESH_PERIOD
	 * @param: {string} domain label, e.g. "www"
	 *         {bool} force - whether to force refresh
	 * @return: {array} 2D array of [is-refreshed, files-created, files-updated, files-deleted, last-refresh-time, next-refresh-time].
	 */
	static function attemptRefreshDocuments($domain_label, $force = FALSE) {
		// Check if we need to refresh now
		$time = getTimeInMs();
		$last_refresh_time = Cache::get('documents-last-refresh-time');
		if (!isset($last_refresh_time)) {
			$last_refresh_time = 0;
		}
		$time_since_last_refresh = $time - $last_refresh_time;
		$refresh_period = DOCUMENTS_REFRESH_PERIOD * 1000;
		if (!$force &&  $time_since_last_refresh < $refresh_period) {
			return array(
				'is-refreshed' => FALSE,
				'files-created' => array(),
				'files-updated' => array(),
				'files-deleted' => array(),
				'last-refresh-time' => $last_refresh_time,
				'next-refresh-time' =>  $last_refresh_time + $refresh_period
			);
		} // not yet time to refresh
		//Log::debug(__METHOD__.'() - refreshing docs...');

		// Get current files on this server
		$doc_dir = self::getDocumentsDirectory($domain_label);
		$local_files = scandir($doc_dir);
		$local_docs = array();
		foreach ($local_files as $idx => $file) {
			if (is_dir($doc_dir. '/' . $file)) {
				continue;
			}
			$local_docs[] = $file;
		}
		//Log::debug(__METHOD__.'() - local docs: '.array2csv($local_docs));
		
		// Get current docs on Datastore
		$docs = Datastore::get('Document', array(
			array('domainLabel', '=', $domain_label)
		), array(
			'fields' => 'domainLabel, fileName, createdTime, lastUpdateTime'
		));

		// Create or update files that have been updated in Datastore
		$files_created = array();
		$files_updated = array();
		foreach ($docs as $doc) {
			$file_exists = in_array($doc['fileName'], $local_docs);

			// Ignore existing files that have not changed since last refresh
			if ($file_exists && $doc['lastUpdateTime'] < $last_refresh_time) {
				continue;
			}

			// Fetch blob from DB
			$doc = Datastore::getOne('Document', array(
				array('domainLabel', '=', $domain_label),
				array('fileName', '=', $doc['fileName'])
			), array(
				'fields' => 'fileName, blob'
			));

			// Re-create doc
			createFileRecursive($doc_dir . '/' . $doc['fileName'], $doc['blob']);

			// Append updated or created
			if ($file_exists) {
				$files_updated[] = $doc['fileName'];
			}
			else {
				$files_created[] = $doc['fileName'];	
			}
		} // foreach Datastore doc

		// Delete docs that have been removed in Datastore
		$files_deleted = array();
		foreach ($local_docs as $local_doc) {
			$exists = FALSE;
			foreach ($docs as $doc) {
				if ($local_doc === $doc['fileName']) {
					$exists = TRUE;
					break;
				}
			}
			if (!$exists) {
				unlink($doc_dir . '/' . $local_doc);
				$files_deleted[] = $local_doc;
			}
		} // foreach local doc

		Cache::set('documents-last-refresh-time', $time);

		return array(
			'documents' => $docs,
			'is-refreshed' => TRUE,
			'files-created' => $files_created,
			'files-updated' => $files_updated,
			'files-deleted' => $files_deleted,
			'last-refresh-time' => $time,
			'next-refresh-time' =>  $time + $refresh_period
		);
	} //attemptRefreshDocuments()

	/**
	 * Attempts a sync of images with datastore
	 * - Note: whether data is refetched from datastore depends on IMAGES_REFRESH_PERIOD
	 * @param: {string} domain label, e.g. "www"
	 *         {bool} force - whether to force refresh
	 * @return: {array} 2D array of [is-refreshed, files-created, files-updated, files-deleted, last-refresh-time, next-refresh-time].
	 */
	static function attemptRefreshImages($domain_label, $force = FALSE) {
		// Check if we need to refresh now
		$time = getTimeInMs();
		$last_refresh_time = Cache::get('images-last-refresh-time');
		if (!isset($last_refresh_time)) {
			$last_refresh_time = 0;
		}
		$time_since_last_refresh = $time - $last_refresh_time;
		$refresh_period = IMAGES_REFRESH_PERIOD * 1000;
		if (!$force &&  $time_since_last_refresh < $refresh_period) {
			return array(
				'is-refreshed' => FALSE,
				'files-created' => array(),
				'files-updated' => array(),
				'files-deleted' => array(),
				'last-refresh-time' => $last_refresh_time,
				'next-refresh-time' =>  $last_refresh_time + $refresh_period
			);
		} // not yet time to refresh
		//Log::debug(__METHOD__.'() - refreshing images...');

		// Get current files on this server
		$img_dir = self::getImageUploadDirectory($domain_label);
		$local_files = scandir($img_dir);
		$local_images = array();
		foreach ($local_files as $idx => $file) {
			if (is_dir($img_dir. '/' . $file)) {
				continue;
			}
			$local_images[] = $file;
		}
		//Log::debug(__METHOD__.'() - local images: '.array2csv($local_images));
		
		// Get current images on Datastore
		$images = Datastore::get('Image', array(
			array('domainLabel', '=', $domain_label)
		), array(
			'fields' => 'domainLabel, fileName, title, description, copyright, width, height, createdTime, lastUpdateTime'
		));

		// Create or update files that have been updated in Datastore
		$files_created = array();
		$files_updated = array();
		foreach ($images as $image) {
			$file_exists = in_array($image['fileName'], $local_images);

			// Ignore existing files that have not changed since last refresh
			if ($file_exists && $image['lastUpdateTime'] < $last_refresh_time) {
				continue;
			}

			// Fetch blob from DB
			$image = Datastore::getOne('Image', array(
				array('domainLabel', '=', $domain_label),
				array('fileName', '=', $image['fileName'])
			), array(
				'fields' => 'fileName, blob'
			));

			// Re-create img and thumbnail
			self::createImageAndThumbnail($image['blob'], $img_dir, $image['fileName']);

			// Append updated or created
			if ($file_exists) {
				$files_updated[] = $image['fileName'];
			}
			else {
				$files_created[] = $image['fileName'];	
			}
		} // foreach Datastore image

		// Delete images that have been removed in Datastore
		$files_deleted = array();
		foreach ($local_images as $local_image) {
			$exists = FALSE;
			foreach ($images as $image) {
				if ($local_image === $image['fileName']) {
					$exists = TRUE;
					break;
				}
			}
			if (!$exists) {
				self::deleteImageAndThumbnail($img_dir, $local_image);
				$files_deleted[] = $local_image;
			}
		} // foreach local image

		Cache::set('images-last-refresh-time', $time);

		return array(
			'images' => $images,
			'is-refreshed' => TRUE,
			'files-created' => $files_created,
			'files-updated' => $files_updated,
			'files-deleted' => $files_deleted,
			'last-refresh-time' => $time,
			'next-refresh-time' =>  $time + $refresh_period
		);
	} //attemptRefreshImages()

	/**
	 * Gets the paths to a directory for a given sub-domain.
	 * @param: {string} domain label e.g. "www"
	 * @return: {string | NULL} path to directory for this domain
	 */
	static function getHTMLPagesDirectory($domain_label) {
		if ($domain_label === 'www') {
			return WWW_ROOT . '/html/pages';
		}

		return NULL;
	}
	static function getHTMLTemplatesDirectory($domain_label) {
		if ($domain_label === 'www') {
			return WWW_ROOT . '/html/templates';
		}

		return NULL;
	}
	static function getImageUploadDirectory($domain_label) {
		if ($domain_label === 'www') {
			return WWW_ROOT . '/img/uploads';
		}

		return NULL;
	}
	static function getDatalistsDirectory($domain_label) {
		if ($domain_label === 'www') {
			return WWW_ROOT . '/data/lists';
		}

		return NULL;
	}

	static function getDocumentsDirectory($domain_label) {
		if ($domain_label === 'www') {
			return WWW_ROOT . '/doc';
		}

		return NULL;
	}

	/**
	 * Gets the meta for all pages
	 * @param: {string} domain label e.g. "www"
	 * @return: {array | NULL} associative array of meta
	 */
	static function readPagesMeta($domain_label) {
		if ($domain_label === 'www') {
			$root = WWW_ROOT;
		}
		else if ($domain_label === 'staff') {
			$root = STAFF_ROOT;
		}
		else {
			throw new Exception(__METHOD__.'() - unknown domain label: ' . $domain_label);
		}

		$pages_json = file_get_contents($root.'/private/pages.json');
		if (!$pages_json) {
			throw new Exception(__METHOD__.'() - unable to read from pages.json ('. $domain_label .')');
		}
		return json_decode($pages_json, TRUE); // format into assoc array
	} // readPagesMeta()

	/**
	 * @param: {string} domain label e.g. "www"
	 *         {array} meta - associative array of pages' meta
	 */
	static function updatePagesMeta($domain_label, $meta) {
		if ($domain_label === 'www') {
			$root = WWW_ROOT;
		}
		else if ($domain_label === 'staff') {
			$root = STAFF_ROOT;
		}
		else {
			throw new Exception(__METHOD__.'() - unknown domain label: ' . $domain_label);
		}

		$pages_json = json_encode($meta, JSON_PRETTY_PRINT);
		createFileRecursive($root.'/private/pages.json', $pages_json);
	} // updatePagesMeta()

	/**
	 * Moves an image to the uploads dir and creates a thumbnail
	 * - If file already exists in the upload dir, it will be overwritten
	 * @param: {string} blob           - innards of the actual image file
	 *         {string} img_dir        - e.g. which directory to store the image
	 *         {string} file_name      - e.g. my-picture.jpg
	 *         {int} detected_type     - e.g. IMAGETYPE_JPEG (optional - will auto-detect if omitted)
	 */
	static function createImageAndThumbnail($blob, $img_dir, $file_name, $detected_type=NULL) {
		$target_path = $img_dir . '/' . $file_name;
		$thumbnail_path = $img_dir . '/thumbnails/'.$file_name;

		// Init detected type
		if (!isset($detected_type)) {
			$detected_type = getimagesizefromstring($blob)[2];
		}

		// Move file
		createFileRecursive($target_path, $blob);
		
		// Create thumbnail
		$resize_options = array(
			'minWidth' => IMAGE_THUMBNAIL_MIN_WIDTH,
			'minHeight' =>IMAGE_THUMBNAIL_MIN_HEIGHT,
			'type' => $detected_type
		);
		$resized_blob = ImageProcessor::resize($blob, $resize_options);
		createFileRecursive($thumbnail_path, $resized_blob);
	} //createImageAndThumbnail()

	/**
	 * Removes an image and its thumbnail
	 * - Note: if file does not exist, assume successful delete (with logged error).
	 * @param: {string} img_dir        - e.g. directory where the image is stored
	 *         {string} file_name      - e.g. my-picture.jpg
	 */
	static function deleteImageAndThumbnail($img_dir, $file_name) {
		$target_path = $img_dir . '/' . $file_name;
		$thumbnail_path = $img_dir . '/thumbnails/'.$file_name;

		// Delete Image
		if (!file_exists($target_path)) {
			Log::error(__METHOD__.'() - trying to delete file that does not exist: '.$target_path);
		}
		else if (!unlink($target_path)) {
			Log::fatal(__METHOD__.'() - Unable to delete file: '.$target_path);
			throw new Exception('Unable to delete image '. $file_name);
		}
		
		// Delete Thumbnail
		if (!file_exists($thumbnail_path)) {
			Log::error(__METHOD__.'() - trying to delete thumbnail that does not exist: '.$thumbnail_path);
		}
		else if (!unlink($thumbnail_path)) {
			Log::fatal(__METHOD__.'() - Unable to delete file: '.$thumbnail_path);
			throw new Exception('Unable to delete thumbnail '. $file_name);
		}
	} //deleteImageAndThumbnail()

	/**
	 * Parses a JSON string representing DB conditions into arrays
	 * @param: {string}
	 * @return: {array | NULL} 3D array of OR conditions
	 */
	private static function parseConditions($conditions_str) {
		// Check conditions
		if (!isset($conditions_str)) {
			$conditions = array();
		}
		else {
			$conditions = json_decode($conditions_str, true);
		}
		if (is_null($conditions)) {
			Log::warning(__METHOD__.'() - conditions is not JSON.');
			return NULL;
		}
		foreach ($conditions as $or_index => $and_conditions) {
			if (!is_array($and_conditions)) {
				Log::warning(__METHOD__.'() - AND Condition ' . $or_index . ' is not an array. Expects each AND condition to be an array of conditions.');
				return NULL;
			}
			foreach ($and_conditions as $and_index => $condition) {
				if (!is_array($condition)) {
					Log::warning(__METHOD__.'() - Condition ' . $and_index . ' is not an array. Expects each condition to be an array of 3 elements.');
					return NULL;
				}
				if (!property_exists($_POST['dataType'], $condition[0])) {
					Log::warning(__METHOD__.'() - Condition ' . $and_index . ' does not match any property in ' . $_POST['dataType'] .' (' . $condition[0] .')');
					return NULL;
				}
			}
		}

		return $conditions;
	} //parseConditions()

	/************ End of Boilerplate for v2.1.0 ************/

	static function getTherapyImageDirectory($domain_label) {
		if ($domain_label === 'www') {
			return WWW_ROOT . '/img/therapies';
		}

		return NULL;
	}

	static function getCategoryImageDirectory($domain_label) {
		if ($domain_label === 'www') {
			return WWW_ROOT . '/img/categories';
		}

		return NULL;
	}

	static function getArticleImageDirectory($domain_label) {
		if ($domain_label === 'www') {
			return WWW_ROOT . '/img/articles';
		}

		return NULL;
	}

	static function getAccoladeImageDirectory($domain_label) {
		if ($domain_label === 'www') {
			return WWW_ROOT . '/img/accolades';
		}

		return NULL;
	}

	static function getAccoladeDocumentDirectory($domain_label) {
		if ($domain_label === 'www') {
			return WWW_ROOT . '/doc/accolades';
		}

		return NULL;	
	}
	static function getMediaImageDirectory($domain_label) {
		if ($domain_label === 'www') {
			return WWW_ROOT . '/img/medias';
		}

		return NULL;
	}

	static function getMediaDocumentDirectory($domain_label) {
		if ($domain_label === 'www') {
			return WWW_ROOT . '/doc/medias';
		}

		return NULL;	
	}

	/**
	 * Attempts a sync of therapy images with datastore
	 * - Note: whether data is refetched from datastore depends on THERAPY_IMAGES_REFRESH_PERIOD
	 * @param: {string} domain label, e.g. "www"
	 *         {bool} force - whether to force refresh
	 * @return: {array} 2D array of [is-refreshed, files-created, files-updated, files-deleted, last-refresh-time, next-refresh-time].
	 */
	static function attemptRefreshTherapyImages($domain_label, $force = FALSE) {
		// Check if we need to refresh now
		$time = getTimeInMs();
		$last_refresh_time = Cache::get('therapy-images-last-refresh-time');
		if (!isset($last_refresh_time)) {
			$last_refresh_time = 0;
		}
		$time_since_last_refresh = $time - $last_refresh_time;
		$refresh_period = THERAPY_IMAGES_REFRESH_PERIOD * 1000;
		if (!$force &&  $time_since_last_refresh < $refresh_period) {
			return array(
				'is-refreshed' => FALSE,
				'files-created' => array(),
				'files-updated' => array(),
				'files-deleted' => array(),
				'last-refresh-time' => $last_refresh_time,
				'next-refresh-time' =>  $last_refresh_time + $refresh_period
			);
		} // not yet time to refresh
		// Log::debug(__METHOD__.'() - refreshing therapy images...');

		// Get current files on this server
		$img_dir = self::getTherapyImageDirectory($domain_label);
		$local_sub_dirs = scandir($img_dir);
		$local_images = array();
		foreach ($local_sub_dirs as $idx => $local_sub_dir) {
			// Ignore dots and files
			if ($local_sub_dir === '.' || $local_sub_dir === '..') {
				continue;
			}
			$sub_dir_path = $img_dir. '/' . $local_sub_dir;
			if (!is_dir($sub_dir_path)) {
				continue;
			}

			// Go through this sub-directory
			$sub_dir_files = scandir($sub_dir_path);
			foreach ($sub_dir_files as $idx => $sub_dir_file) {
				if (is_dir($sub_dir_path. '/' . $sub_dir_file)) {
					continue;
				}
				$local_images[] = $local_sub_dir. '/' . $sub_dir_file;
			}
		}
		//Log::debug(__METHOD__.'() - local images: '.array2csv($local_images));
		
		// Get current images on Datastore
		$images = Datastore::get('TherapyImage', array(
		), array(
			'fields' => 'therapyID, fileName, width, height, createdTime, lastUpdateTime' // everything except blob
		));

		// Create or update files that have been updated in Datastore
		$files_created = array();
		$files_updated = array();
		foreach ($images as $image) {
			$file_exists = in_array($image['therapyID'] . '/' . $image['fileName'], $local_images);

			// Ignore existing files that have not changed since last refresh
			if ($file_exists && $image['lastUpdateTime'] < $last_refresh_time) {
				continue;
			}

			// Fetch blob from DB
			$image = Datastore::getOne('TherapyImage', array(
				array('therapyID', '=', $image['therapyID']),
				array('fileName', '=', $image['fileName'])
			), array(
				'fields' => 'therapyID, fileName, blob'
			));

			// Re-create img and thumbnail
			self::createImageAndThumbnail($image['blob'], $img_dir.'/'.$image['therapyID'], $image['fileName']);

			// Append updated or created
			if ($file_exists) {
				$files_updated[] = $image['therapyID'] . '/' .$image['fileName'];
			}
			else {
				$files_created[] = $image['therapyID'] . '/' .$image['fileName'];	
			}
		} // foreach Datastore image

		// Delete images that have been removed in Datastore
		$files_deleted = array();
		foreach ($local_images as $local_image) {
			$exists = FALSE;
			foreach ($images as $image) {
				if ($local_image === $image['therapyID'] . '/' . $image['fileName']) {
					$exists = TRUE;
					break;
				}
			}
			if (!$exists) {
				// self::deleteImageAndThumbnail($img_dir, $local_image);
				Log::debug(__METHOD__.'() - Should delete ' . $local_image);
				$files_deleted[] = $local_image;
			}
		} // foreach local image

		Cache::set('therapy-images-last-refresh-time', $time);

		return array(
			'images' => $images,
			'is-refreshed' => TRUE,
			'files-created' => $files_created,
			'files-updated' => $files_updated,
			'files-deleted' => $files_deleted,
			'last-refresh-time' => $time,
			'next-refresh-time' =>  $time + $refresh_period
		);
	} //attemptRefreshTherapyImages()

	/**
	 * Attempts a sync of category images with datastore
	 * - Note: whether data is refetched from datastore depends on CATEGORY_IMAGES_REFRESH_PERIOD
	 * @param: {string} domain label, e.g. "www"
	 *         {bool} force - whether to force refresh
	 * @return: {array} 2D array of [is-refreshed, files-created, files-updated, files-deleted, last-refresh-time, next-refresh-time].
	 */
	static function attemptRefreshCategoryImages($domain_label, $force = FALSE) {
		// Check if we need to refresh now
		$time = getTimeInMs();
		$last_refresh_time = Cache::get('category-images-last-refresh-time');
		if (!isset($last_refresh_time)) {
			$last_refresh_time = 0;
		}
		$time_since_last_refresh = $time - $last_refresh_time;
		$refresh_period = THERAPY_IMAGES_REFRESH_PERIOD * 1000;
		if (!$force &&  $time_since_last_refresh < $refresh_period) {
			return array(
				'is-refreshed' => FALSE,
				'files-created' => array(),
				'files-updated' => array(),
				'files-deleted' => array(),
				'last-refresh-time' => $last_refresh_time,
				'next-refresh-time' =>  $last_refresh_time + $refresh_period
			);
		} // not yet time to refresh
		// Log::debug(__METHOD__.'() - refreshing category images...');

		// Get current files on this server
		$img_dir = self::getCategoryImageDirectory($domain_label);
		$local_sub_dirs = scandir($img_dir);
		$local_images = array();
		foreach ($local_sub_dirs as $idx => $local_sub_dir) {
			// Ignore dots and files
			if ($local_sub_dir === '.' || $local_sub_dir === '..') {
				continue;
			}
			$sub_dir_path = $img_dir. '/' . $local_sub_dir;
			if (!is_dir($sub_dir_path)) {
				continue;
			}

			// Go through this sub-directory
			$sub_dir_files = scandir($sub_dir_path);
			foreach ($sub_dir_files as $idx => $sub_dir_file) {
				if (is_dir($sub_dir_path. '/' . $sub_dir_file)) {
					continue;
				}
				$local_images[] = $local_sub_dir. '/' . $sub_dir_file;
			}
		}
		//Log::debug(__METHOD__.'() - local images: '.array2csv($local_images));
		
		// Get current images on Datastore
		$images = Datastore::get('CategoryImage', array(
		), array(
			'fields' => 'categoryID, fileName, width, height, createdTime, lastUpdateTime' // everything except blob
		));

		// Create or update files that have been updated in Datastore
		$files_created = array();
		$files_updated = array();
		foreach ($images as $image) {
			$file_exists = in_array($image['categoryID'] . '/' . $image['fileName'], $local_images);

			// Ignore existing files that have not changed since last refresh
			if ($file_exists && $image['lastUpdateTime'] < $last_refresh_time) {
				continue;
			}

			// Fetch blob from DB
			$image = Datastore::getOne('CategoryImage', array(
				array('categoryID', '=', $image['categoryID']),
				array('fileName', '=', $image['fileName'])
			), array(
				'fields' => 'categoryID, fileName, blob'
			));

			// Re-create img and thumbnail
			self::createImageAndThumbnail($image['blob'], $img_dir.'/'.$image['categoryID'], $image['fileName']);

			// Append updated or created
			if ($file_exists) {
				$files_updated[] = $image['categoryID'] . '/' .$image['fileName'];
			}
			else {
				$files_created[] = $image['categoryID'] . '/' .$image['fileName'];	
			}
		} // foreach Datastore image

		// Delete images that have been removed in Datastore
		$files_deleted = array();
		foreach ($local_images as $local_image) {
			$exists = FALSE;
			foreach ($images as $image) {
				if ($local_image === $image['categoryID'] . '/' . $image['fileName']) {
					$exists = TRUE;
					break;
				}
			}
			if (!$exists) {
				// self::deleteImageAndThumbnail($img_dir, $local_image);
				Log::debug(__METHOD__.'() - Should delete ' . $local_image);
				$files_deleted[] = $local_image;
			}
		} // foreach local image

		Cache::set('category-images-last-refresh-time', $time);

		return array(
			'images' => $images,
			'is-refreshed' => TRUE,
			'files-created' => $files_created,
			'files-updated' => $files_updated,
			'files-deleted' => $files_deleted,
			'last-refresh-time' => $time,
			'next-refresh-time' =>  $time + $refresh_period
		);
	} // attemptRefreshCategoryImages()

	/**
	 * Attempts a sync of article images with datastore
	 * - Note: whether data is refetched from datastore depends on ARTICLE_IMAGES_REFRESH_PERIOD
	 * @param: {string} domain label, e.g. "www"
	 *         {bool} force - whether to force refresh
	 * @return: {array} 2D array of [is-refreshed, files-created, files-updated, files-deleted, last-refresh-time, next-refresh-time].
	 */
	static function attemptRefreshArticleImages($domain_label, $force = FALSE) {
		// Check if we need to refresh now
		$time = getTimeInMs();
		$last_refresh_time = Cache::get('article-images-last-refresh-time');
		if (!isset($last_refresh_time)) {
			$last_refresh_time = 0;
		}
		$time_since_last_refresh = $time - $last_refresh_time;
		$refresh_period = ARTICLE_IMAGES_REFRESH_PERIOD * 1000;
		if (!$force &&  $time_since_last_refresh < $refresh_period) {
			return array(
				'is-refreshed' => FALSE,
				'files-created' => array(),
				'files-updated' => array(),
				'files-deleted' => array(),
				'last-refresh-time' => $last_refresh_time,
				'next-refresh-time' =>  $last_refresh_time + $refresh_period
			);
		} // not yet time to refresh
		// Log::debug(__METHOD__.'() - refreshing article images...');

		// Get current files on this server
		$img_dir = self::getArticleImageDirectory($domain_label);
		$local_sub_dirs = scandir($img_dir);
		$local_images = array();
		foreach ($local_sub_dirs as $idx => $local_sub_dir) {
			// Ignore dots and files
			if ($local_sub_dir === '.' || $local_sub_dir === '..') {
				continue;
			}
			$sub_dir_path = $img_dir. '/' . $local_sub_dir;
			if (!is_dir($sub_dir_path)) {
				continue;
			}

			// Go through this sub-directory
			$sub_dir_files = scandir($sub_dir_path);
			foreach ($sub_dir_files as $idx => $sub_dir_file) {
				if (is_dir($sub_dir_path. '/' . $sub_dir_file)) {
					continue;
				}
				$local_images[] = $local_sub_dir. '/' . $sub_dir_file;
			}
		}
		//Log::debug(__METHOD__.'() - local images: '.array2csv($local_images));
		
		// Get current images on Datastore
		$images = Datastore::get('ArticleImage', array(
		), array(
			'fields' => 'articleID, fileName, width, height, createdTime, lastUpdateTime' // everything except blob
		));

		// Create or update files that have been updated in Datastore
		$files_created = array();
		$files_updated = array();
		foreach ($images as $image) {
			$file_exists = in_array($image['articleID'] . '/' . $image['fileName'], $local_images);

			// Ignore existing files that have not changed since last refresh
			if ($file_exists && $image['lastUpdateTime'] < $last_refresh_time) {
				continue;
			}

			// Fetch blob from DB
			$image = Datastore::getOne('ArticleImage', array(
				array('articleID', '=', $image['articleID']),
				array('fileName', '=', $image['fileName'])
			), array(
				'fields' => 'articleID, fileName, blob'
			));

			// Re-create img and thumbnail
			self::createImageAndThumbnail($image['blob'], $img_dir.'/'.$image['articleID'], $image['fileName']);

			// Append updated or created
			if ($file_exists) {
				$files_updated[] = $image['articleID'] . '/' .$image['fileName'];
			}
			else {
				$files_created[] = $image['articleID'] . '/' .$image['fileName'];	
			}
		} // foreach Datastore image

		// Delete images that have been removed in Datastore
		$files_deleted = array();
		foreach ($local_images as $local_image) {
			$exists = FALSE;
			foreach ($images as $image) {
				if ($local_image === $image['articleID'] . '/' . $image['fileName']) {
					$exists = TRUE;
					break;
				}
			}
			if (!$exists) {
				// self::deleteImageAndThumbnail($img_dir, $local_image);
				Log::debug(__METHOD__.'() - Should delete ' . $local_image);
				$files_deleted[] = $local_image;
			}
		} // foreach local image

		Cache::set('article-images-last-refresh-time', $time);

		return array(
			'images' => $images,
			'is-refreshed' => TRUE,
			'files-created' => $files_created,
			'files-updated' => $files_updated,
			'files-deleted' => $files_deleted,
			'last-refresh-time' => $time,
			'next-refresh-time' =>  $time + $refresh_period
		);
	} // attemptRefreshArticleImages()

	/**
	 * Attempts a sync of accolade images with datastore
	 * - Note: whether data is refetched from datastore depends on ACCOLADE_IMAGES_REFRESH_PERIOD
	 * @param: {string} domain label, e.g. "www"
	 *         {bool} force - whether to force refresh
	 * @return: {array} 2D array of [is-refreshed, files-created, files-updated, files-deleted, last-refresh-time, next-refresh-time].
	 */
	static function attemptRefreshAccoladeImages($domain_label, $force = FALSE) {
		// Check if we need to refresh now
		$time = getTimeInMs();
		$last_refresh_time = Cache::get('accolade-images-last-refresh-time');
		if (!isset($last_refresh_time)) {
			$last_refresh_time = 0;
		}
		$time_since_last_refresh = $time - $last_refresh_time;
		$refresh_period = ACCOLADE_IMAGES_REFRESH_PERIOD * 1000;
		if (!$force &&  $time_since_last_refresh < $refresh_period) {
			return array(
				'is-refreshed' => FALSE,
				'files-created' => array(),
				'files-updated' => array(),
				'files-deleted' => array(),
				'last-refresh-time' => $last_refresh_time,
				'next-refresh-time' =>  $last_refresh_time + $refresh_period
			);
		} // not yet time to refresh
		// Log::debug(__METHOD__.'() - refreshing accolade images...');

		// Get current files on this server
		$img_dir = self::getAccoladeImageDirectory($domain_label);
		$local_sub_dirs = scandir($img_dir);
		$local_images = array();
		foreach ($local_sub_dirs as $idx => $local_sub_dir) {
			// Ignore dots and files
			if ($local_sub_dir === '.' || $local_sub_dir === '..') {
				continue;
			}
			$sub_dir_path = $img_dir. '/' . $local_sub_dir;
			if (!is_dir($sub_dir_path)) {
				continue;
			}

			// Go through this sub-directory
			$sub_dir_files = scandir($sub_dir_path);
			foreach ($sub_dir_files as $idx => $sub_dir_file) {
				if (is_dir($sub_dir_path. '/' . $sub_dir_file)) {
					continue;
				}
				$local_images[] = $local_sub_dir. '/' . $sub_dir_file;
			}
		}
		//Log::debug(__METHOD__.'() - local images: '.array2csv($local_images));
		
		// Get current images on Datastore
		$images = Datastore::get('AccoladeImage', array(
		), array(
			'fields' => 'accoladeID, fileName, width, height, createdTime, lastUpdateTime' // everything except blob
		));

		// Create or update files that have been updated in Datastore
		$files_created = array();
		$files_updated = array();
		foreach ($images as $image) {
			$file_exists = in_array($image['accoladeID'] . '/' . $image['fileName'], $local_images);

			// Ignore existing files that have not changed since last refresh
			if ($file_exists && $image['lastUpdateTime'] < $last_refresh_time) {
				continue;
			}

			// Fetch blob from DB
			$image = Datastore::getOne('AccoladeImage', array(
				array('accoladeID', '=', $image['accoladeID']),
				array('fileName', '=', $image['fileName'])
			), array(
				'fields' => 'accoladeID, fileName, blob'
			));

			// Re-create img and thumbnail
			self::createImageAndThumbnail($image['blob'], $img_dir.'/'.$image['accoladeID'], $image['fileName']);

			// Append updated or created
			if ($file_exists) {
				$files_updated[] = $image['accoladeID'] . '/' .$image['fileName'];
			}
			else {
				$files_created[] = $image['accoladeID'] . '/' .$image['fileName'];	
			}
		} // foreach Datastore image

		// Delete images that have been removed in Datastore
		$files_deleted = array();
		foreach ($local_images as $local_image) {
			$exists = FALSE;
			foreach ($images as $image) {
				if ($local_image === $image['accoladeID'] . '/' . $image['fileName']) {
					$exists = TRUE;
					break;
				}
			}
			if (!$exists) {
				// self::deleteImageAndThumbnail($img_dir, $local_image);
				Log::debug(__METHOD__.'() - Should delete ' . $local_image);
				$files_deleted[] = $local_image;
			}
		} // foreach local image

		Cache::set('accolade-images-last-refresh-time', $time);

		return array(
			'images' => $images,
			'is-refreshed' => TRUE,
			'files-created' => $files_created,
			'files-updated' => $files_updated,
			'files-deleted' => $files_deleted,
			'last-refresh-time' => $time,
			'next-refresh-time' =>  $time + $refresh_period
		);
	} // attemptRefreshAccoladeImages()

        
        
	/**
	 * Attempts a sync of accolade images with datastore
	 * - Note: whether data is refetched from datastore depends on ACCOLADE_DOCUMENTS_REFRESH_PERIOD
	 * @param: {string} domain label, e.g. "www"
	 *         {bool} force - whether to force refresh
	 * @return: {array} 2D array of [is-refreshed, files-created, files-updated, files-deleted, last-refresh-time, next-refresh-time].
	 */
	static function attemptRefreshAccoladeDocuments($domain_label, $force = FALSE) {
		// Check if we need to refresh now
		$time = getTimeInMs();
		$last_refresh_time = Cache::get('accolade-documents-last-refresh-time');
		if (!isset($last_refresh_time)) {
			$last_refresh_time = 0;
		}
		$time_since_last_refresh = $time - $last_refresh_time;
		$refresh_period = ACCOLADE_DOCUMENTS_REFRESH_PERIOD * 1000;
		if (!$force &&  $time_since_last_refresh < $refresh_period) {
			return array(
				'is-refreshed' => FALSE,
				'files-created' => array(),
				'files-updated' => array(),
				'files-deleted' => array(),
				'last-refresh-time' => $last_refresh_time,
				'next-refresh-time' =>  $last_refresh_time + $refresh_period
			);
		} // not yet time to refresh
		// Log::debug(__METHOD__.'() - refreshing accolade docs...');

		// Get current files on this server
		$doc_dir = self::getAccoladeDocumentDirectory($domain_label);
		$local_sub_dirs = scandir($doc_dir);
		$local_docs = array();
		foreach ($local_sub_dirs as $idx => $local_sub_dir) {
			// Ignore dots and files
			if ($local_sub_dir === '.' || $local_sub_dir === '..') {
				continue;
			}
			$sub_dir_path = $doc_dir. '/' . $local_sub_dir;
			if (!is_dir($sub_dir_path)) {
				continue;
			}

			// Go through this sub-directory
			$sub_dir_files = scandir($sub_dir_path);
			foreach ($sub_dir_files as $idx => $sub_dir_file) {
				if (is_dir($sub_dir_path. '/' . $sub_dir_file)) {
					continue;
				}
				$local_docs[] = $local_sub_dir. '/' . $sub_dir_file;
			}
		}
		//Log::debug(__METHOD__.'() - local docs: '.array2csv($local_docs));

		// Get current docs on Datastore
		$docs = Datastore::get('AccoladeDocument', array(
		), array(
			'fields' => 'accoladeID, fileName, createdTime, lastUpdateTime' // everything except blob
		));

		// Create or update files that have been updated in Datastore
		$files_created = array();
		$files_updated = array();
		foreach ($docs as $doc) {
			$file_exists = in_array($doc['accoladeID'] . '/' . $doc['fileName'], $local_docs);

			// Ignore existing files that have not changed since last refresh
			if ($file_exists && $doc['lastUpdateTime'] < $last_refresh_time) {
				continue;
			}

			// Fetch blob from DB
			$doc = Datastore::getOne('AccoladeDocument', array(
				array('accoladeID', '=', $doc['accoladeID']),
				array('fileName', '=', $doc['fileName'])
			), array(
				'fields' => 'accoladeID, fileName, blob'
			));

			// Re-create doc
			createFileRecursive($doc_dir . '/' . $doc['accoladeID'] . '/' . $doc['fileName'], $doc['blob']);

			// Append updated or created
			if ($file_exists) {
				$files_updated[] = $doc['accoladeID'] . '/' .$doc['fileName'];
			}
			else {
				$files_created[] = $doc['accoladeID'] . '/' .$doc['fileName'];	
			}
		} // foreach Datastore doc

		// Delete docs that have been removed in Datastore
		$files_deleted = array();
		foreach ($local_docs as $local_doc) {
			$exists = FALSE;
			foreach ($docs as $doc) {
				if ($local_doc === $doc['accoladeID'] . '/' . $doc['fileName']) {
					$exists = TRUE;
					break;
				}
			}
			if (!$exists) {
				//unlink($doc_dir . '/' . $local_doc);
				Log::debug(__METHOD__.'() - Should delete ' . $local_doc);
				$files_deleted[] = $local_doc;
			}
		} // foreach local doc

		Cache::set('accolade-documents-last-refresh-time', $time);

		return array(
			'docs' => $docs,
			'is-refreshed' => TRUE,
			'files-created' => $files_created,
			'files-updated' => $files_updated,
			'files-deleted' => $files_deleted,
			'last-refresh-time' => $time,
			'next-refresh-time' =>  $time + $refresh_period
		);
	} // attemptRefreshAccoladeDocuments()
	
        
        
	/**
	 * Attempts a sync of media images with datastore
	 * - Note: whether data is refetched from datastore depends on MEDIA_IMAGES_REFRESH_PERIOD
	 * @param: {string} domain label, e.g. "www"
	 *         {bool} force - whether to force refresh
	 * @return: {array} 2D array of [is-refreshed, files-created, files-updated, files-deleted, last-refresh-time, next-refresh-time].
	 */
	static function attemptRefreshMediaImages($domain_label, $force = FALSE) {
		// Check if we need to refresh now
		$time = getTimeInMs();
		$last_refresh_time = Cache::get('media-images-last-refresh-time');
		if (!isset($last_refresh_time)) {
			$last_refresh_time = 0;
		}
		$time_since_last_refresh = $time - $last_refresh_time;
		$refresh_period = MEDIA_IMAGES_REFRESH_PERIOD * 1000;
		if (!$force &&  $time_since_last_refresh < $refresh_period) {
			return array(
				'is-refreshed' => FALSE,
				'files-created' => array(),
				'files-updated' => array(),
				'files-deleted' => array(),
				'last-refresh-time' => $last_refresh_time,
				'next-refresh-time' =>  $last_refresh_time + $refresh_period
			);
		} // not yet time to refresh
		// Log::debug(__METHOD__.'() - refreshing media images...');

		// Get current files on this server
		$img_dir = self::getMediaImageDirectory($domain_label);
		$local_sub_dirs = scandir($img_dir);
		$local_images = array();
		foreach ($local_sub_dirs as $idx => $local_sub_dir) {
			// Ignore dots and files
			if ($local_sub_dir === '.' || $local_sub_dir === '..') {
				continue;
			}
			$sub_dir_path = $img_dir. '/' . $local_sub_dir;
			if (!is_dir($sub_dir_path)) {
				continue;
			}

			// Go through this sub-directory
			$sub_dir_files = scandir($sub_dir_path);
			foreach ($sub_dir_files as $idx => $sub_dir_file) {
				if (is_dir($sub_dir_path. '/' . $sub_dir_file)) {
					continue;
				}
				$local_images[] = $local_sub_dir. '/' . $sub_dir_file;
			}
		}
		//Log::debug(__METHOD__.'() - local images: '.array2csv($local_images));
		
		// Get current images on Datastore
		$images = Datastore::get('MediaImage', array(
		), array(
			'fields' => 'mediaID, fileName, width, height, createdTime, lastUpdateTime' // everything except blob
		));

		// Create or update files that have been updated in Datastore
		$files_created = array();
		$files_updated = array();
		foreach ($images as $image) {
			$file_exists = in_array($image['mediaID'] . '/' . $image['fileName'], $local_images);

			// Ignore existing files that have not changed since last refresh
			if ($file_exists && $image['lastUpdateTime'] < $last_refresh_time) {
				continue;
			}

			// Fetch blob from DB
			$image = Datastore::getOne('MediaImage', array(
				array('mediaID', '=', $image['mediaID']),
				array('fileName', '=', $image['fileName'])
			), array(
				'fields' => 'mediaID, fileName, blob'
			));

			// Re-create img and thumbnail
			self::createImageAndThumbnail($image['blob'], $img_dir.'/'.$image['mediaID'], $image['fileName']);

			// Append updated or created
			if ($file_exists) {
				$files_updated[] = $image['mediaID'] . '/' .$image['fileName'];
			}
			else {
				$files_created[] = $image['mediaID'] . '/' .$image['fileName'];	
			}
		} // foreach Datastore image

		// Delete images that have been removed in Datastore
		$files_deleted = array();
		foreach ($local_images as $local_image) {
			$exists = FALSE;
			foreach ($images as $image) {
				if ($local_image === $image['mediaID'] . '/' . $image['fileName']) {
					$exists = TRUE;
					break;
				}
			}
			if (!$exists) {
				// self::deleteImageAndThumbnail($img_dir, $local_image);
				Log::debug(__METHOD__.'() - Should delete ' . $local_image);
				$files_deleted[] = $local_image;
			}
		} // foreach local image

		Cache::set('media-images-last-refresh-time', $time);

		return array(
			'images' => $images,
			'is-refreshed' => TRUE,
			'files-created' => $files_created,
			'files-updated' => $files_updated,
			'files-deleted' => $files_deleted,
			'last-refresh-time' => $time,
			'next-refresh-time' =>  $time + $refresh_period
		);
	} // attemptRefreshMediaImages()
        
        /**
	 * Attempts a sync of media images with datastore
	 * - Note: whether data is refetched from datastore depends on MEDIA_DOCUMENTS_REFRESH_PERIOD
	 * @param: {string} domain label, e.g. "www"
	 *         {bool} force - whether to force refresh
	 * @return: {array} 2D array of [is-refreshed, files-created, files-updated, files-deleted, last-refresh-time, next-refresh-time].
	 */
	static function attemptRefreshMediaDocuments($domain_label, $force = FALSE) {
		// Check if we need to refresh now
		$time = getTimeInMs();
		$last_refresh_time = Cache::get('media-documents-last-refresh-time');
		if (!isset($last_refresh_time)) {
			$last_refresh_time = 0;
		}
		$time_since_last_refresh = $time - $last_refresh_time;
		$refresh_period = MEDIA_DOCUMENTS_REFRESH_PERIOD * 1000;
		if (!$force &&  $time_since_last_refresh < $refresh_period) {
			return array(
				'is-refreshed' => FALSE,
				'files-created' => array(),
				'files-updated' => array(),
				'files-deleted' => array(),
				'last-refresh-time' => $last_refresh_time,
				'next-refresh-time' =>  $last_refresh_time + $refresh_period
			);
		} // not yet time to refresh
		// Log::debug(__METHOD__.'() - refreshing media docs...');

		// Get current files on this server
		$doc_dir = self::getMediaDocumentDirectory($domain_label);
		$local_sub_dirs = scandir($doc_dir);
		$local_docs = array();
		foreach ($local_sub_dirs as $idx => $local_sub_dir) {
			// Ignore dots and files
			if ($local_sub_dir === '.' || $local_sub_dir === '..') {
				continue;
			}
			$sub_dir_path = $doc_dir. '/' . $local_sub_dir;
			if (!is_dir($sub_dir_path)) {
				continue;
			}

			// Go through this sub-directory
			$sub_dir_files = scandir($sub_dir_path);
			foreach ($sub_dir_files as $idx => $sub_dir_file) {
				if (is_dir($sub_dir_path. '/' . $sub_dir_file)) {
					continue;
				}
				$local_docs[] = $local_sub_dir. '/' . $sub_dir_file;
			}
		}
		//Log::debug(__METHOD__.'() - local docs: '.array2csv($local_docs));

		// Get current docs on Datastore
		$docs = Datastore::get('MediaDocument', array(
		), array(
			'fields' => 'mediaID, fileName, createdTime, lastUpdateTime' // everything except blob
		));

		// Create or update files that have been updated in Datastore
		$files_created = array();
		$files_updated = array();
		foreach ($docs as $doc) {
			$file_exists = in_array($doc['mediaID'] . '/' . $doc['fileName'], $local_docs);

			// Ignore existing files that have not changed since last refresh
			if ($file_exists && $doc['lastUpdateTime'] < $last_refresh_time) {
				continue;
			}

			// Fetch blob from DB
			$doc = Datastore::getOne('MediaDocument', array(
				array('mediaID', '=', $doc['mediaID']),
				array('fileName', '=', $doc['fileName'])
			), array(
				'fields' => 'mediaID, fileName, blob'
			));

			// Re-create doc
			createFileRecursive($doc_dir . '/' . $doc['mediaID'] . '/' . $doc['fileName'], $doc['blob']);

			// Append updated or created
			if ($file_exists) {
				$files_updated[] = $doc['mediaID'] . '/' .$doc['fileName'];
			}
			else {
				$files_created[] = $doc['mediaID'] . '/' .$doc['fileName'];	
			}
		} // foreach Datastore doc

		// Delete docs that have been removed in Datastore
		$files_deleted = array();
		foreach ($local_docs as $local_doc) {
			$exists = FALSE;
			foreach ($docs as $doc) {
				if ($local_doc === $doc['mediaID'] . '/' . $doc['fileName']) {
					$exists = TRUE;
					break;
				}
			}
			if (!$exists) {
				//unlink($doc_dir . '/' . $local_doc);
				Log::debug(__METHOD__.'() - Should delete ' . $local_doc);
				$files_deleted[] = $local_doc;
			}
		} // foreach local doc

		Cache::set('media-documents-last-refresh-time', $time);

		return array(
			'docs' => $docs,
			'is-refreshed' => TRUE,
			'files-created' => $files_created,
			'files-updated' => $files_updated,
			'files-deleted' => $files_deleted,
			'last-refresh-time' => $time,
			'next-refresh-time' =>  $time + $refresh_period
		);
	} // attemptRefreshMediaDocuments()
        
        

} // class InternalAPI
