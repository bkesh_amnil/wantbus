<?php
/****
 * Backend Framework v2.1.0 (Edited)
 * ========================
 *
 * Functions called by Staff users
 ***/
class StaffAPI {
	/**
	 * Attempts to set the staff as the session user.
	 * The Session ID will be passed to the browser in the background, and will be used to identify this session for as long as the browser is open.
	 * @input: $_POST['email', 'password', 'persistent']
	 * @output: staff
	 *          persistent-login-token (optional)
	 * on fail: password-attempts-remaining
	 */
	function login($response) {
		$_POST['userType'] = 'Staff';
		return (new InternalAPI())->login($response);
	} //login()
	
	/**
	 * Attempts to set the staff as the session user using a persistent cookie.
	 * A new persistent cookie will be issued.
	 * The Session ID will be passed to the browser in the background, and will be used to identify this session for as long as the browser is open.
	 * @input: $_POST['email', 'persistent-login-token']
	 * @output: staff
	 *          persistent-login-token
	 */
	function persistentLogin($response) {
		$_POST['userType'] = 'Staff';
		return (new InternalAPI())->persistentLogin($response);
	} //persistentLogin()

	/**
	 * Logout a staff
	 * Any existing Session ID is cleared and a new session will be started
	 * @input: -
	 * @output: -
	 */
	function logout($response) {
		$_POST['userType'] = 'Staff';
		return (new InternalAPI())->logout($response);
	} //logout()

	/**
	 * Changes password of session staff
	 * - must be logged in
	 * @input: $_POST['old-password, new-password']
	 * @output: -
	 */
	function changePassword($response) {
		$_POST['userType'] = 'Staff';
		return (new InternalAPI())->changePassword($response);
	} //changePassword()

	/**
	 * Sends an OTP to user's email, and sets a new time-sensitive email verification hash. To be followed up with verifyEmail request.
	 * @input: $_POST['email']
	 * @output: -
	 */
	function forgetPassword($response) {
		$_POST['userType'] = 'Staff';
		$_POST['url'] = STAFF_URL . '/login';
		return (new InternalAPI())->forgetPassword($response);
	}

	/**
	 * Verify email with a one-time passcode (part of forget password process)
	 * - On success, user will be considered authenticated (logged in), with his password randomised and returned. Expect to follow up with changePassword request.
	 * @input: $_POST['email', 'code']
	 * @output: staff,
	 *          password,
	 *          csrf-token
	 * on fail: otp-attempts-remaining
	 */
	function verifyEmail($response) {
		$_POST['userType'] = 'Staff';
		return (new InternalAPI())->verifyEmail($response);
	} // verifyEmail()

	/**
	 * Edits account data of session staff.
	 * @input: $_POST['name, phone, gender, birthDay, birthMonth, birthYear']
	 * @output: staff
	 */
	function editAccount($response) {
		// Ensure action is legal - check that the user is indeed a staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff');
			return 401;
		}
		
		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check name
		if (!isset($_POST['name']) || !Validator::isName($_POST['name'])) {
			$name = NULL;
		}
		else {
			$name = trim($_POST['name']);
		}

		// Check phone
		if (!isset($_POST['phone']) || !Validator::isPhone($_POST['phone'])) {
			$phone = NULL;
		}
		else {
			$phone = $_POST['phone'];
		}

		// Check gender
		$gender_options = InternalAPI::readDatalist('genders');
		if (!isset($_POST['gender']) || !Validator::isValidOptionKey($_POST['gender'], $gender_options)) {
			$gender = NULL;
		}
		else {
			$gender = $_POST['gender'];
		}

		// Check birthdate
		if (!isset($_POST['birthDay']) || !isset($_POST['birthMonth']) || !isset($_POST['birthYear']) ||
			!Validator::isDate($_POST['birthYear'], $_POST['birthMonth'], $_POST['birthDay'])) {
			$birthDay = NULL;
			$birthMonth = NULL;
			$birthYear = NULL;
		}
		else {
			$birthDay = $_POST['birthDay'];
			$birthMonth = $_POST['birthMonth'];
			$birthYear = $_POST['birthYear'];
		}

		// Transaction for ACID-ity
		$user_id = $_SESSION['user']->getID();
		$action = 'Edited Account';
		$committed = FALSE;
		while (!$committed) {
			$time = getTimeInMs();

			Database::beginTransaction();
			
			// Read from DB
			$user = Database::readObjectByID('Staff', $user_id, array(
				'update' => TRUE
			));
			if (!isset($user)) {
				Log::fatal(__METHOD__.'() - unable to read session staff ' . $user_id . ' from DB');
				$response->addData('error', 'Unable to read user from DB.');
				return 500;
			}

			// Update user
			$user->update(array(
				'name' => $name,
				'phone' => $phone,
				'gender' => $gender,
				'birthDay' => $birthDay,
				'birthMonth' => $birthMonth,
				'birthYear' => $birthYear,
				'lastUpdateTime' => $time
			));
			if (!Database::update($user)) {
				Log::fatal(__METHOD__.'() - unable to update session staff ' . $user_id);
				$response->addData('error', 'Unable to save to DB.');
				return 500;
			}

			$committed = Database::endTransaction();
		} // while not committed

		// Store this activity
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => 'Staff',
                            'action' => $action,
                            'createdTime' => $time
                    ));
                }

		$_SESSION['user'] = $user;
		$response->addData('staff', $user->getSanitizedArray());
		return 200;
	} //editAccount()

	/**
	 * @input: -
	 * @output: activity
	 */
	function getLastActivity($response) {
		// Ensure action is legal - check that the user is indeed a staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff');
			return 401;
		}
		
		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		$user_id = $_SESSION['user']->getID();
		$activities = Datastore::get('Activity', array(
			array('userID', '=', $user_id),
			array('userType', '=', 'Staff')
		), array(
			'count' => 1,
			'sort' => 'createdTime',
			'order' => 'DESC'
		));
		if (count($activities) === 0) {
			Log::warning(__METHOD__.'() - no last activity found for Staff ' . $user_id);
			return 200;
		}

		$response->addData('activity', $activities[0]);
		return 200;
	} //getLastActivity()

	/**
	 * @input: $_GET['offset, count']
	 * @output: activities, total-count
	 */
	function getActivities($response) {
            if(LOCAL ==1){
                $response->addData('activity-count', $activity_count);
		return 200;
            }
		// Ensure action is legal - check that the user is indeed a staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff');
			return 401;
		}
		
		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check input
		if (!isset($_GET['offset']) || !isUnsignedInt($_GET['offset'])) {
			$_GET['offset'] = 0;
		}
		if (!isset($_GET['count']) || !isUnsignedInt($_GET['count'])) {
			$_GET['count'] = 10;
		}

		// Check activity count
		$user_id = $_SESSION['user']->getID();
		$activity_count = Datastore::count('Activity', array(
			array('userID', '=', $user_id),
			array('userType', '=', 'Staff')
		));
		if ($activity_count < $_GET['offset']) {
			$response->addData('error', 'Activity offset too high.');
			return 400;
		}

		// Read activities	
		$desired_count = $_GET['offset'] + $_GET['count'];
		$activities = Datastore::get('Activity', array(
			array('userID', '=', $user_id),
			array('userType', '=', 'Staff')
		), array(
			'count' => $desired_count, // no offset in CQL
			'sort' => 'createdTime',
			'order' => 'DESC'
		));
		$actual_count = count($activities);
		if ($actual_count <= $_GET['offset']) {
			$response->addData('error', 'No activities found.');
			return 404;
		}

		$return_data = array();
		for ($i=$_GET['offset']; $i<$actual_count; $i++) {
			$return_data[] = $activities[$i];
		}

		$response->addData('activities', $return_data);
		$response->addData('activity-count', $activity_count);
		return 200;
	} //getActivities()

	/**
	* Change application setting(s)
	* - POST keys that do not match any setting key are ignored.
	* @input: $_POST[ ... ] (various key values)
	* @output: settings - Array of all settings
	*/
	function editSettings($response) {
		// Ensure action is legal - check that the user is indeed a staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff');
			return 401;
		}
		
		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check permissions
		if ($_SESSION['user']->getAccessLevel() < ADMIN_ACCESS_LEVEL) {
			$response->setData(array('error' => 'Insufficient access level.'));
			return 403;
		}

		// Transaction Data
		$user_id = $_SESSION['user']->getID();
		$action = 'Edited Setting';
		$time = getTimeInMs();

		$change_remarks = '';
		$ignored_keys = array();
		$unchanged_keys = array();

		// Go through each POST setting
		$settings = InternalAPI::readSettings(TRUE); // force fetch from Datastore
		foreach ($_POST as $key => $value) {
			$found = FALSE;

			// Ignore keys that don't already exist in current settings
			if (!array_key_exists($key, $settings)) {
				$ignored_keys[] = $key;
				continue;
			}

			// Check if we need to update the setting's value
			$curr_value = $settings[$key];
			if ($curr_value === $value) {
				$unchanged_keys[] = $key;
				continue;
			}
                        
			// Update the setting value
                        if(LOCAL != 1){
                            Datastore::set('Setting', array(
                                    'key' => $key,
                                    'value' => $value,
                                    'lastUpdateTime' => $time
                            ));
                        }
			$change_remarks .= $key.': "'.$curr_value.'" --> "'.$value .'";';
		} // foreach POST key		

		// Store this activity
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => 'Staff',
                            'action' => $action,
                            'createdTime' => $time,
                            'remarks' => $change_remarks
                    ));
                }

		// Return settings
		$response->addData('ignored', $ignored_keys);
		$response->addData('unchanged', $unchanged_keys);
		$response->addData('settings', $settings);
		return 200;
	} //editSettings()

	/**
	 * Gets the total number of staffs in the system, broken down by access level. Useful for quick overview.
	 * #input: -
	 * @output: general, supervisor, admin, superadmin
	 */
	function getStaffStats($response) {
		// Ensure action is legal - check that the user is indeed a staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff');
			return 401;
		}
		
		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Read stats from DB
		$general_count = Database::count('Staff', array(
			array(
				array('accessLevel', '=', GENERAL_STAFF_ACCESS_LEVEL)
			)
		));
		$supervisor_count = Database::count('Staff', array(
			array(
				array('accessLevel', '=', SUPERVISOR_ACCESS_LEVEL)
			)
		));
		$admin_count = Database::count('Staff', array(
			array(
				array('accessLevel', '=', ADMIN_ACCESS_LEVEL)
			)
		));
		$superadmin_count = Database::count('Staff', array(
			array(
				array('accessLevel', '=', SUPER_ADMIN_ACCESS_LEVEL)
			)
		));

		$response->addData('general', $general_count);
		$response->addData('supervisor', $supervisor_count);
		$response->addData('admin', $admin_count);
		$response->addData('superadmin', $superadmin_count);
		return 200;
	} //getStaffStats()

	/**
	 * Note: conditions is read as a JSON string representing a 3D array of OR conditions.
	 *       when parsed, each condition should be an array of 3 elements - representing KEY, OPERAND, VALUE.
	 * Note: order must be "ASC" or "DESC"
	 * Note: search is case insensitive by default (COLLATE utf8_unicode_ci)
	 * @input: $_GET['conditions, offset, count, sort, order, sort2, order2']
	 * @putput: Array of member
	 */
	function getStaffs($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		$_POST['userType'] = 'Staff';
		$_POST['dataType'] = 'Staff';
		$_POST['dataTypePlural'] = 'Staffs';

		return (new InternalAPI())->getData($response);
	} //getStaffs()

	/**
	 * Adds a staff user
	 * @input: $_POST['name, phone, gender, email, password, access-level']
	 *         $_FILES['img'] (optional)
	 * @output: staff
	 */
	function addStaff($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check Permissions
		if ($_SESSION['user']->getAccessLevel() < ADMIN_ACCESS_LEVEL) {
			$response->setData(array('error' => 'Insufficient access level.'));
			return 403;
		}

		// Check name
		if (!isset($_POST['name']) || !Validator::isName($_POST['name'])) {
			$name = NULL;
		}
		else {
			$name = trim($_POST['name']);
		}

		// Check phone
		if (!isset($_POST['phone']) || !Validator::isPhone($_POST['phone'])) {
			$phone = NULL;
		}
		else {
			$phone = $_POST['phone'];
		}

		// Check gender
		$gender_options = InternalAPI::readDatalist('genders');
		if (!isset($_POST['gender']) || !Validator::isValidOptionKey($_POST['gender'], $gender_options)) {
			$gender = NULL;
		}
		else {
			$gender = $_POST['gender'];
		}

		// Check birthdate
		if (!isset($_POST['birthDay']) || !isset($_POST['birthMonth']) || !isset($_POST['birthYear']) ||
			!Validator::isDate($_POST['birthYear'], $_POST['birthMonth'], $_POST['birthDay'])) {
			$birthDay = NULL;
			$birthMonth = NULL;
			$birthYear = NULL;
		}
		else {
			$birthDay = $_POST['birthDay'];
			$birthMonth = $_POST['birthMonth'];
			$birthYear = $_POST['birthYear'];
		}

		// Check email
		if (!isset($_POST['email'])) {
			$response->addData('error', 'Email not specified.');
			return 400;
		}
		if (!Validator::isEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email.');
			return 400;
		}
		$email = $_POST['email'];

		// Check password validity and generate hash
		if (!isset($_POST['password'])) {
			$response->addData('error', 'Password not specified.');
			return 400;
		}
		if (!Validator::isStrongPassword($_POST['password'])) {
			$response->addData('error', 'Password is not strong enough.');
			return 400;
		}
		$passwordHash = Encryptor::getHash($_POST['password']);

		// Check access level
		if (!isset($_POST['accessLevel']) || !isUnsignedInt($_POST['accessLevel'])) {
			$response->addData('error', 'Invalid access level');
			return 400;
		}
		$access_level = (int) $_POST['accessLevel'];

		// Check if img file was uploaded
		$has_img_file_upload = TRUE;
		if (!isset($_FILES['img']['tmp_name'])) {
			$has_img_file_upload = FALSE;
		}
		else if ($_FILES['img']['error'] > 0) {
			if ($_FILES['img']['error'] !== 4) { // 4: no file uploaded
				Log::warning(__METHOD__.'() - img appears uploaded but there were errors: '.$_FILES['img']['error']);
			}
			$has_img_file_upload = FALSE;
		}
		if ($has_img_file_upload) {
			$size = getimagesize($_FILES['img']['tmp_name']); // size = [width, height, detected_type]
			if (!$size) {
				$response->addData('error', 'Unknown file type.');
				return 400;
			}
			if ($size[2] == IMAGETYPE_PNG || $size[2] == IMAGETYPE_GIF) {
				// convert to JPEG
				$blob = file_get_contents($_FILES['img']['tmp_name']);
				$blob = ImageProcessor::crop($blob, array(
					'type' => IMAGETYPE_JPEG
				));
			}
			else if ($size[2] == IMAGETYPE_JPEG) {
				$blob = file_get_contents($_FILES['img']['tmp_name']);
			}
			else {
				$response->addData('error', 'Unknown file type.');
				return 400;
			}
		} // has file upload
		else {
			$size = getimagesize(API_ROOT.'/assets/staffs/0/profile.jpg');
			$blob = file_get_contents(API_ROOT.'/assets/staffs/0/profile.jpg');
		}

		// Transaction for ACID-ity
		$user_id = $_SESSION['user']->getID();
		$action = 'Added Staff';
		$committed = FALSE;
		while (!$committed) {
			$time = getTimeInMs();

			// Create Staff - can be done outside transaction as we don't need persistence (yet)
			try {
				$staff = new Staff(array(
					'name' => $name,
					'phone' => $phone,
					'gender' => $gender,
					'birthDay' => $birthDay,
					'birthMonth' => $birthMonth,
					'birthYear' => $birthYear,
					'accessLevel' => $access_level,
					'email' => $email,
					'emailVerified' => TRUE,
					'passwordHash' => $passwordHash,
					'createdTime' => $time,
					'lastUpdateTime' => $time
				));
			}
			catch (Exception $e) {
				$response->addData('error', $e->getMessage());
				return 400;
			}

			Database::beginTransaction();

			// Create it
			if (!Database::create($staff)) {
				Log::warning(__METHOD__.'() - DB Unable to create Staff ' . $email);
				$response->addData('error', 'Unable to create Staff. Possible email conflict.');
				return 400;
			}

			$committed = Database::endTransaction();
		} // while not committed

		$staff_id = $staff->getID();

		// Add profile picture
                if(LOCAL != 1){
                    Datastore::set('PrivateImage', array(
                            'userID' => $staff_id,
                            'userType' => 'Staff',
                            'fileName' => 'profile.jpg',
                            'blob' => $blob,
                            'width' => $size[0],
                            'height' => $size[1],
                            'title' => 'Profile Image',
                            'description' => 'Profile Image for Staff '.$staff_id,
                            'createdTime' => $time,
                            'lastUpdateTime' => $time
                    ));
                }

		// Store this activity
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => 'Staff',
                            'action' => $action,
                            'subjectType' => 'Staff',
                            'subjectID' => $staff_id,
                            'createdTime' => $time
                    ));
                }

		// YAY
		$response->addData('staff', $staff->getSanitizedArray());
		return 200;
	} // addStaff()

	/**
	 * Edits a staff user. (Admin only)
	 * @input: $_POST['id, name, phone, gender, email, access-level']
	 *         $_FILES['img']
	 * @output: Staff
	 */
	function editStaff($response) {
		sleep(6); // DEBUG
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check Permissions
		if ($_SESSION['user']->getAccessLevel() < ADMIN_ACCESS_LEVEL) {
			$response->setData(array('error' => 'Insufficient access level.'));
			return 403;
		}

		// Check name
		if (!isset($_POST['name']) || !Validator::isName($_POST['name'])) {
			$name = NULL;
		}
		else {
			$name = trim($_POST['name']);
		}

		// Check phone
		if (!isset($_POST['phone']) || !Validator::isPhone($_POST['phone'])) {
			$phone = NULL;
		}
		else {
			$phone = $_POST['phone'];
		}

		// Check gender
		$gender_options = InternalAPI::readDatalist('genders');
		if (!isset($_POST['gender']) || !Validator::isValidOptionKey($_POST['gender'], $gender_options)) {
			$gender = NULL;
		}
		else {
			$gender = $_POST['gender'];
		}

		// Check email
		if (!isset($_POST['email'])) {
			$response->addData('error', 'Email not specified.');
			return 400;
		}
		if (!Validator::isEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email.');
			return 400;
		}
		$email = $_POST['email'];

		// Check access level
		if (!isset($_POST['accessLevel']) || !isUnsignedInt($_POST['accessLevel'])) {
			$response->addData('error', 'Invalid access level');
			return 400;
		}
		$access_level = (int) $_POST['accessLevel'];

		// Check if img file was uploaded
		$has_img_file_upload = TRUE;
		if (!isset($_FILES['img']['tmp_name'])) {
			$has_img_file_upload = FALSE;
		}
		else if ($_FILES['img']['error'] > 0) {
			if ($_FILES['img']['error'] !== 4) { // 4: no file uploaded
				Log::warning(__METHOD__.'() - img appears uploaded but there were errors: '.$_FILES['img']['error']);
			}
			$has_img_file_upload = FALSE;
		}
		if ($has_img_file_upload) {
			$size = getimagesize($_FILES['img']['tmp_name']); // size = [width, height, detected_type]
			if (!$size) {
				$response->addData('error', 'Unknown file type.');
				return 400;
			}
			if ($size[2] === IMAGETYPE_JPEG) {
			}
			else if ($size[2] === IMAGETYPE_PNG) {
			}
			else {
				$response->addData('error', 'Unknown file type. Expects JPEG / PNG.');
				return 400;
			}

			$blob = file_get_contents($_FILES['img']['tmp_name']);
		} // has file upload

		// Transaction for ACID-ity
		$user_id = $_SESSION['user']->getID();
		$action = 'Edited Staff';
		$committed = FALSE;
		while (!$committed) {
			$time = getTimeInMs();

			Database::beginTransaction();

			// Read from DB
			$staff = Database::readObjectByID('Staff', $_POST['id'], array(
				'update' => TRUE
			));
			if (!isset($staff)) {
				$response->addData('error', 'No such Staff, id: ' . $_POST['id']);
				return 404;
			}

			// Update it
			try {
				$staff->update(array(
					'name' => $name,
					'phone' => $phone,
					'gender' => $gender,
					'accessLevel' => $access_level,
					'email' => $email,
					'lastUpdateTime' => $time
				));
			}
			catch (Exception $e) {
				$response->addData('error', $e->getMessage());
				return 400;
			}
			if (!Database::update($staff)) {
				Log::fatal(__METHOD__.'() - unable to update '.$staff);
				$response->addData('error', 'DB unable to update Staff');
				return 500;
			}
			$staff_id = $staff->getID();

			$committed = Database::endTransaction();
		} // while not committed

		// Edit profile image if necessary
		if ($has_img_file_upload) {
                    if(LOCAL != 1){
			Datastore::set('PrivateImage', array(
				'userID' => $staff_id,
				'userType' => 'Staff',
				'fileName' => 'profile.jpg',
				'blob' => $blob,
				'width' => $size[0],
				'height' => $size[1],
				'createdTime' => $time,
				'lastUpdateTime' => $time
			));
                    }
		} // has file to upload

		// Store this activity
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => 'Staff',
                            'action' => $action,
                            'subjectType' => 'Staff',
                            'subjectID' => $_POST['id'],
                            'createdTime' => $time
                    ));
                }

		// YAY
		$response->addData('staff', $staff->getSanitizedArray());
		return 200;
	} // editStaff()

	/**
	 * Sets a staff's paswordAttemptsRemmaing to 0. (Admin only)
	 * @input: $_POST['id']
	 * @output: staff
	 */
	function disableStaff($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check Permissions
		if ($_SESSION['user']->getAccessLevel() < ADMIN_ACCESS_LEVEL) {
			$response->setData(array('error' => 'Insufficient access level.'));
			return 403;
		}

		$_POST['userType'] = 'Staff';
		return $this->disableUser($response);
	} //disableStaff()

	/**
	 * Sets a staff's password. Will also set his emailVerified to be TRUE and reset his passwordAttempts / otpAttempts.
	 * @input: $_POST['id, password']
	 * @output: staff
	 */
	function setStaffPassword($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check Permissions
		if ($_SESSION['user']->getAccessLevel() < ADMIN_ACCESS_LEVEL) {
			$response->setData(array('error' => 'Insufficient access level.'));
			return 403;
		}

		$_POST['userType'] = 'Staff';
		return $this->setUserPassword($response);
	} //setStaffPassword()

	/**
	 * Gets the total number of members in the system, verified number and unverified number. Useful for quick overview.
	 * - Note: disabled accounts can be gotten simply by (total - verified - unverified)
	 * @input: -
	 * @output: total, verified, unverified
	 */
	function getMemberStats($response) {
		// Ensure action is legal - check that the user is indeed a staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff');
			return 401;
		}
		
		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Read stats from DB
		$total_count = Database::count('Member', array());
		$verified_count = Database::count('Member', array(
			array(
				array('emailVerified', '=', TRUE),
				array('passwordAttemptsRemaining', '>', 0),
				array('otpAttemptsRemaining', '>', 0)
			)
		));
		$unverified_count = Database::count('Member', array(
			array(
				array('emailVerified', '=', FALSE),
				array('passwordAttemptsRemaining', '>', 0),
				array('otpAttemptsRemaining', '>', 0)
			)
		));
			
		$response->addData('total', $total_count);
		$response->addData('verified', $verified_count);
		$response->addData('unverified', $unverified_count);
		return 200;
	} // getMemberStats()

	/**
	 * Gets members from the system.
	 * Note: conditions is read as a JSON string representing a 3D array of OR conditions. When parsed, each condition should be an array of 3 elements - representing KEY, OPERAND, VALUE.
	 * Note: order must be "ASC" or "DESC"
	 * Note: search is case insensitive by default (COLLATE utf8_unicode_ci)
	 * @input: $_GET['conditions, offset, count, sort, order, sort2, order2']
	 * @putput: Array of member
	 */
	function getMembers($response) {
		$_POST['userType'] = 'Staff';
		$_POST['dataType'] = 'Member';
		$_POST['dataTypePlural'] = 'Members';

		return (new InternalAPI())->getData($response);
	} // getMembers()

	/**
	 * Gets count of certain types of members from the system. Useful if data is not required, only counters.
	 * Note: conditions is read as a JSON string representing a 3D array of OR conditions. When parsed, each condition should be an array of 3 elements - representing KEY, OPERAND, VALUE.
	 * Note: order must be "ASC" or "DESC"
	 * Note: search is case insensitive by default (COLLATE utf8_unicode_ci)
	 * @input: $_GET['conditions, offset, count, sort, order, sort2, order2']
	 * @putput: count
	 */
	function getMemberCount($response) {
		$_POST['userType'] = 'Staff';
		$_POST['dataType'] = 'Member';

		return (new InternalAPI())->getDataCount($response);
	} //getMemberCount()

	/**
	 * Adds a new member to system.
	 * - Note: password is required if email-verified is TRUE
	 * @input: $_POST['name, phone, gender, birthDay, birthMonth, birthYear, email, email-verified, password']
	 *         $_FILES['img']
	 * @output: member
	 */
	function addMember($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check name
		if (!isset($_POST['name']) || !Validator::isName($_POST['name'])) {
			$name = NULL;
		}
		else {
			$name = trim($_POST['name']);
		}

		// Check phone
		if (!isset($_POST['phone']) || !Validator::isPhone($_POST['phone'])) {
			$phone = NULL;
		}
		else {
			$phone = $_POST['phone'];
		}

		// Check gender
		$gender_options = InternalAPI::readDatalist('genders');
		if (!isset($_POST['gender']) || !Validator::isValidOptionKey($_POST['gender'], $gender_options)) {
			$gender = NULL;
		}
		else {
			$gender = $_POST['gender'];
		}

		// Check birthdate
		if (!isset($_POST['birthDay']) || !isset($_POST['birthMonth']) || !isset($_POST['birthYear']) ||
			!Validator::isDate($_POST['birthYear'], $_POST['birthMonth'], $_POST['birthDay'])) {
			$birthDay = NULL;
			$birthMonth = NULL;
			$birthYear = NULL;
		}
		else {
			$birthDay = $_POST['birthDay'];
			$birthMonth = $_POST['birthMonth'];
			$birthYear = $_POST['birthYear'];
		}

		// Check email
		if (!isset($_POST['email'])) {
			$response->addData('error', 'Email not specified.');
			return 400;
		}
		if (!Validator::isEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email.');
			return 400;
		}
		$email = $_POST['email'];

		// Check email verified
		if (isset($_POST['email-verified']) && $_POST['email-verified']) {
			$emailVerified = TRUE;

			// Check password validity and generate hash
			if (!isset($_POST['password'])) {
				$response->addData('error', 'Password not specified.');
				return 400;
			}
			if (!Validator::isStrongPassword($_POST['password'])) {
				$response->addData('error', 'Password is not strong enough.');
				return 400;
			}
			$passwordHash = Encryptor::getHash($_POST['password']);
		}
		else {
			$emailVerified = FALSE;
			$passwordHash = NULL;
		}

		// Check if img file was uploaded
		$has_img_file_upload = TRUE;
		if (!isset($_FILES['img']['tmp_name'])) {
			$has_img_file_upload = FALSE;
		}
		else if ($_FILES['img']['error'] > 0) {
			if ($_FILES['img']['error'] !== 4) { // 4: no file uploaded
				Log::warning(__METHOD__.'() - img appears uploaded but there were errors: '.$_FILES['img']['error']);
			}
			$has_img_file_upload = FALSE;
		}
		if ($has_img_file_upload) {
			$size = getimagesize($_FILES['img']['tmp_name']); // size = [width, height, detected_type]
			if (!$size) {
				$response->addData('error', 'Unknown file type.');
				return 400;
			}
			if ($size[2] == IMAGETYPE_PNG || $size[2] == IMAGETYPE_GIF) {
				// convert to JPEG
				$blob = file_get_contents($_FILES['img']['tmp_name']);
				$blob = ImageProcessor::crop($blob, array(
					'type' => IMAGETYPE_JPEG
				));
			}
			else if ($size[2] == IMAGETYPE_JPEG) {
				$blob = file_get_contents($_FILES['img']['tmp_name']);
			}
			else {
				$response->addData('error', 'Unknown file type.');
				return 400;
			}
		} // has file upload
		else {
			$size = getimagesize(API_ROOT.'/assets/members/0/profile.jpg');
			$blob = file_get_contents(API_ROOT.'/assets/members/0/profile.jpg');
		}

		// Transaction for ACID-ity
		$user_id = $_SESSION['user']->getID();
		$action = 'Added Member';
		$committed = FALSE;
		while (!$committed) {
			$time = getTimeInMs();

			// Create Member - can be done outside transaction as we don't need persistence (yet)
			try {
				$member = new Member(array(
					'name' => $name,
					'phone' => $phone,
					'gender' => $gender,
					'birthDay' => $birthDay,
					'birthMonth' => $birthMonth,
					'birthYear' => $birthYear,
					'email' => $email,
					'emailVerified' => $emailVerified,
					'passwordHash' => $passwordHash,
					'createdTime' => $time,
					'lastUpdateTime' => $time
				));
			}
			catch (Exception $e) {
				$response->addData('error', $e->getMessage());
				return 400;
			}

			Database::beginTransaction();
			
			// Create it
			if (!Database::create($member)) {
				Log::warning(__METHOD__.'() - DB Unable to create Member ' . $email);
				$response->addData('error', 'Unable to create Member. Possible email conflict.');
				return 400;
			}

			$committed = Database::endTransaction();
		} // while not committed

		$member_id = $member->getID();

		// Add profile picture
                if(LOCAL != 1){
                    Datastore::set('PrivateImage', array(
                            'userID' => $member_id,
                            'userType' => 'Member',
                            'fileName' => 'profile.jpg',
                            'blob' => $blob,
                            'width' => $size[0],
                            'height' => $size[1],
                            'title' => 'Profile Image',
                            'description' => 'Profile Image for Member '.$member_id,
                            'createdTime' => $time,
                            'lastUpdateTime' => $time
                    ));
                }

		// Store this activity
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => 'Staff',
                            'action' => $action,
                            'subjectType' => 'Member',
                            'subjectID' => $member_id,
                            'createdTime' => $time
                    ));
                }

		// YAY
		$response->addData('member', $member->getSanitizedArray());
		return 200;
	} //addMember()

	/**
	 * Edits a new member of the system.
	 * - Note: cannot edit email, emailVerified and password.
	 * @input: $_POST['id, name, phone, gender, birthDay, birthMonth, birthYear']
	 *         $_FILES['img']
	 * @output: member
	 */
	function editMember($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check id
		if (!isset($_POST['id'])) {
			$response->addData('error', 'id not specified.');
			return 400;
		}
		if (!isUnsignedInt($_POST['id'])) {
			$response->addData('error', 'Illegal id.');
			return 400;
		}
		$id = $_POST['id'];

		// Check name
		if (!isset($_POST['name']) || !Validator::isName($_POST['name'])) {
			$name = NULL;
		}
		else {
			$name = trim($_POST['name']);
		}

		// Check phone
		if (!isset($_POST['phone']) || !Validator::isPhone($_POST['phone'])) {
			$phone = NULL;
		}
		else {
			$phone = $_POST['phone'];
		}

		// Check gender
		$gender_options = InternalAPI::readDatalist('genders');
		if (!isset($_POST['gender']) || !Validator::isValidOptionKey($_POST['gender'], $gender_options)) {
			$gender = NULL;
		}
		else {
			$gender = $_POST['gender'];
		}

		// Check birthdate
		if (!isset($_POST['birthDay']) || !isset($_POST['birthMonth']) || !isset($_POST['birthYear']) ||
			!Validator::isDate($_POST['birthYear'], $_POST['birthMonth'], $_POST['birthDay'])) {
			$birthDay = NULL;
			$birthMonth = NULL;
			$birthYear = NULL;
		}
		else {
			$birthDay = $_POST['birthDay'];
			$birthMonth = $_POST['birthMonth'];
			$birthYear = $_POST['birthYear'];
		}

		// Check if img file was uploaded
		$has_img_file_upload = TRUE;
		if (!isset($_FILES['img']['tmp_name'])) {
			$has_img_file_upload = FALSE;
		}
		else if ($_FILES['img']['error'] > 0) {
			if ($_FILES['img']['error'] !== 4) { // 4: no file uploaded
				Log::warning(__METHOD__.'() - img appears uploaded but there were errors: '.$_FILES['img']['error']);
			}
			$has_img_file_upload = FALSE;
		}
		if ($has_img_file_upload) {
			$size = getimagesize($_FILES['img']['tmp_name']); // size = [width, height, detected_type]
			if (!$size) {
				$response->addData('error', 'Unknown file type.');
				return 400;
			}
			if ($size[2] == IMAGETYPE_PNG || $size[2] == IMAGETYPE_GIF) {
				// convert to JPEG
				$blob = file_get_contents($_FILES['img']['tmp_name']);
				$blob = ImageProcessor::crop($blob, array(
					'type' => IMAGETYPE_JPEG
				));
			}
			else if ($size[2] == IMAGETYPE_JPEG) {
				$blob = file_get_contents($_FILES['img']['tmp_name']);
			}
			else {
				$response->addData('error', 'Unknown file type.');
				return 400;
			}
		} // has file upload

		// Transaction for ACID-ity
		$user_id = $_SESSION['user']->getID();
		$action = 'Edited Member';
		$committed = FALSE;
		while (!$committed) {
			$time = getTimeInMs();

			// Begin DB transaction
			Database::beginTransaction();

			// Read from DB
			$member = Database::readObjectByID('Member', $id, array(
				'update' => TRUE
			));
			if (!isset($member)) {
				$response->addData('error', 'No such Member, id: ' . $id);
				return 404;
			}

			// Update it
			try {
				$member->update(array(
					'name' => $name,
					'phone' => $phone,
					'gender' => $gender,
					'birthDay' => $birthDay,
					'birthMonth' => $birthMonth,
					'birthYear' => $birthYear,
					'lastUpdateTime' => $time
				));
			}
			catch (Exception $e) {
				Log::fatal(__METHOD__.'() - unable to update '.$member.'. ('.$e->getMessage().')');
				$response->addData('error', $e->getMessage());
				return 500;
			}
			if (!Database::update($member)) {
				Log::fatal(__METHOD__.'() - unable to update '.$member);
				$response->addData('error', 'DB unable to update Member');
				return 500;
			}
			$member_id = $member->getID();

			$committed = Database::endTransaction();
		} // while not committed

		// Edit profile image if necessary
		if ($has_img_file_upload) {
                    if(LOCAL != 1){
			Datastore::set('PrivateImage', array(
				'userID' => $member_id,
				'userType' => 'Member',
				'fileName' => 'profile.jpg',
				'blob' => $blob,
				'width' => $size[0],
				'height' => $size[1],
				'createdTime' => $time,
				'lastUpdateTime' => $time
			));
                    }
		} // has file to upload

		// Store this activity
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => 'Staff',
                            'action' => $action,
                            'subjectType' => 'Member',
                            'subjectID' => $member_id,
                            'createdTime' => $time
                    ));
                }

		// YAY
		$response->addData('member', $member->getSanitizedArray());
		return 200;
	} //editMember()

	/**
	 * Sets a member's paswordAttemptsRemmaing to 0.
	 * @input: $_POST['id']
	 * @output: member
	 */
	function disableMember($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		$_POST['userType'] = 'Member';
		return $this->disableUser($response);
	} //disableMember()

	/**
	 * Sets a member's password. Will also set his emailVerified to be TRUE and reset his passwordAttempts / otpAttempts.
	 * @input: $_POST['id, password']
	 * @output: member
	 */
	function setMemberPassword($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		$_POST['userType'] = 'Member';
		return $this->setUserPassword($response);
	} //setMemberPassword()

	/**
	 * Gets a list of all uploaded images
	 * @input: $_GET['domainLabel']
	 * @output: images, last-refresh-time
	 */
	function getImages($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check domain
		if (!isset($_GET['domainLabel'])) {
			$domain_label = 'www';
		}
		else {
			$domain_label = trim(strtolower($_GET['domainLabel']));
		}

		// Read all from DB
		$refreshed_result = InternalAPI::attemptRefreshImages($domain_label, TRUE);

		// YAY
		$response->addData('last-refresh-time', $refreshed_result['last-refresh-time']);
		$response->addData('images', $refreshed_result['images']);
		return 200;
	} //getImages()

	/**
	 * Adds an upload image for content management purpose
	 * - Note: if fileName is not specified, uploaded file's base name will be used.
	 * - Note: file extension will be appended to the fileName automatically if omitted.
	 * @input: $_POST['domainLabel, fileName, title, description, copyright']
	 *         $_FILES['img']
	 * @output: uploaded-image (blob not returned)
	 */
	function addImage($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check for file errors
		if (!isset($_FILES['img']['tmp_name'])) {
			$response->addData('error', 'No file uploaded.');
			return 400;
		}
		if ($_FILES['img']['error'] > 0) {
			$response->addData('error', $_FILES['img']['error']);
			return 400;
		}

		// Check domain
		if (!isset($_POST['domainLabel'])) {
			$domain_label = 'www';
		}
		else {
			$domain_label = trim(strtolower($_POST['domainLabel']));
		}

		// Check file name
		if (!isset($_POST['fileName']) || strlen($_POST['fileName']) == 0) {
			$file_name = strtolower($_FILES['img']['name']);
		}
		else {
			$file_name = encodeFileName($_POST['fileName']);
		}

		// Check title
		if (!isset($_POST['title']) || strlen(trim($_POST['title'])) === 0) {
			$response->addData('error', 'Title cannot be blank.');
			return 400;
		}
		else {
			$title = trim($_POST['title']);
		}

		// Check descriptipon
		if (!isset($_POST['descriptipon'])) {
			$description = '';
		}
		else {
			$description = trim($_POST['description']);
		}

		// Check copyright
		if (!isset($_POST['copyright'])) {
			$copyright = '';
		}
		else {
			$copyright	 = trim($_POST['copyright']);
		}

		// Check file type (and ensure filename extension)
		$size = getimagesize($_FILES['img']['tmp_name']); // size = [width, height, detected_type]
		if (!$size) {
			$response->addData('error', 'Unknown file type.');
			return 400;
		}
		if ($size[2] == IMAGETYPE_PNG) {
			if (!endsWith($file_name, '.png')) {
				$file_name = $file_name.'.png';
			}
		}
		else if ($size[2] == IMAGETYPE_JPEG) {
			if (!endsWith($file_name, '.jpg')) {
				$file_name = $file_name.'.jpg';
			}
		}
		else if ($size[2] == IMAGETYPE_GIF) {
			if (!endsWith($file_name, '.gif')) {
				$file_name = $file_name.'.gif';
			}
		}
		else {
			$response->addData('error', 'Unknown file type.');
			return 400;
		}

		// Check for file name conflicts
		if (Datastore::has('Image', array(
			array('domainLabel', '=', $domain_label),
			array('fileName', '=', $file_name)
		))) {
			$response->addData('error', 'File ' . $file_name . ' already exists.');
			return 409;
		}

		$blob = file_get_contents($_FILES['img']['tmp_name']);

		// Transaction data
		$time = getTimeInMs();
		$user_id = $_SESSION['user']->getID();
		$action = 'Added Image';

		// Check that image does not already exist
		$image = Datastore::getOne('Image', array(
			array('domainLabel', '=', $domain_label),
			array('fileName', '=', $file_name)
		), array(
			'fields' => 'domainLabel, fileName'
		));
		if (isset($image)) {
			$response->addData('error', 'Image "'.$file_name.'" (' . $domain_label . ') already exists.');
			return 409;
		}

		// Store this image
		$image = array(
			'domainLabel' => $domain_label,
			'fileName' => $file_name,
			'blob' => $blob,
			'width' => $size[0],
			'height' => $size[1],
			'title' => $title,
			'description' => $description,
			'copyright' => $copyright,
			'createdTime' => $time,
			'lastUpdateTime' => $time
		);
                if(LOCAL != 1){
                    Datastore::set('Image', $image);
                }

		// Store this activity
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => 'Staff',
                            'action' => $action,
                            'remarks' => $file_name . ' (' . $domain_label . ')',
                            'createdTime' => $time
                    ));
                }

		// Force a refresh
		$refreshed_result = InternalAPI::attemptRefreshImages($domain_label, TRUE);
		
		unset($image['blob']); // dont need to return blob
		$response->addData('image', $image);
		return 200;
	} //addImage()

	/**
	 * Edits an upload image for content management purpose
	 * - Note: domainLabel and fileName are used to identify image only, not to be edited.
	 * - Note: file extension will be appended to the fileName automatically if omitted.
	 * @input: $_POST['domainLabel, fileName, title, description, copyright']
	 *         $_FILES['img']
	 * @output: uploaded-image (blob not returned)
	 */
	function editImage($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check domain
		if (!isset($_POST['domainLabel'])) {
			$response->addData('error', 'Domain Label not provided.');
			return 400;
		}
		else {
			$domain_label = trim(strtolower($_POST['domainLabel']));
		}

		// Check file name
		if (!isset($_POST['fileName']) || strlen($_POST['fileName']) == 0) {
			$response->addData('error', 'File name not provided');
			return 400;
		}
		else {
			$file_name = $_POST['fileName'];
		}

		// Check title
		if (!isset($_POST['title']) || strlen(trim($_POST['title'])) === 0) {
			$response->addData('error', 'Title cannot be blank.');
			return 400;
		}
		else {
			$title = trim($_POST['title']);
		}

		// Check descriptipon
		if (!isset($_POST['descriptipon'])) {
			$description = '';
		}
		else {
			$description = trim($_POST['description']);
		}

		// Check copyright
		if (!isset($_POST['copyright'])) {
			$copyright = '';
		}
		else {
			$copyright	 = trim($_POST['copyright']);
		}

		// Check if img file was uploaded
		$has_img_file_upload = TRUE;
		if (!isset($_FILES['img']['tmp_name'])) {
			$has_img_file_upload = FALSE;
		}
		else if ($_FILES['img']['error'] > 0) {
			if ($_FILES['img']['error'] !== 4) { // 4: no file uploaded
				Log::warning(__METHOD__.'() - img appears uploaded but there were errors: '.$_FILES['img']['error']);
			}
			$has_img_file_upload = FALSE;
		}
		if ($has_img_file_upload) {
			$size = getimagesize($_FILES['img']['tmp_name']); // size = [width, height, detected_type]
			if (!$size) {
				$response->addData('error', 'Unknown file type.');
				return 400;
			}
			if ($size[2] == IMAGETYPE_PNG) {
				if (!endsWith($file_name, '.png')) {
					$file_name = $file_name.'.png';
				}
			}
			else if ($size[2] == IMAGETYPE_JPEG) {
				if (!endsWith($file_name, '.jpg')) {
					$file_name = $file_name.'.jpg';
				}
			}
			else if ($size[2] == IMAGETYPE_GIF) {
				if (!endsWith($file_name, '.gif')) {
					$file_name = $file_name.'.gif';
				}
			}
			else {
				$response->addData('error', 'Unknown file type.');
				return 400;
			}

			$blob = file_get_contents($_FILES['img']['tmp_name']);
		}

		// Transaction data
		$time = getTimeInMs();
		$user_id = $_SESSION['user']->getID();
		$action = 'Edited Image';

		// Check that image exists
		$image = Datastore::getOne('Image', array(
			array('domainLabel', '=', $domain_label),
			array('fileName', '=', $file_name)
		), array(
			'fields' => 'domainLabel, fileName'
		));
		if (!isset($image)) {
			$response->addData('error', 'No such image found: '.$file_name.' ('.$domain_label.')');
			return 404;
		}

		// Update image data
		$image['title'] = $title;
		$image['description'] = $description;
		$image['copyright'] = $copyright;
		$image['lastUpdateTime'] = $time;
		if ($has_img_file_upload) {
			$image['blob'] = $blob;
			$image['width'] = $size[0];
			$image['height'] = $size[1];
		}
                if(LOCAL != 1){
                    Datastore::set('Image', $image);
                }

		// Read latest complete data (less blob)
		$image = Datastore::getOne('Image', array(
			array('domainLabel', '=', $domain_label),
			array('fileName', '=', $file_name)
		), array(
			'fields' => 'domainLabel, fileName, title, description, copyright, width, height, createdTime, lastUpdateTime'
		));

		// Store this activity
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => 'Staff',
                            'action' => $action,
                            'remarks' => $file_name . ' (' . $domain_label . ')',
                            'createdTime' => $time
                    ));
                }
		
		// Force a refresh if we need to
		if ($has_img_file_upload) {
			$refreshed_result = InternalAPI::attemptRefreshImages($domain_label, TRUE);
		} // has file to upload

		// Return
		$response->addData('uploaded-image', $image);
		return 200;
	} //editImage()

	/**
	 * @input: $_POST['domainLabel, fileName, newFileName']
	 * @output: -
	 */
	function renameImage($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check domain
		if (!isset($_POST['domainLabel'])) {
			$response->addData('error', 'Domain Label not provided.');
			return 400;
		}
		else {
			$domain_label = trim(strtolower($_POST['domainLabel']));
		}

		// Check file name
		if (!isset($_POST['fileName']) || strlen($_POST['fileName']) == 0) {
			$response->addData('error', 'File name not provided');
			return 400;
		}
		else {
			$file_name = $_POST['fileName'];
		}

		if (!isset($_POST['newFileName']) || strlen($_POST['newFileName']) == 0) {
			$response->addData('error', 'New file name not provided');
			return 400;
		}
		else {
			$new_file_name = encodeFileName($_POST['newFileName']);
		}

		// Transaction data
		$time = getTimeInMs();
		$user_id = $_SESSION['user']->getID();
		$action = 'Renamed Image';

		// Check that new file name does not conflict any existing file
		$existing_image = Datastore::getOne('Image', array(
			array('domainLabel', '=', $domain_label),
			array('fileName', '=', $new_file_name)
		), array(
			'fields' => 'domainLabel, fileName'
		));
		if (isset($existing_image)) {
			$response->addData('error', 'Image with file name '.$new_file_name.' (' . $domain_label . ') already exists.');
			return 409;
		}

		// Check that image exists
		$image = Datastore::getOne('Image', array(
			array('domainLabel', '=', $domain_label),
			array('fileName', '=', $file_name)
		));
		if (!isset($image)) {
			$response->addData('error', 'No such image found: '.$file_name.' ('.$domain_label.')');
			return 404;
		}

		// Remove old image, set new one in Datastore
		Datastore::remove('Image', array(
			array('domainLabel', '=', $domain_label),
			array('fileName', '=', $file_name)
		));
		$image['fileName'] = $new_file_name;
		$image['createdTime'] = $time;
		$image['lastUpdateTime'] = $time;
		Datastore::set('Image', $image);

		// Store this activity
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => 'Staff',
                            'action' => $action,
                            'remarks' => $file_name . ' --> ' . $new_file_name . ' (' . $domain_label . ')',
                            'createdTime' => $time
                    ));
                }

		// Force a refresh
		$refreshed_result = InternalAPI::attemptRefreshImages($domain_label, TRUE);
		
		// Return
		unset($image['blob']); // no need to return blob
		$response->addData('uploaded-image', $image);
		return 200;
	} //renameImage()

	/**
	 * Deletes an uploaded image (irreversible)
	 * - Note: also deletes actual image and thumbnail files
	 * @input: $_POST['domainLabel, fileName']
	 * @output: -
	 */
	function removeImage($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check domain
		if (!isset($_POST['domainLabel'])) {
			$domain_label = 'www';
		}
		else {
			$domain_label = trim(strtolower($_POST['domainLabel']));
		}

		// Check file name
		if (!isset($_POST['fileName']) || strlen($_POST['fileName']) == 0) {
			$file_name = strtolower($_FILES['img']['name']);
		}
		else {
			$file_name = encodeFileName($_POST['fileName']);
		}

		// Check that there's such an image
		$key_conditions = array(
			array('domainLabel', '=', $domain_label),
			array('fileName', '=', $file_name)
		);
		if (!Datastore::has('Image', $key_conditions)) {
			$response->addData('error', 'No such image found: '.$file_name.' ('.$domain_label.')');
			return 404;
		}

		// Remove image from Datastore
		Datastore::remove('Image', $key_conditions);
		
		// Transaction data
		$time = getTimeInMs();
		$user_id = $_SESSION['user']->getID();
		$action = 'Removed Image';

		// Store this activity	
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => 'Staff',
                            'action' => $action,
                            'remarks' => $file_name . ' (' . $domain_label . ')',
                            'createdTime' => $time
                    ));
                }

		// Force a refresh
		$refreshed_result = InternalAPI::attemptRefreshImages($domain_label, TRUE);
		
		return 200;
	} //removeImage()

	/**
	 * Gets meta, template and data of a page
	 * @input: $_GET['domainLabel, name']
	 * @output: template    - string,
	 *          data        - json string,
	 *          title       - string (meta),
	 *          description - string (meta),
	 *          img         - string (meta)
	 */
	function getPage($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check domain label
		if (!isset($_GET['domainLabel']) || strlen($_GET['domainLabel']) === 0) {
			$response->addData('error', 'Domain label not specified.');
			return 400;
		}
		$domain_label = trim($_GET['domainLabel']);

		// Check page name
		if (!isset($_GET['name']) || strlen($_GET['name']) === 0) {
			$response->addData('error', 'Page name not specified.');
			return 400;
		}
		$name = trim($_GET['name']);

		// Read page from Datastore
		$page = Datastore::getOne('Page', array(
			array('domainLabel','=',$domain_label),
			array('name','=',$name)
		));
		if (is_null($page)) {
			$response->addData('error', 'Page not found: '. $name . ' (' .$domain_label .')');
			return 400;
		}

		$response->setData($page);
		return 200;
	} //getPage()

	/**
	 * Edits a HTML page of a particular front-end domain_root
	 * @input: $_POST['domainLabel, name, template, data, title, description']
	 *         $_POST['img, css, js'] (optional)
	 * @output: -
	 */
	function editPage($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Check domain label
		if (!isset($_POST['domainLabel']) || strlen($_POST['domainLabel']) === 0) {
			$response->addData('error', 'Domain label not specified.');
			return 400;
		}
		$domain_label = trim($_POST['domainLabel']);

		// Check page name
		if (!isset($_POST['name']) || strlen($_POST['name']) === 0) {
			$response->addData('error', 'Page name not specified.');
			return 400;
		}
		$name = trim($_POST['name']);

		// Check template
		if (!isset($_POST['template']) || strlen($_POST['template']) === 0) {
			$response->addData('error', 'HTML template not specified.');
			return 400;
		}
		$template = $_POST['template'];
		
		// Check data
		if (!isset($_POST['data']) || strlen($_POST['data']) === 0) {
			$response->addData('error', 'Template data not specified.');
			return 400;
		}
		$data = json_decode($_POST['data'], TRUE); // format into associative array
		if (is_null($data)) {
			$response->addData('error', 'Invalid data.');
			return 400;
		}
		$data = json_encode($data); // format to JSON string

		// Check title
		if (!isset($_POST['title']) || strlen($_POST['title']) === 0) {
			$response->addData('error', 'Title not specified.');
			return 400;
		}
		$title = trim($_POST['title']);

		// Check description
		if (!isset($_POST['description']) || strlen($_POST['description']) === 0) {
			$response->addData('error', 'Description not specified.');
			return 400;
		}
		$description = trim($_POST['description']);

		// Check img
		if (isset($_POST['img']) && strlen($_POST['img']) > 0) {
			$img = trim($_POST['img']);
		}
		else {
			$img = NULL;
		}
		
		// Check css
		if (isset($_POST['css']) && strlen($_POST['css']) > 0) {
			$css = trim($_POST['css']);
		}
		else {
			$css = NULL;
		}

		// Check js
		if (isset($_POST['js']) && strlen($_POST['js']) > 0) {
			$js = trim($_POST['js']);
		}
		else {
			$js = NULL;
		}

		// Transaction data
		$user_id = $_SESSION['user']->getID();
		$time = getTimeInMs();

		// Store this page
                if(LOCAL != 1){
                    Datastore::set('Page', array(
                            'domainLabel' => $domain_label,
                            'name' => $name,
                            'template' => $template,
                            'data' => $data,
                            'title'=> $title,
                            'description' => $description,
                            'img' => $img,
                            'css' => $css,
                            'js' => $js,
                            'lastUpdateTime' => $time
                    ));
                }

		// Store this activity
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => 'Staff',
                            'action' => 'Edited Page',
                            'createdTime' => $time,
                            'remarks' => $name . ' (' . $domain_label .')'
                    ));
                }

		// Force a refresh
		$refreshed_result = InternalAPI::attemptRefreshHTMLPages($domain_label, TRUE);
		
		return 200;
	} //editPage()

	/**
	 * Serves a private image of a user
	 * - Note: No CSRF checks as this request may come from an embedded URL
	 * @input: $_GET['userType', 'userID', 'fileName']
	 */
	function servePrivateImage($response) {
		// Ensure action is legal - check that the user is indeed a logged in staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff.');
			return 401;
		}

		// Check userType
		if (!isset($_GET['userType']) || strlen($_GET['userType']) === 0) {
			$response->addData('error', 'User Type not specified.');
			return 400;
		}
		$user_type = trim($_GET['userType']);

		// Check userID
		if (!isset($_GET['userID']) || !isUnsignedInt($_GET['userID'])) {
			$response->addData('error', 'User ID not specified or not recognised.');
			return 400;
		}
		$user_id = str2uint($_GET['userID']);

		// Check file name
		$file_name = trim($_GET['fileName']);
		if (mb_strlen($file_name) === 0) {
			$response->addData('error', 'File name is empty.');
			return 400;
		}

		// Get from Datastore
		$image = Datastore::getOne('PrivateImage', array(
			array('userType', '=', $user_type),
			array('userID', '=', $user_id),
			array('fileName', '=', $file_name)
		), array('fields' => 'blob'));
		if (is_null($image)) {
			$response->addData('error', 'Image not found.');
			return 400;
		}

		$response->serveImage($image['blob']);
	} //servePrivateImage()

	/**
	 * Adds a tag to the system
	 * @input: $_POST['type, value']
	 * @output: Tag
	 */
	function addTag($response) {
		// Ensure action is legal - check that the user is indeed a staff
		if (get_class($_SESSION['user']) != 'Staff') {
			$response->addData('error', 'No session staff');
			return 401;
		}
		
		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}

		// Transaction for ACID-ity
		$user_id = $_SESSION['user']->getID();
		$action = 'Added Tag';
		$committed = FALSE;
		while (!$committed) {
			$time = getTimeInMs();

			// Create tag - can be done outside transaction as we don't need persistence (yet)
			try {
				$tag = new Tag(array(
					'type' => $_POST['type'],
					'value' => $_POST['value'],
					'createdTime' => $time,
					'lastUpdateTime' => $time
				));
			}
			catch (Exception $e) {
				$response->addData('error', $e->getMessage());
				return 400;
			}

			// Begin DB transaction
			Database::beginTransaction();

			// Create it
			if (!Database::create($tag)) {
				Log::warning(__METHOD__.'() - DB Unable to create Tag ' . $value. '('.$type.')');
				$response->addData('error', 'Unable to create Tag. Possible duplicate.');
				return 400;
			}

			$committed = Database::endTransaction();
		} // while not committed

		// Store this activity
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => 'Staff',
                            'action' => $action,
                            'createdTime' => $time,
                    ));
                }

		// Return settings
		$response->addData('tag', $tag->toArray());
		return 200;
	} //addTag()
	
	/**
	 * Same logic for multiple disable functions.
	 * - Note: Session, CSRF and permission checks should all be done prior to calling this function
	 */
	private function disableUser($response) {
		// Check id
		if (!isset($_POST['id'])) {
			$response->addData('error', 'id not specified.');
			return 400;
		}
		if (!isUnsignedInt($_POST['id'])) {
			$response->addData('error', 'Illegal id.');
			return 400;
		}
		$id = $_POST['id'];

		// Transaction for ACID-ity
		$user_id = $_SESSION['user']->getID();
		$action = 'Disabled '.$_POST['userType'];
		$committed = FALSE;
		while (!$committed) {
			$time = getTimeInMs();

			Database::beginTransaction();

			// Read from DB
			$user = Database::readObjectByID($_POST['userType'], $id, array(
				'update' => TRUE
			));
			if (!isset($user)) {
				$response->addData('error', 'No such '. $_POST['userType'] .', id: ' . $id);
				return 404;
			}

			// Update it
			try {
				$user->update(array(
					'passwordAttemptsRemaining' => 0,
					'lastUpdateTime' => $time
				));
			}
			catch (Exception $e) {
				Log::fatal(__METHOD__.'() - unable to update '.$user.'. ('.$e->getMessage().')');
				$response->addData('error', $e->getMessage());
				return 500;
			}
			if (!Database::update($user)) {
				Log::fatal(__METHOD__.'() - unable to update '.$user);
				$response->addData('error', 'DB unable to update '. $user);
				return 500;
			}

			$committed = Database::endTransaction();
		} // while not committed

		// Store this activity
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => 'Staff',
                            'action' => $action,
                            'subjectType' => $_POST['userType'],
                            'subjectID' => $user->getID(),
                            'createdTime' => $time
                    ));
                }

		// YAY
		$response->addData(lcfirst($_POST['userType']), $user->getSanitizedArray());
		return 200;
	} //disableUser()

	/**
	 * Same logic for multiple set password functions.
	 * - Note: Session, CSRF and permission checks should all be done prior to calling this function
	 */
	private function setUserPassword($response) {
		// Check id
		if (!isset($_POST['id'])) {
			$response->addData('error', 'id not specified.');
			return 400;
		}
		if (!isUnsignedInt($_POST['id'])) {
			$response->addData('error', 'Illegal id.');
			return 400;
		}
		$id = $_POST['id'];

		// Check password validity and generate hash
		if (!isset($_POST['password'])) {
			$response->addData('error', 'Password not specified.');
			return 400;
		}
		if (!Validator::isStrongPassword($_POST['password'])) {
			$response->addData('error', 'Password is not strong enough.');
			return 400;
		}
		$passwordHash = Encryptor::getHash($_POST['password']);

		// Transaction for ACID-ity
		$user_id = $_SESSION['user']->getID();
		$action = 'Set Member Password';
		$committed = FALSE;
		while (!$committed) {
			$time = getTimeInMs();

			Database::beginTransaction();

			// Read from DB
			$user = Database::readObjectByID($_POST['userType'], $id, array(
				'update' => TRUE
			));
			if (!isset($user)) {
				$response->addData('error', 'No such ' . $_POST['userType'] . ', id: ' . $id);
				return 404;
			}

			// Update it
			try {
				$user->update(array(
					'emailVerified' => TRUE,
					'passwordHash' => $passwordHash,
					'passwordAttemptsRemaining' => MAX_PASSWORD_ATTEMPTS,
					'otpAttemptsRemaining' => MAX_OTP_ATTEMPTS,
					'lastUpdateTime' => $time
				));
			}
			catch (Exception $e) {
				Log::fatal(__METHOD__.'() - unable to update '.$user.'. ('.$e->getMessage().')');
				$response->addData('error', $e->getMessage());
				return 500;
			}
			if (!Database::update($user)) {
				Log::fatal(__METHOD__.'() - unable to update '.$user);
				$response->addData('error', 'DB unable to update '. $user);
				return 500;
			}

			$committed = Database::endTransaction();
		} // while not committed

		// Store this activity
                if(LOCAL != 1){
                    Datastore::set('Activity', array(
                            'userID' => $user_id,
                            'userType' => 'Staff',
                            'action' => $action,
                            'subjectType' => $_POST['userType'],
                            'subjectID' => $user->getID(),
                            'createdTime' => $time
                    ));
                }

		// YAY
		$response->addData(lcfirst($_POST['userType']), $user->getSanitizedArray());
		return 200;
	} //setUserPassword()

} //class StaffAPI