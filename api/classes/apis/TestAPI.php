<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * TESTS only - not to be used in production
 *
 * Before test, ensure:
 *   - SQL Database is created with Member table
 *   - CQL Datastore is created with Setting and Tag tables
 * TODO: complete test suite
 ***/
class TestAPI {
	// Throwaway test
	function zz($response) {
		$time = getTimeInMs();
		
		$a = array();
		$b = array();
		$response->addData('0true', UnitTester::isSubset($a,$b));

		$a = array(
			'a' => 'a',
			'b' => NULL,
			'c' => FALSE
		);


		$b = array(
			'a' => 'a'
		);
		$response->addData('1true', UnitTester::isSubset($a,$b));
		$b = array(
			'b' => NULL
		);
		$response->addData('2true', UnitTester::isSubset($a,$b));
		$b = array(
			'c' => FALSE
		);
		$response->addData('3true', UnitTester::isSubset($a,$b));
		$b = array(
			'a' => 'a',
			'c' => FALSE
		);
		$response->addData('4true', UnitTester::isSubset($a,$b));
		$b = array(
			'c' => FALSE,
			'b' => NULL
		);
		$response->addData('5true', UnitTester::isSubset($a,$b));

		$b = array(
			'b' => FALSE
		);
		$response->addData('6false', UnitTester::isSubset($a,$b));

		$b = array(
			'c' => NULL
		);
		$response->addData('7false', UnitTester::isSubset($a,$b));
		
		$get = HTTP::get('https://blog.tarabliss.com.sg/2016/03/24/really-running-late/');
		$response->addData('https', $get);
		return 200;
	}

	/**
	 * Automate tests to check the system setup.
	 * @input: -
	 * @output: passed, failed
	 */
	function testAll($response) {
		Log::test(__METHOD__.'() - Starting...');

		$suites = array(
			'Database',
			'Datastore',
			'Cache',
			'StringHelpers'
		);

		// Run through each suite
		$passed = array(); // Store names of passed tests
		$failed = array(); // Store names of failed tests
		foreach ($suites as $suite) {
			$tmp = 'test'.$suite;

			$test_response = new HTTPResponse();
			$this->$tmp($test_response);
			$passed = array_merge($passed, self::prefixSuffixEach($suite.'->', $test_response->getData('passed')));
			$failed = array_merge($failed, self::prefixSuffixEach($suite.'->', $test_response->getData('failed')));
		}

		// Calculate stats
		$passed_count = count($passed);
		$failed_count = count($failed);
		$total_count = $passed_count + $failed_count;
		Log::test(__METHOD__.'() - Passed: ' . $passed_count . '/' . $total_count . ', ' . $failed_count . '/' . $total_count);

		$response->addData('passedCount', $passed_count);
		$response->addData('failedCount', $failed_count);
		$response->addData('passed', $passed);
		$response->addData('failed', $failed);
		return 200;
	} //testAll()

	/**
	 * Tests the Database
	 * - Note: expects Database to have been created with Member table
	 */
	function testDatabase($response) {
		return $this->runTestSuite($response, 'Database', array(
			'crudUsingObjects',
			'crudUsingArrays'
		));
	} //testDatabase()

	/**
	 * Tests the Datastore methods with various parameters
	 * - Note: expects Datastore to have been created with Setting and Tag tables
	 */
	function testDatastore($response) {
		return $this->runTestSuite($response, 'Datastore', array(
			'removeAndSet',
			'get',
			'getOne',
			'hasAndRemove',
			'count',
			'query'
		));
	} //testDatastore()

	function testCache($response) {
		return $this->runTestSuite($response, 'Cache', array(
			'setGetHasExpire',
			'expirePersistTTL',
			'keysFlush'
		));		
	} //testCache()

	function testStringHelpers($response) {
		return $this->runTestSuite($response, 'StringHelpers', array(
			'escapeHTML',
			'escapeFileName',
			'encodeFileName',
			'formatCamelCase',
			'formatPascalCase',
			'str2uint',
			'csv2array',
			'csv2multiarrays',
			'array2csv',
			'array2xml',
			'slugify',
			'getInitials',
			'renderTemplate',
			'startsWith',
			'endsWith',
			'isInt',
			'isUnsignedInt',
			'isPositiveInt'
		));
	} //testStringHelpers()

	function testFileHelpers($response) {
		return $this->runTestSuite($response, 'FileHelpers', array(
			'recursiveFileOperations'
		));
	} //testFileHelpers()

	function testImageProcessor($response) {
		return $this->runTestSuite($response, 'ImageProcessor', array(
			'resizePNG',
			'resizeJPG',
			'resizeGIF',
			'crop'
		));
	} //testImageProcessor()

	function testValidator($response) {
		return $this->runTestSuite($response, 'Validator', array(
			// TOOD
		));
	} //testValidator()

	private function runTestSuite($response, $suite, $tests) {
		$total_count = count($tests);
		Log::test(__METHOD__.'() - starting ' . $total_count . ' tests on ' . $suite);

		// Tests are organised into classes
		$tmp = $suite.'Test';
		require (API_ROOT. '/test/'.$tmp.'.php');
		$test_suite = new $tmp();

		// Run tests
		$failed = array();
		$passed = array();
		try {
			foreach ($tests as $test_name) {
				Log::test(__METHOD__.'() - running '. $test_name. '()');
				if ($test_suite->$test_name()) {
					$passed[] = $test_name;
				}
				else {
					$failed[] = $test_name;
				}
			}			
		}
		catch (Exception $e) {
			Log::error(__METHOD__.'() - Exception in ' . $test_name. '. ' . $e->getMessage());
			$response->addData('passed', $passed);
			$response->addData('failed', $failed);
			$response->addData('error', 'Exception in ' . $test_name. '. ' . $e->getMessage());
			return 200;
		}

		// Calculate stats
		$passed_count = count($passed);
		$failed_count = count($failed);
		Log::test(__METHOD__.'() - '. $suite .' Tests Passed: ' . count($passed) . '/' . $total_count . '. Tests Failed: ' . count($failed) . '/' . $total_count);

		// Return
		$response->addData('passedCount', count($passed));
		$response->addData('failedCount', count($failed));
		$response->addData('passed', $passed);
		$response->addData('failed', $failed);
		return 200;
	} //runTestSuite()

	/**
	 * Prepends and appends strings to each item in array
	 * @param: {string} prefix
	 *         {array}  array to iterate through
	 *         {string} suffix
	 */
	private static function prefixSuffixEach($prefix, $array, $suffix='') {
		foreach ($array as $key => $val) {
			$array[$key] = $prefix . $val .$suffix;
		}

		return $array;
	} // prefixSuffixEach()
} //class TestAPI
