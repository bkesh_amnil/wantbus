<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Collection of procedures for image processing.
 *
 ***/
class ImageProcessor {
	// Default function: make invalid method calls throw Exceptions
	static function __callStatic($name, $arguments) {
		throw new Exception ('Error in ImageProcessor class: method '.$name.'() does not exist');
	} //call()

	/**
	 * Resizes an image
	 * - Note: the actual encoding depends on the "type" option
	 * @param: {string} blob of original image
	 *         {array} $options
	 *                 {int} width     - desired width. if omitted, will derive using height and aspect ratio
	 *                 {int} height    - desired height. if omitted, will derive using width and aspect ratio,
	 *                 {int} minWidth  - only used if BOTH width and height are not given. Will keep aspect ratio, and ensure resultant image is at least minWidth x minHeight
	 *                 {int} minHeight - only used if BOTH width and height are not given. Will keep aspect ratio, and ensure resultant image is at least minWidth x minHeight
	 *                 {int} type      - output type [IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG] (optional - DEFAULT: detected type)
	 *                 {int} quality   - [1-100] (optional - only applicable for JPEG and PNG)
	 * @return: {string} blob of resized image.
	 */
	static function resize($blob, $options) {
		list($orig_width, $orig_height, $detected_type) = getimagesizefromstring($blob);

		// Set quality and output function
		if (!isset($options['type'])) {
			$options['type'] = $detected_type;
		}
		if ($options['type'] == IMAGETYPE_JPEG) {
			if (!isset($options['quality']) || $options['quality'] < 1 || $options['quality'] > 100) {
				$options['quality'] = 90;
			}
			$image_save_func = 'imagejpeg';
		}
		else if ($options['type'] == IMAGETYPE_PNG) {
			if (!isset($options['quality']) || $options['quality'] < 1 || $options['quality'] > 9 ) {
				$options['quality'] = 9; // 1 is FASTEST but produces larger files, 9 provides the smallest files but takes a longer time. PHP default: 6
			}
			$image_save_func = 'imagepng';
		}
		else if ($options['type'] == IMAGETYPE_GIF) {
			$options['quality'] = NULL;
			$image_save_func = 'imagegif';
		}

		// Calculate new dimensions
		$aspect_ratio = $orig_width / $orig_height;
		if (isset($options['width'])) {
			if (isset($options['height'])) {
				$width = $options['width'];
				$height = $options['height'];
			} // fixed width and height
			else {
				$width = $options['width'];
				$height = $width / $aspect_ratio;
			}
		} // fixed witdh
		else if (isset($options['height'])) {
			$height = $options['height'];
			$width = $height * $aspect_ratio;
		} // fixed height
		else if (isset($options['minWidth']) && isset($options['minHeight'])) {
			$option_ratio = $options['minWidth'] / $options['minHeight'];
			if ($aspect_ratio > $option_ratio) {
				$height = $options['minHeight'];
				$width = $height * $aspect_ratio;
			}
			else {
				$width = $options['minWidth'];
				$height = $width / $aspect_ratio;
			}
		} // minWidth and minHeight
		else {
			Log::error(__METHOD__.'() - No dimensions specified for resize operation.');
			throw new Exception('No dimensions specified for resized image.');
		} // nothing specified: error case

		// Create image from blob
		$image = imagecreatefromstring($blob);

		// Trivial case: nothing to do
		if ($orig_width == $width && $orig_height == $height) {
			ob_start();
			$image_save_func($image);
			return ob_get_clean();
		}

		// Create new image
		$image_new = imagecreatetruecolor($width, $height);
		imagealphablending($image_new, FALSE); // don't let bg "shine through" onto image
		imagesavealpha($image_new, TRUE);

		// Copy image into new image with size
		imagecopyresampled($image_new, $image, 0, 0, 0, 0, $width, $height, $orig_width, $orig_height);
		
		// Output
		ob_start();
		$image_save_func($image_new);
		return ob_get_clean();
	} // resize()

	/**
	 * - Note: if width is not specified, image will be as wide as possible
	 * - Note: if height is not specified, image will be as tall as possible
	 * - Note: if specified crop area exceeds the bounds of the original image, the resultant image will be padded with black space
	 * @param: {string} blob
	 *         {array} options,
	 *                 {int} fromX     - (DEFAULT: 0)
	 *                 {int} fromY     - (DEFAULT: 0)
	 *                 {int} width     - (DEFAULT: 0)
	 *                 {int} height    - (DEFAULT: 0)
	 *                 {int} type      - output type [IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG]
	 *                 {int} quality   - [1-100] (optional - only applicable for JPEG and PNG)
	 * @return: {string} blob of resized image
	 */
	static function crop($blob, $options) {
		$size = getimagesizefromstring($blob);
		if (!$size) {
			throw new Exception('Unrecognised file format.');
		}

		$orig_width = $size[0];
		$orig_height = $size[1];
		$detected_type = $size[2];

		// Set quality and output function
		if (!isset($options['type'])) {
			$options['type'] = $detected_type;
		}
		if ($options['type'] == IMAGETYPE_JPEG) {
			if (!isset($options['quality']) || $options['quality'] < 1 || $options['quality'] > 100) {
				$options['quality'] = 90;
			}
			$image_save_func = 'imagejpeg';
		}
		else if ($options['type'] == IMAGETYPE_PNG) {
			if (!isset($options['quality']) || $options['quality'] < 1 || $options['quality'] > 9 ) {
				$options['quality'] = 9; // 1 is FASTEST but produces larger files, 9 provides the smallest files but takes a longer time. PHP default: 6
			}
			$image_save_func = 'imagepng';
		}
		else if ($options['type'] == IMAGETYPE_GIF) {
			$options['quality'] = NULL;
			$image_save_func = 'imagegif';
		}

		// Calculate crop dimensions
		if (!isset($options['fromX']) || !isUnsignedInt($options['fromX'])) {
			$options['fromX'] = 0;
		}
		if (!isset($options['fromY']) || !isUnsignedInt($options['fromY'])) {
			$options['fromY'] = 0;
		}
		if (!isset($options['width']) || !isUnsignedInt($options['width'])) {
			$options['width'] = $orig_width - $options['fromX'];
		}
		if (!isset($options['height']) || !isUnsignedInt($options['height'])) {
			$options['height'] = $orig_width - $options['fromY'];
		}

		// Create image from blob
		$image = imagecreatefromstring($blob);

		// Trivial case: simply copy file if required and return
		if ($orig_width == $options['width'] && $orig_height == $options['height'] && $from_x == 0  && $from_y == 0) {
			ob_start();
			$image_save_func($image);
			return ob_get_clean();
		}

		// Create new image
		$image_new = imagecreatetruecolor($options['width'], $options['height']);
		imagealphablending($image_new, FALSE); // don't let bg "shine through" onto image
		imagesavealpha($image_new, TRUE);

		// Copy image into new image with dimensions
		imagecopyresampled($image_new, $image, 0, 0, $options['fromX'], $options['fromY'], $options['width'], $options['height'], $options['width'], $options['height']);

		// Output
		ob_start();
		$image_save_func($image_new);
		return ob_get_clean();
	} // crop()
} //ImageProcessor
