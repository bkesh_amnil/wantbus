<?php
/****
 * Backend Framework v2.1.0
 * ========================
 *
 * Test application script.
 * - simple user interface to fire AJAX requests to the backend and observe its response.
 * - 
 *
 * Invoked through the world wide web. Remove from public access once testing is complete.

 ***/
try {
	require __DIR__.'/init.php';
	$result = '';
}
catch (Exception $e) {
	$result = $e->getTraceAsString().'<br>';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>API Tester: <?php echo PROJECT_NAME;?></title>
<link rel="stylesheet" href="assets/reset.css" />
<link rel="stylesheet" href="assets/common.css" />
</head>
<body>
<div id="site">
	<nav>
		<a href="/info.php">Info</a>
		<a href="/install.php">Installer</a>
		<a href="/test.php" class="current">Tester</a>
	</nav>
	<section>
		<details open="open">
			<summary><h2>Test API Endpoints</h2></summary>
			<form id="staff-api-login-form">
				<h3> Staff Login/out</h3>
				<input type="email" name="email" placeholder="Email" value="chuan@yokestudio.com">
				<input type="password" name="password" placeholder="Password" value="password">
				<button type="submit">Test Staff Login</button>
				<button type="button" id="staff-api-logout-button">Test Staff Logout</button>
			</form>
			<form id="member-api-login-form">
				<h3> Member Login/Out</h3>
				<input type="email" name="email" placeholder="Email" value="chuan@yokestudio.com">
				<input type="password" name="password" placeholder="Password" value="password">
				<button type="submit">Test Member Login</button>
				<button type="button" id="member-api-logout-button">Test Member Logout</button>
			</form>
			<br>

			<!-- Test API Forms -->
			<form id="staff-api-form">
				<h3> Staff API</h3>
				<input type="text" name="endpoint" placeholder="end point, e.g. /getStaffActivities">
				<textarea name="data" placeholder="data (JSON)"></textarea>
				<select name="method">
					<option value="POST">POST</option>
					<option value="GET">GET</option>
				</select>
				<input type="text" name="file-input-key" placeholder="File Input Key, e.g. img, file">
				<input type="file" name="file" id="staff-api-file-input">
				<button type="button" id="staff-api-clear-file-input">Clear File</button>
				<button type="submit">Test StaffAPI</button>
			</form>
			
			<form id="member-api-form">
				<h3> Member API</h3>
				<input type="text" name="endpoint" placeholder="end point, e.g. /getMemberActivities">
				<textarea name="data" placeholder="data (JSON)"></textarea>
				<select name="method">
					<option value="POST">POST</option>
					<option value="GET">GET</option>
				</select>
				<input type="text" name="file-input-key" placeholder="File Input Key, e.g. img, file">
				<input type="file" name="file" id="member-api-file-input">
				<button type="button" id="member-api-clear-file-input">Clear File</button>
				<button type="submit">Test MemberAPI</button>
			</form>
			
			<form id="user-api-form">
				<h3> User API</h3>
				<input type="text" name="endpoint" placeholder="end point, e.g. /getOutlets">
				<textarea name="data" placeholder="data (JSON)"></textarea>
				<select name="method">
					<option value="POST">POST</option>
					<option value="GET">GET</option>
				</select>
				<input type="text" name="file-input-key" placeholder="File Input Key, e.g. img, file">
				<input type="file" name="file" id="user-api-file-input">
				<button type="button" id="user-api-clear-file-input">Clear File</button>
				<button type="submit">Test User API</button>
			</form>
			
			<form id="test-form">
				<h3> * (any) API</h3>
				<input type="text" name="endpoint" placeholder="API and end point, e.g. happiness/getState">
				<textarea name="data" placeholder="data (JSON)"></textarea>
				<select name="method">
					<option value="POST">POST</option>
					<option value="GET">GET</option>
				</select>
				<input type="text" name="file-input-key" placeholder="File Input Key, e.g. img, file">
				<input type="file" name="file" id="test-file-input">
				<button type="button" id="test-clear-file-input">Clear File</button>
				<button type="submit">Test API</button>
			</form>
		</details>
	
		<details open="open">
			<summary><h2>Result</h2></summary>
			<button class="clear-result-button">Clear Results</button>
			<section id="result"><?php echo $result;?></section>
			<button class="clear-result-button">Clear Results</button>
		</details>
	</section>
</div>
<script>
	/* details-summary-polyfill-1.0.2.min.js */
	(function(h){function f(){function a(b){var c,d,e,f;d=b.length;if(0!==d)for(c=0;c<d;c++)e=b[c],f=e.nodeName.toUpperCase(),"DETAILS"===f&&k(e),a(e.childNodes)}var b,d;document.head.insertAdjacentHTML("afterbegin",'<br><style>details,summary{display: block;}details>summary:focus{outline:1px solid blue;}details>summary::before{content:"\u25ba";margin-right: 5px;}details[open]>summary::before{content:"\u25bc";}</style>');var c=document.getElementsByTagName("details"),e=document.getElementsByTagName("summary");b=0;for(d=c.length;b<d;b++)k(c[b]);b=0;for(d=e.length;b<d;b++)e[b].tabIndex="0";h.MutationObserver&&(new MutationObserver(function(b){b.forEach(function(b){a(b.addedNodes)})})).observe(document.body,{childList:!0,subtree:!0});document.body.addEventListener("click",n,!0);document.body.addEventListener("keydown",p,!0);document.removeEventListener("DOMContentLoaded",f,!1)}function l(a){for(var b=document.body;a!==b;){if("SUMMARY"===a.nodeName.toUpperCase())return a.parentElement;a=a.parentElement}return null}function k(a){for(var b,d=0,c=a.children.length;d<c;d++)if("SUMMARY"===a.children[d].nodeName.toUpperCase()){b=a.children[d];break}b||(b=document.createElement("summary"),b.innerHTML="Details");a.insertBefore(b,a.firstChild);b.tabIndex="0";g(a)}function m(a){a.hasAttribute("open")?a.removeAttribute("open"):a.setAttribute("open","open");g(a);var b=new Event("toggle");a.dispatchEvent(b)}function g(a){var b,d,c;if(a.hasAttribute("open"))for(b=1,d=a.children.length;b<d;b++){c=a.children[b];var e=c.getAttribute("data-details-open-display");c.style.display=e?e:""}else for(b=1,d=a.children.length;b<d;b++)c=a.children[b],c.hasAttribute("data-details-open-display")||(c.style.display?c.setAttribute("data-details-open-display",c.style.display):c.setAttribute("data-details-open-display","")),c.style.display="none"}function p(a){var b=l(a.target);null===b||13!==a.keyCode&&32!==a.keyCode||(a.preventDefault(),m(b))}function n(a){a=l(a.target);null!==a&&m(a)}"open"in document.createElement("details")||(Object.defineProperty(h.Element.prototype,"open",{get:function(){return"nodeName"in this&&"DETAILS"==this.nodeName.toUpperCase()?this.hasAttribute("open"):void 0},set:function(a){"nodeName"in this&&"DETAILS"==this.nodeName.toUpperCase()&&(a?this.setAttribute("open","open"):this.removeAttribute("open"),g(this))}}),"complete"!==document.readyState?document.addEventListener("DOMContentLoaded",f,!1):f())})(this);

	/* AJAX library - ajax-1.1.0.min.js */
	(function(g){function f(a,b,c){function d(b,c){e.onreadystatechange=function(){if(4===e.readyState){var a=e.status;200<=a&&300>a?b(e):c(e)}};e.onerror=function(){c(Error("Network Error"))};e.send(a.data)}if("undefined"!==typeof b){if("function"!==typeof b)throw new TypeError("ajax.request() - Expecting param2 to be success callback function. Got: "+typeof b+". Value: "+b);if("function"!==typeof c)throw new TypeError("ajax.request() - Expecting param3 to be failure callback function. Got: "+typeof c+". Value: "+c);}a=h(a);var e=k(a);return"undefined"!==typeof b?d(b,c):new Promise(d)}function h(a){"string"!==typeof a.url&&(a.url=window.location.toString());if("string"!==typeof a.method||"GET"!==a.method&&"POST"!==a.method&&"PUT"!==a.method&&"DELETE"!==a.method)a.method="GET";"boolean"!==typeof a.crossDomain&&(a.crossDomain=/^([\w-]+:)?\/\/([^\/]+)/.test(a.url)&&RegExp.$2!==window.location.host);"GET"===a.method||"DELETE"===a.method?a.encType="":"form-data"!==a.encType&&"plain"!==a.encType&&"urlencoded"!==a.encType&&(a.encType=a.data instanceof FormData?"form-data":"string"===typeof a.data?"plain":"urlencoded");var b;if("form-data"===a.encType&&!(a.data instanceof FormData)){var c=new FormData;if("object"===typeof a.data){for(b in a.data)a.data.hasOwnProperty(b)&&c.append(b,a.data[b]);a.data=c}else console.warn("ajax.processSettings() - for encType: form-data, expect data to be object, got :"+typeof a.data+" instead."),delete a.data}else if("plain"===a.encType&&"string"!==typeof a.data)a.data=JSON.stringify(a.data);else if(("urlencoded"===a.encType||""===a.encType)&&"string"!==typeof a.data)if("object"===typeof a.data){b=a.data;c=[];for(key in b)b.hasOwnProperty(key)&&("object"===typeof b[key]?c.push(encodeURIComponent(key)+"="+encodeURIComponent(JSON.stringify(b[key]))):c.push(encodeURIComponent(key)+"="+encodeURIComponent(b[key])));b=c.join("&").replace("%20","+");a.data=b}else"undefined"!==typeof a.data&&(console.warn("ajax.processSettings() - for encType: urlencoded || none, expect data to be string/object, got :"+typeof a.data+" instead."),delete a.data);if("string"!==typeof a.returnType||"text"!==a.returnType&&"html"!==a.returnType&&"xml"!==a.returnType)a.returnType="json";return a}function k(a){var b=new XMLHttpRequest;if("GET"===a.method&&"undefined"!==typeof a.data){var c="?";a.url.includes("?")&&(c="&");b.open("GET",a.url+c+a.data,!0)}else b.open(a.method,a.url,!0);a.crossDomain?b.withCredentials=!0:b.setRequestHeader("X-Requested-With","XMLHttpRequest");"plain"===a.encType?b.setRequestHeader("Content-Type","text/plain; charset=UTF-8"):"urlencoded"===a.encType&&b.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");"text"===a.returnType?b.setRequestHeader("Accept","text/plain, */*; q=0.01"):"html"===a.returnType?b.setRequestHeader("Accept","text/html, */*; q=0.01"):"xml"===a.returnType?b.setRequestHeader("Accept","application/xml, text/xml, */*; q=0.01"):b.setRequestHeader("Accept","application/json, text/javascript, */*; q=0.01");for(var d in a.headers)a.headers.hasOwnProperty(d)&&"string"===typeof a.headers[d]&&b.setRequestHeader(d,a.headers[d]);return b}g.ajax={request:f,post:function(a,b,c){a.method="POST";return f(a,b,c)},get:function(a,b,c){a.method="GET";return f(a,b,c)},put:function(a,b,c){a.method="PUT";return f(a,b,c)},"delete":function(a,b,c){a.method="DELETE";return f(a,b,c)},load:function(a,b,c){return f({url:a,method:"GET",returnType:"html"},b,c)}}})(this||{});

	/**
	 * Makes AJAX request to a URL
	 * @param {string} method
	 *        {string} url
	 *        {FormData | object literal} data
	 * @returns {Promise} 
	 */
	function ajaxRequest(t,e,r){var s={method:t,url:e,data:r,returnType:"json"},a=sessionStorage.getItem('csrf-token');null!==a&&(s.headers={"CSRF-TOKEN":a});var n;return ajax.request(s).then(function(t){try{n=JSON.parse(t.response)}catch(e){return{status:500,data:{error:"Unable to parse server response."}}}return{status:t.status,data:n}},function(t){try{n=JSON.parse(t.response)}catch(e){throw{status:500,data:{error:"Unable to parse server response."}}}throw{status:t.status,data:n}})}

	/**
	 * Changes all newlines ["\r", "\n", \r\n"] to <br> tags
	 * @return (string)
	 */
	function nl2br(str) {
		if (typeof str !== 'string') {
			throw new TypeError('nl2br() - param is not str: ' + str);
		}
		return str.replace(/(?:\r\n|\r|\n)/g, '<br>');
	}
</script>
<script>
	var API_URL = '<?php echo API_URL;?>';
	var USER_API_URL = API_URL + '/user';
	var MEMBER_API_URL = API_URL + '/member';
	var STAFF_API_URL = API_URL + '/staff';
	
	// DOM - Staff
	var dom_staff_api_login_form = document.getElementById('staff-api-login-form');
	var dom_staff_logout_button = document.getElementById('staff-api-logout-button');
	var dom_staff_api_form = document.getElementById('staff-api-form');
	var dom_staff_api_clear_file_input_button = document.getElementById('staff-api-clear-file-input');
	// DOM - Member
	var dom_member_api_login_form = document.getElementById('member-api-login-form');
	var dom_member_logout_button = document.getElementById('member-api-logout-button');
	var dom_member_api_form = document.getElementById('member-api-form');
	var dom_member_api_clear_file_input_button = document.getElementById('member-api-clear-file-input');
	// DOM - User
	var dom_user_api_form = document.getElementById('user-api-form');
	var dom_user_api_clear_file_input_button = document.getElementById('user-api-clear-file-input');
	// DOM - Test
	var dom_test_form = document.getElementById('test-form');
	var dom_test_clear_file_input_button = document.getElementById('test-clear-file-input');
	// DOM - Results
	var dom_result = document.getElementById('result');
	var dom_clear_result_buttons = document.getElementsByClassName('clear-result-button');

	/***********
	 * Results *
	 ***********/
	function appendResult(data) {
		if (typeof data !== 'string') {	
			dom_result.innerHTML += nl2br(JSON.stringify(data, null, 4)) + '<br>';
		}
		else {
			dom_result.innerHTML += data + '<br>';
		}

		dom_result.scrollTop = dom_result.scrollHeight;
	}

	for (var i=0;i<dom_clear_result_buttons.length; i++) {
		dom_clear_result_buttons[i].addEventListener('click', function(e) {
			dom_result.innerHTML = '';
		});
	}

	/*********
	 * Staff *
	 *********/
	// Process Staff API Login		
	dom_staff_api_login_form.addEventListener('submit', function(e) {
		e.preventDefault();
		
		var formData = new FormData(this);
		appendResult('POST: ' + STAFF_API_URL + '/login');
		ajaxRequest('POST', STAFF_API_URL+'/login', formData).then(function(response){
			appendResult(response.data);
			appendResult('------<br>');
			sessionStorage.setItem('csrf-token', response.data['csrf-token']);
		}, function(response) {
			appendResult(response.data);
			appendResult('------<br>');
		});
	});
	
	// Process Staff API Logout
	dom_staff_logout_button.addEventListener('click', function(e) {
		e.preventDefault();
		
		appendResult('POST: ' + STAFF_API_URL + '/logout');
		ajaxRequest('POST', STAFF_API_URL+'/logout').then(function(response){
			appendResult(response.data);
			appendResult('------<br>');
			csrf_token = null;
		}, function(response) {
			appendResult(response.data);
			appendResult('------<br>');
		});
	});
	
	// Process Staff API test
	dom_staff_api_form.addEventListener('submit', function(e) {
		e.preventDefault();
		
		// Parse form values
		var endpoint = dom_staff_api_form.elements['endpoint'].value;
		if (endpoint.indexOf('/') !== 0) {
			endpoint = '/' + endpoint;
		}
		var method = dom_staff_api_form.elements['method'].value;
		var data = dom_staff_api_form.elements['data'].value;
		data = data.replace(/[\r\n]/g, '');
		if (data.indexOf('{') === 0) {
			data = JSON.parse(data);
		}
		else {
			data = JSON.parse('{'+data+'}');
		}
		
		var fileInputKey = dom_staff_api_form.elements['file-input-key'].value;
		var fileInput = document.getElementById('staff-api-file-input');
		var file = fileInput.files[0];
		
		// DEBUG: show what is being sent
		console.log(method + ' to ' + endpoint + ' with: ');
		console.log(data);
		
		var formData;
		if (method === 'POST') {
			formData = new FormData();
			for (var key in data) {
				formData.append(key, data[key]);
			}
			formData.append(fileInputKey, file);
		}
		else {
			formData = {};
			for (var key in data) {
				formData[key] = data[key];
			}
		}
		appendResult('StaffAPI ' + endpoint);
		ajaxRequest(method, STAFF_API_URL+endpoint, formData).then(function(response) {
			if (response.data['csrf-token']) {
				sessionStorage.setItem('csrf-token', response.data['csrf-token']);
			}
			appendResult(response.data);
			appendResult('------<br>');
		}, function(response) {
			appendResult(response.data);
			appendResult('------<br>');
		});
	});
	
	// Process Staff API clear file input
	dom_staff_api_clear_file_input_button.addEventListener('click', function(e) {
		var fileInput = document.getElementById('staff-api-file-input');
		fileInput.value = null;
	});
	
	/************
	 * Member *
	 ************/
	// Process Member API Login
	dom_member_api_login_form.addEventListener('submit', function(e) {
		e.preventDefault();
		
		var formData = new FormData(this);
		ajaxRequest('POST', MEMBER_API_URL+'/login', formData).then(function(response){
			appendResult(response.data);
			appendResult('------<br>');
			sessionStorage.setItem('csrf-token', response.data['csrf-token']);
		}, function(response) {
			appendResult(response.data);
			appendResult('------<br>');
		});
	});
	
	// Process Member API Logout
	dom_member_logout_button.addEventListener('click', function(e) {
		e.preventDefault();
		
		ajaxRequest('POST', MEMBER_API_URL+'/logout').then(function(response){
			appendResult(response.data);
			appendResult('------<br>');
			csrf_token = null;
		}, function(response) {
			appendResult(response.data);
			appendResult('------<br>');
		});
	});
	
	// Process Member API test
	dom_member_api_form.addEventListener('submit', function(e) {
		e.preventDefault();
		
		// Parse form values
		var endpoint = dom_member_api_form.elements['endpoint'].value;
		if (endpoint.indexOf('/') !== 0) {
			endpoint = '/' + endpoint;
		}
		var method = dom_member_api_form.elements['method'].value;
		var data = dom_member_api_form.elements['data'].value;
		data = data.replace(/[\r\n]/g, '');
		if (data.indexOf('{') === 0) {
			data = JSON.parse(data);
		}
		else {
			data = JSON.parse('{'+data+'}');
		}
		
		var fileInputKey = dom_member_api_form.elements['file-input-key'].value;
		var fileInput = document.getElementById('member-api-file-input');
		var file = fileInput.files[0];
		
		// DEBUG: show what is being sent
		console.log(method + ' to ' + endpoint + ' with: ');
		console.log(data);
		
		var formData;
		if (method === 'POST') {
			formData = new FormData();
			for (var key in data) {
				formData.append(key, data[key]);
			}
			formData.append(fileInputKey, file);
		}
		else {
			formData = {};
			for (var key in data) {
				formData[key] = data[key];
			}
		}
		appendResult('MemberAPI ' + endpoint);
		ajaxRequest(method, MEMBER_API_URL+endpoint, formData).then(function(response) {
			if (response.data['csrf-token']) {
				sessionStorage.setItem('csrf-token', response.data['csrf-token']);
			}
			appendResult(response.data);
			appendResult('------<br>');
		}, function(response) {
			appendResult(response.data);
			appendResult('------<br>');
		});
	});
	
	// Process Member API clear file input
	dom_member_api_clear_file_input_button.addEventListener('click', function(e) {
		var fileInput = document.getElementById('member-api-file-input');
		fileInput.value = null;
	});
	
	/**********
	 *  User  *
	 **********/
	// Process User API test
	dom_user_api_form.addEventListener('submit', function(e) {
		e.preventDefault();
		
		// Parse form values
		var endpoint = dom_user_api_form.elements['endpoint'].value;
		if (endpoint.indexOf('/') !== 0) {
			endpoint = '/' + endpoint;
		}
		var method = dom_user_api_form.elements['method'].value;
		var data = dom_user_api_form.elements['data'].value;
		data = data.replace(/[\r\n]/g, '');
		if (data.indexOf('{') === 0) {
			data = JSON.parse(data);
		}
		else {
			data = JSON.parse('{'+data+'}');
		}
		
		var fileInputKey = dom_user_api_form.elements['file-input-key'].value;
		var fileInput = document.getElementById('user-api-file-input');
		var file = fileInput.files[0];
		
		// DEBUG: show what is being sent
		console.log(method + ' to ' + endpoint + ' with: ');
		console.log(data);
		
		var formData;
		if (method === 'POST') {
			formData = new FormData();
			for (var key in data) {
				formData.append(key, data[key]);
			}
			formData.append(fileInputKey, file);
		}
		else {
			formData = {};
			for (var key in data) {
				formData[key] = data[key];
			}
		}
		appendResult('UserAPI ' + endpoint);
		ajaxRequest(method, USER_API_URL+endpoint, formData).then(function(response) {
			appendResult(response.data);
			appendResult('------<br>');
		}, function(response) {
			appendResult(response.data);
			appendResult('------<br>');
		});
	});
	
	// Process Clear File input
	dom_user_api_clear_file_input_button.addEventListener('click', function(e) {
		var fileInput = document.getElementById('user-api-file-input');
		fileInput.value = null;
	});
	
	/*************
	 * Free form *
	 *************/
	// Process test
	dom_test_form.addEventListener('submit', function(e) {
		e.preventDefault();
		
		// Parse form values
		var endpoint = dom_test_form.elements['endpoint'].value;
		if (endpoint.indexOf('/') !== 0) {
			endpoint = '/' + endpoint;
		}
		var method = dom_test_form.elements['method'].value;
		var data = dom_test_form.elements['data'].value;
		data = data.replace(/[\r\n]/g, '');
		if (data.indexOf('{') === 0) {
			data = JSON.parse(data);
		}
		else {
			data = JSON.parse('{'+data+'}');
		}
		
		var fileInputKey = dom_test_form.elements['file-input-key'].value;
		var fileInput = document.getElementById('test-file-input');
		var file = fileInput.files[0];
		
		// DEBUG: show what is being sent
		console.log(method + ' to ' + endpoint + ' with: ');
		console.log(data);
		
		var formData;
		if (method === 'POST') {
			formData = new FormData();
			for (var key in data) {
				formData.append(key, data[key]);
			}
			formData.append(fileInputKey, file);
		}
		else {
			formData = {};
			for (var key in data) {
				formData[key] = data[key];
			}
		}
		appendResult(endpoint);
		ajaxRequest(method, API_URL+endpoint, formData).then(function(response){
			appendResult(response.data);
			appendResult('------<br>');
		}, function(response) {
			appendResult(response.data);
			appendResult('------<br>');
		});
	});
	
	// Process Clear File input
	dom_test_clear_file_input_button.addEventListener('click', function(e) {
		var fileInput = document.getElementById('test-file-input');
		fileInput.value = null;
	});
</script>
</body>
</html>
