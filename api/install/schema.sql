/***
 * SQL Schema
 *
 * In order for installer to parse queries correctly:
 * - Ensure only CREATE TABLE statements.
 * - Ensure each table name is wrapped in ` (backtick).
 * - Ensure each statement ends with a ; (semi-colon).
 * - The installer will render the following templates:
 *     {{table-prefix}}                        e.g. "TB_"
 *     {{engine}}                              e.g. "InnoDB"
 *     {{charset}}                             e.g. "utf-8"
 *     {{collate}}                             e.g. "utf8_unicode_ci"
 */

/* Staff */
CREATE TABLE `{{table-prefix}}Staff`(
	`id` int unsigned NOT NULL auto_increment PRIMARY KEY,
	`name` varchar(128),
	`phone` varchar(16),
	`gender` char(2),
	`birthDay` tinyint unsigned,
	`birthMonth` tinyint unsigned,
	`birthYear` smallint unsigned,
	`accessLevel` tinyint unsigned NOT NULL,
	`email` varchar(254) NOT NULL,
	`emailVerified` tinyint unsigned NOT NULL DEFAULT 0,
	`emailVerificationHash` varchar(77),
	`otpAttemptsRemaining` tinyint unsigned NOT NULL DEFAULT 0,
	`passwordHash` varchar(77),
	`persistentLoginHashes` varchar(2000) NOT NULL DEFAULT '',
	`passwordAttemptsRemaining` tinyint unsigned NOT NULL DEFAULT 0,
	`createdTime` bigint unsigned NOT NULL,
	`lastUpdateTime` bigint unsigned NOT NULL,
	`lastLoginTime` bigint unsigned,
	INDEX(`emailVerified`),
	INDEX(`otpAttemptsRemaining`),
	INDEX(`passwordAttemptsRemaining`),
	UNIQUE KEY(`email`)
) ENGINE = {{engine}} CHARACTER SET {{charset}} COLLATE {{collate}};

/* Member */
CREATE TABLE `{{table-prefix}}Member`(
	`id` int unsigned NOT NULL auto_increment PRIMARY KEY,
	`name` varchar(128),
	`phone` varchar(16),
	`gender` char(2),
	`birthDay` tinyint unsigned,
	`birthMonth` tinyint unsigned,
	`birthYear` smallint unsigned,
	`email` varchar(254) NOT NULL,
	`emailVerified` tinyint unsigned NOT NULL DEFAULT 0,
	`emailVerificationHash` varchar(77),
	`otpAttemptsRemaining` tinyint unsigned NOT NULL DEFAULT 0,
	`passwordHash` varchar(77),
	`persistentLoginHashes` varchar(2000) NOT NULL DEFAULT '',
	`passwordAttemptsRemaining` tinyint unsigned NOT NULL DEFAULT 0,
	`createdTime` bigint unsigned NOT NULL,
	`lastUpdateTime` bigint unsigned NOT NULL,
	`lastLoginTime` bigint unsigned,
	INDEX(`emailVerified`),
	INDEX(`otpAttemptsRemaining`),
	INDEX(`passwordAttemptsRemaining`),
	UNIQUE KEY(`email`)
) ENGINE = {{engine}} CHARACTER SET {{charset}} COLLATE {{collate}};

/* Category */
CREATE TABLE `{{table-prefix}}Category`(
	`id` int unsigned NOT NULL auto_increment PRIMARY KEY,
	`name` varchar(128) NOT NULL,
	`description` varchar(1024) NOT NULL,
	`displayPriority` tinyint unsigned NOT NULL,
	`createdTime` bigint unsigned NOT NULL,
	`lastUpdateTime` bigint unsigned NOT NULL,
	UNIQUE KEY(`name`)
) ENGINE = {{engine}} CHARACTER SET {{charset}} COLLATE {{collate}};

/* SubCategory */
CREATE TABLE `{{table-prefix}}SubCategory`(
	`id` int unsigned NOT NULL auto_increment PRIMARY KEY,
	`name` varchar(128) NOT NULL,
	`categoryID` int unsigned NOT NULL,
	`description` varchar(1024) NOT NULL,
	`displayPriority` tinyint unsigned NOT NULL,
	`createdTime` bigint unsigned NOT NULL,
	`lastUpdateTime` bigint unsigned NOT NULL,
	UNIQUE KEY(`name`),
	FOREIGN KEY (`categoryID`) REFERENCES {{table-prefix}}Category(`id`)
) ENGINE = {{engine}} CHARACTER SET {{charset}} COLLATE {{collate}};

/* Therapy */
CREATE TABLE `{{table-prefix}}Therapy`(
	`id` int unsigned NOT NULL auto_increment PRIMARY KEY,
	`name` varchar(128) NOT NULL,
	`subCategoryID` int unsigned NOT NULL,
	`description` varchar(1024) NOT NULL,
	`benefits` varchar(512) NOT NULL,
	`duration` smallint unsigned NOT NULL,
	`price` mediumint unsigned NOT NULL,
	`promotionPrice` mediumint unsigned,
	`promotionStartTime` bigint unsigned,
	`promotionEndTime` bigint unsigned,
	`promotionTerms` varchar(512),
	`displayPriority` tinyint unsigned NOT NULL,
	`createdTime` bigint unsigned NOT NULL,
	`lastUpdateTime` bigint unsigned NOT NULL,
	UNIQUE KEY nameSubCat(`name`, `subCategoryID`),
	FOREIGN KEY (`subCategoryID`) REFERENCES {{table-prefix}}SubCategory(`id`)
) ENGINE = {{engine}} CHARACTER SET {{charset}} COLLATE {{collate}};

/* Article */
CREATE TABLE `{{table-prefix}}Article`(
	`id` int unsigned NOT NULL auto_increment PRIMARY KEY,
	`title` varchar(128) NOT NULL,
	`excerpt` varchar(512) NOT NULL,
	`url` varchar(256) NOT NULL,
	`displayPriority` tinyint unsigned NOT NULL,
	`createdTime` bigint unsigned NOT NULL,
	`lastUpdateTime` bigint unsigned NOT NULL
) ENGINE = {{engine}} CHARACTER SET {{charset}} COLLATE {{collate}};

/* Accolade */
CREATE TABLE `{{table-prefix}}Accolade`(
	`id` int unsigned NOT NULL auto_increment PRIMARY KEY,
	`title` varchar(128) NOT NULL,
	`description` varchar(512) NOT NULL,
	`displayPriority` tinyint unsigned NOT NULL,
	`createdTime` bigint unsigned NOT NULL,
	`lastUpdateTime` bigint unsigned NOT NULL
) ENGINE = {{engine}} CHARACTER SET {{charset}} COLLATE {{collate}};

/* JobListing */
CREATE TABLE `{{table-prefix}}JobListing`(
	`id` int unsigned NOT NULL auto_increment PRIMARY KEY,
	`title` varchar(128) NOT NULL,
	`responsibilities` varchar(512) NOT NULL,
	`traits` varchar(512) NOT NULL,
	`displayPriority` tinyint unsigned NOT NULL,
	`createdTime` bigint unsigned NOT NULL,
	`lastUpdateTime` bigint unsigned NOT NULL
) ENGINE = {{engine}} CHARACTER SET {{charset}} COLLATE {{collate}};