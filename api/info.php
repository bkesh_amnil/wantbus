<?php
/****
 * Backend Framework v2.1.0
 * ========================
 *
 * Environment Information
 * - check that all environment variables are correct before install
 *
 * Invoked through the world wide web. Remove from public access once testing is complete.

 ***/
try {
	require __DIR__.'/init.php';
}
catch (Exception $e) {
	echo $e->getTraceAsString();
}

/**
 * Checks if a path is readable/writable
 * - Note: called during population of system environment table
 * @param: {string} path 
 * @return: {string} HTML
 */
function getDirectoryStatusHTML($path) {
	if (is_writable($path)) {
		return '<span class="">OK!</span>';
	}
	else {
		if (is_readable($path)) {
			return '<span class="error">Read Only</span>';
		}
		else {
			return '<span class="error">Does Not Exist</span>';
		}
	}
} //getDirectoryStatusHTML()

/**
  * Checks if an SQL DB is readable/writable
 * - Note: called during population of system environment table
 * @param: {string} DB name
 * @return: {string} HTML
 */
function getSQLDBStatus($db_name) {
	$q = 'SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE `SCHEMA_NAME` = "'.$db_name.'"';

	try {
		$pdo_statement = Database::query($q);
		$result = $pdo_statement->fetchAll(PDO::FETCH_ASSOC);
		if (count($result) === 0) {
			return '<span class="error">DB does Not Exist</span>';
		}

		return '<span class="">OK!</span>';
	}
	catch (Exception $e) {
		Log::debug('Exception in getSQLDBStatus()');
		Log::debug($e);
		return '<span class="error">Unable to connect to SQL</span>';
	}
} //getSQLDBStatus()

/**
  * Checks if an SQL DB is readable/writable
 * - Note: called during population of system environment table
 * @param: {string} DB name
 * @return: {string} HTML
 */
function getCQLDBStatus($db_name) {
	$q = '';

	try {
		$result = Datastore::query('SELECT COUNT(*) from '.CASSANDRA_TABLE_PREFIX.'Setting');
		if (!is_array($result) || count($result) === 0) {
			return '<span class="error">Unable to query CQL</span>';
		}

		return '<span class="">' . $result[0]['count'] . ' settings found.</span>';
	}
	catch (Exception $e) {
		Log::debug('Exception in getCQLDBStatus()');
		Log::debug($e);
		return '<span class="error">Unable to connect to CQL</span>';
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Info: <?php echo PROJECT_NAME;?></title>
<link rel="stylesheet" href="assets/reset.css" />
<link rel="stylesheet" href="assets/common.css" />
</head>
<body>
<div id="site">
	<nav>
		<a href="/info.php" class="current">Info</a>
		<a href="/install.php">Installer</a>
		<a href="/test.php">Tester</a>
	</nav>
	<section>
		<details open="open">
			<summary><h2>System Environment</h2></summary>
			<table id="environment-table">
				<tr>
					<th>MYSQL_DB_NAME</th>
					<td><?php echo MYSQL_DB_NAME . ' ['. getSQLDBStatus(MYSQL_DB_NAME) . ']';?></td>
				</tr>
				<tr>
					<th>CASSANDRA_KEYSPACE</th>
					<td><?php echo CASSANDRA_KEYSPACE . ' ['. getCQLDBStatus(CASSANDRA_KEYSPACE) . ']';?></td>
				</tr>
				<tr>
					<th>PHP_VERSION</th>
					<td><?php echo PHP_VERSION;?></td>
				</tr>
				<tr>
					<th>Extensions</th>
					<td><?php echo array2csv(get_loaded_extensions(), array('delimiter' => ', ')); ?></td>
				</tr>
				<tr>
					<th>PHP_INT_MAX</th>
					<td><?php echo PHP_INT_MAX.' ('.PHP_INT_SIZE * 8 .' bits)';?></td>
				</tr>
				<tr>
					<th>UNIX Time</th>
					<td><?php try {echo time().' (in seconds)<br>'.getTimeInMs().' (in ms)';}catch(Exception $e) { echo $e->getMessage(); }?></td>
				</tr>
				<tr>
					<th>Script User</th>
					<td><?php echo exec('whoami');?></td>
				</tr>
				<tr>
					<th>Script Owner</th>
					<td><?php echo get_current_user();?></td>
				</tr>
				<tr>
					<th>API_URL</th>
					<td><?php echo API_URL;?></td>
				</tr>
				<tr>
					<th>WWW_URL</th>
					<td><?php echo WWW_URL;?></td>
				</tr>
				<tr>
					<th>API_ROOT</th>
					<td><?php echo API_ROOT;?></td>
				</tr>
				<tr>
					<th>WWW_ROOT</th>
					<td><?php echo WWW_ROOT;?></td>
				</tr>
				<tr>
					<th>API_ROOT [logs]</th>
					<td><?php echo getDirectoryStatusHTML(API_ROOT.'/logs'); ?></td>
				</tr>
				<tr>
					<th>API_ROOT [tmp]</th>
					<td><?php echo getDirectoryStatusHTML(API_ROOT.'/tmp'); ?></td>
				</tr>
				<tr>
					<th>WWW_ROOT [html]/[pages]</th>
					<td><?php echo getDirectoryStatusHTML(WWW_ROOT.'/html/pages'); ?></td>
				</tr>
				<tr>
					<th>WWW_ROOT [html]/[templates]</th>
					<td><?php echo getDirectoryStatusHTML(WWW_ROOT.'/html/templates'); ?></td>
				</tr>
				<tr>
					<th>WWW_ROOT [img]/[uploads]</th>
					<td><?php echo getDirectoryStatusHTML(WWW_ROOT.'/img/uploads'); ?></td>
				</tr>
				<tr>
					<th>WWW_ROOT [doc]</th>
					<td><?php echo getDirectoryStatusHTML(WWW_ROOT.'/doc'); ?></td>
				</tr>
				<tr>
					<th>WWW_ROOT [data]</th>
					<td><?php echo getDirectoryStatusHTML(WWW_ROOT.'/data'); ?></td>
				</tr>
				<tr>
					<th>WWW_ROOT [private]</th>
					<td><?php echo getDirectoryStatusHTML(WWW_ROOT.'/private'); ?></td>
				</tr>
			</table>
		</details>
		<details open="open">
			<summary><h2>This Browser Client</h2></summary>
			<p>All pixel values in CSS pixels.</p>
			<table id="resolution-table">
				<tr>
					<th>Element</th>
					<th>Size</th>
					<th>Description</th>
				</tr>
				<tr>
					<td>Screen</td>
					<td id="screen-dimensions"></td>
					<td>Resolution of this display screen</td>
				</tr>
				<tr>
					<td>Screen Available</td>
					<td id="screen-avail-dimensions"></td>
					<td>Resolution of this display available for browser</td>
				</tr>
				<tr>
					<td>Window Outer</td>
					<td id="window-outer-dimensions"></td>
					<td>Resolution of the browser window including browser chrome</td>
				</tr>
				<tr>
					<td>Window Inner</td>
					<td id="window-inner-dimensions"></td>
					<td>Resolution of the browser window available for the html page</td>
				</tr>
				<tr>
					<td>HTML Offset</td>
					<td id="html-offset-dimensions"></td>
					<td>Size of page including borders and scrollbars</td>
				</tr>
				<tr>
					<td>HTML Client</td>
					<td id="html-client-dimensions"></td>
					<td>Size of page excluding borders and scrollbars</td>
				</tr>
				<tr>
					<td>Body Offset</td>
					<td id="body-offset-dimensions"></td>
					<td>Size of body including borders and scrollbars</td>
				</tr>
				<tr>
					<td>Body Client</td>
					<td id="body-client-dimensions"></td>
					<td>Size of body excluding borders and scrollbars</td>
				</tr>
				<tr>
					<td>Site Container Offset</td>
					<td id="site-offset-dimensions"></td>
					<td>Size of #site including borders and scrollbars</td>
				</tr>
				<tr>
					<td>Site Container Client Dimensions</td>
					<td id="site-client-dimensions"></td>
					<td>Size of #site excluding borders and scrollbars</td>
				</tr>
				<tr>
					<td>Site Container Scroll Dimensions</td>
					<td id="site-scroll-dimensions"></td>
					<td>Size of #site content excluding borders and scrollbars</td>
				</tr>
			</table>
		</details>
	</section>
</div>
<script>
	/* details-summary-polyfill-1.0.2.min.js */
	(function(h){function f(){function a(b){var c,d,e,f;d=b.length;if(0!==d)for(c=0;c<d;c++)e=b[c],f=e.nodeName.toUpperCase(),"DETAILS"===f&&k(e),a(e.childNodes)}var b,d;document.head.insertAdjacentHTML("afterbegin",'<br><style>details,summary{display: block;}details>summary:focus{outline:1px solid blue;}details>summary::before{content:"\u25ba";margin-right: 5px;}details[open]>summary::before{content:"\u25bc";}</style>');var c=document.getElementsByTagName("details"),e=document.getElementsByTagName("summary");b=0;for(d=c.length;b<d;b++)k(c[b]);b=0;for(d=e.length;b<d;b++)e[b].tabIndex="0";h.MutationObserver&&(new MutationObserver(function(b){b.forEach(function(b){a(b.addedNodes)})})).observe(document.body,{childList:!0,subtree:!0});document.body.addEventListener("click",n,!0);document.body.addEventListener("keydown",p,!0);document.removeEventListener("DOMContentLoaded",f,!1)}function l(a){for(var b=document.body;a!==b;){if("SUMMARY"===a.nodeName.toUpperCase())return a.parentElement;a=a.parentElement}return null}function k(a){for(var b,d=0,c=a.children.length;d<c;d++)if("SUMMARY"===a.children[d].nodeName.toUpperCase()){b=a.children[d];break}b||(b=document.createElement("summary"),b.innerHTML="Details");a.insertBefore(b,a.firstChild);b.tabIndex="0";g(a)}function m(a){a.hasAttribute("open")?a.removeAttribute("open"):a.setAttribute("open","open");g(a);var b=new Event("toggle");a.dispatchEvent(b)}function g(a){var b,d,c;if(a.hasAttribute("open"))for(b=1,d=a.children.length;b<d;b++){c=a.children[b];var e=c.getAttribute("data-details-open-display");c.style.display=e?e:""}else for(b=1,d=a.children.length;b<d;b++)c=a.children[b],c.hasAttribute("data-details-open-display")||(c.style.display?c.setAttribute("data-details-open-display",c.style.display):c.setAttribute("data-details-open-display","")),c.style.display="none"}function p(a){var b=l(a.target);null===b||13!==a.keyCode&&32!==a.keyCode||(a.preventDefault(),m(b))}function n(a){a=l(a.target);null!==a&&m(a)}"open"in document.createElement("details")||(Object.defineProperty(h.Element.prototype,"open",{get:function(){return"nodeName"in this&&"DETAILS"==this.nodeName.toUpperCase()?this.hasAttribute("open"):void 0},set:function(a){"nodeName"in this&&"DETAILS"==this.nodeName.toUpperCase()&&(a?this.setAttribute("open","open"):this.removeAttribute("open"),g(this))}}),"complete"!==document.readyState?document.addEventListener("DOMContentLoaded",f,!1):f())})(this);
</script>
<script>
	/*******************************
	 * Script for resolution table *
	 *******************************/
	// DOM elements
	var dom_screen_dimensions = document.getElementById('screen-dimensions');
	var dom_screen_avail_dimensions = document.getElementById('screen-avail-dimensions');
	var dom_window_outer_dimensions = document.getElementById('window-outer-dimensions');
	var dom_window_inner_dimensions = document.getElementById('window-inner-dimensions');
	var dom_html_offset_dimensions = document.getElementById('html-offset-dimensions');
	var dom_html_client_dimensions = document.getElementById('html-client-dimensions');
	var dom_body_offset_dimensions = document.getElementById('body-offset-dimensions');	
	var dom_body_client_dimensions = document.getElementById('body-client-dimensions');
	var dom_site_offset_dimensions = document.getElementById('site-offset-dimensions');	
	var dom_site_client_dimensions = document.getElementById('site-client-dimensions');
	var dom_site_scroll_dimensions = document.getElementById('site-scroll-dimensions');
	var dom_site = document.getElementById('site');

	showDimensions();
	window.addEventListener('resize', showDimensions);

	function showDimensions() {
		dom_screen_dimensions.innerHTML = '<b> ' + window.screen.width + ' x ' + window.screen.height + '</b>';
		dom_screen_avail_dimensions.innerHTML = '<b> ' + window.screen.availWidth + ' x ' + window.screen.availHeight + '</b>';
		dom_window_outer_dimensions.innerHTML = '<b> ' + window.outerWidth + ' x ' + window.outerHeight + '</b>';
		dom_window_inner_dimensions.innerHTML = '<b> ' + window.innerWidth + ' x ' + window.innerHeight + '</b>';
		dom_html_offset_dimensions.innerHTML = '<b> ' + document.documentElement.offsetWidth + ' x ' + document.documentElement.offsetHeight + '</b>';
		dom_html_client_dimensions.innerHTML = '<b> ' + document.documentElement.clientWidth + ' x ' + document.documentElement.clientHeight + '</b>';
		dom_body_offset_dimensions.innerHTML = '<b> ' + document.body.offsetWidth + ' x ' + document.body.offsetHeight + '</b>';
		dom_body_client_dimensions.innerHTML = '<b> ' + document.body.clientWidth + ' x ' + document.body.clientHeight + '</b>';
		dom_site_offset_dimensions.innerHTML = '<b> ' + dom_site.offsetWidth + ' x ' + dom_site.offsetHeight + '</b>';
		dom_site_client_dimensions.innerHTML = '<b> ' + dom_site.clientWidth + ' x ' + dom_site.clientHeight + '</b>';
		dom_site_scroll_dimensions.innerHTML = '<b> ' + dom_site.scrollWidth + ' x ' + dom_site.scrollHeight + '</b>';
	} // showDimensions()
</script>
