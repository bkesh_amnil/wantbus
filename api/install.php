<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Installation application script.
 * - simple user interface to load the data stores with initial data
 * 
 * Invoked through the world wide web. Remove from public access once installation is complete.
 *
 * @input: $_POST['action'], optionally other stuff like $_FILE['file'] depending on action type. Each action should be one function.
 * @output: $result - string
 ***/
try {
	require __DIR__.'/init.php';

	// Arg check
	if (!isset($_POST['action'])) {
		$result = '';
	}
	else {
		$result = $_POST['action']();
	}
} //try
catch (Exception $e) {
	$result = '<br><br>Error: "' . $e->getMessage();
	$result .= '<br><br>--- Start Trace ---<br>';
	$result .= $e->getTraceAsString().'<br>';
	$result .= '---- End Trace ----<br>';
} //catch

/*** 
 * Drops all tables and starts a fresh copy of the DB.
 * Automatically loads data from [install]/[data-sql] folder
 * - Note: data is expected to be CSV
 * @input: $_POST['schema-file-name']
 * @return: {string} result to print
 ***/
function recreateSQLDatabase() {
	$return = '';

	// Read from Schema file
	if (!isset($_POST['schema-file-name']) || strlen($_POST['schema-file-name']) === 0) {
		return '[ERROR] Schema file not specified.';
	}
	$file_path = API_ROOT. '/install/' .$_POST['schema-file-name'];
	if (!file_exists($file_path)) {
		return '[ERROR] ' . $_POST['schema-file-name'] . ' does not exist.<br>';
	}
	$sql_template = file_get_contents($file_path);

	// Render template to populate table settings
	$sql = renderTemplate($sql_template, array(
		'table-prefix' => MYSQL_TABLE_PREFIX,
		'engine' => MYSQL_ENGINE,
		'charset' => 'utf8',
		'collate' => 'utf8_unicode_ci'
	));

	// Remove comments
	$sql = preg_replace('!/\*.*?\*/!s', '', $sql);
	$sql = preg_replace('/\n\s*\n/', "\n", $sql);

	// Init arrays to store all queries to execute
	$create_queries = array();
	$create_table_names = array();

	// Find all queries
	$tokens = explode(';', $sql);
	foreach ($tokens as $i => $q) {
		$q = trim($q);

		// Ignore empty queries
		if (strlen($q) === 0) {
			continue;
		}

		// Ignore poorly formed queries
		if (startsWith(strtoupper($q), 'CREATE TABLE')) {
			// Find table name of this query
			if (preg_match('/\`(.+?)\`/', $q, $group)) {
				$table_name = $group[1];
			}

			// Form and store queries to execute later
			array_push($create_queries, $q);
			array_push($create_table_names, $table_name);	
		}
		else {
			$return .= '[WARNING] Ignored unexpected query (Length: '. strlen($q) .'):<br>'.
			'----------------------------<br>'.
			$q.'<br>'.
			'----------------------------<br>';
		}
	}

	// Drop and recreate existing database
	$start_time = getTimeInMs();
	$return .= '----- Recreating SQL database -----<br>';
	$pdo = new PDO('mysql:host='.MYSQL_HOST,  // Use a separate PDO because we need to recreate the Database
		MYSQL_USER,
		MYSQL_PASS,
		array( // set error mode, set charset
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"
		)); 
	$stmt = $pdo->query('SELECT COUNT(*) FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = "'.MYSQL_DB_NAME. '"');
	$exists = (bool) $stmt->fetchColumn();
	if ($exists) {
		$result = $pdo->query('DROP DATABASE IF EXISTS '.MYSQL_DB_NAME);
		$return .= 'Dropped Database '. MYSQL_DB_NAME . '<br>';
	}
	else {
		$return .= 'Database ' . MYSQL_DB_NAME .' does not exist<br>';
	}
	$result =$pdo->query('CREATE DATABASE '.MYSQL_DB_NAME);
	$return .= 'Created Database '. MYSQL_DB_NAME.'<br>';
	$return .= ' (Done in: ' . (getTimeInMs() - $start_time) . 'ms)<br>';
	$return .= '<br>';
	
	// Create tables
	$start_time = getTimeInMs();
	$return .= '----- Creating SQL tables -----<br>';
	foreach ($create_queries as $i => $q) {
		//Log::debug('install.php recreateSQLDatabase() - Query: '. $q);
		$result = Database::query($q);
		if ($result === FALSE) {
			$return .= '[FAILURE] Unable to create SQL Table '.$create_table_names[$i].': DB query failure<br>';
		}
		else {
			$return .= 'SQL Table created: '.$create_table_names[$i].'<br>';
		}
	} // foreach create query
	$return .= ' (Done in: ' . (getTimeInMs() - $start_time) . 'ms)<br>';
	$return .= '<br>';

	// Load SQL data
	$start_time = getTimeInMs();
	$return .= '----- Loading SQL data -----<br>';
	foreach ($create_table_names as $i => $table_name) {
		// Check if we have CSV to load
		$class = substr($table_name, strlen(MYSQL_TABLE_PREFIX));
		$data_file_path = API_ROOT. '/install/data-sql/' . $class . '.csv';
		if (!file_exists($data_file_path)) {
			continue;
		}

		$return .= parseDataFileToInsertSQLData($data_file_path, $class, $start_time);
	} // for each query
	$return .= ' (Done in: ' . (getTimeInMs() - $start_time) . 'ms)<br>';
	$return .= '<br>';

	$return .= ' ----- All Done. ----- <br>';
	return $return;
} //recreateSQLDatabase()

/**
 * Loads data into the SQL DB. Only used to load additional data on top of those already auto-loaded during creation
 * @input: $_FILES['file']
 *         $_POST['class-name']
 */
function loadSQLData() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}

	$class = $_POST['class-name'];
	if (!class_exists($class)) {
		return $return.'Class "'.$class. '" does not exist.';
	}

	$start_time = getTimeInMs();
	$return .= '----- Loading SQL data -----<br>';
	$return .= parseDataFileToInsertSQLData($_FILES['file']['tmp_name'], $class, $start_time);
	$return .= ' (Done in: ' . (getTimeInMs() - $start_time) . 'ms)<br>';
	$return .= '<br>';

	$return .= ' ----- All Done. ----- <br>';
	return $return;
} //loadSQLData()

/**
 * Common procedure called by recreateSQLDatabase() and loadSQLData()
 * @param: {string} data_file_path - path to data file
 *         {string} class - class name of object to create
 *         {int} start_time
 * @return: {string} human-readable results
 */
function parseDataFileToInsertSQLData($data_file_path, $class, $start_time) {
	// Init counters
	$count = 0;
	$failed = 0;
	$failedNames = '';

	//Parse each line
	unset($headers);
	$lines = file($data_file_path);
	foreach ($lines as $line_number => $line) {
		// Ignore comment lines and empty lines
		if (startsWith($line, '//') || trim($line) === '') {
			continue;
		}

		// Parse this line (make empty values NULL)
		$arr = str_getcsv($line);
		foreach ($arr as $j => $val) {
			$arr[$j] = trim($val);
			if (strlen($arr[$j]) === 0) {
				$arr[$j] = NULL;
			}
		}

		// First row is headers
		if (!isset($headers)) {
			$headers = $arr;
			continue;
		}

		// Form object
		$data = array(
			'createdTime' => $start_time,
			'lastUpdateTime' => $start_time
		);
		foreach ($headers as $j => $key) {
			$data[$key] = $arr[$j];
		}

		// Create object
		try {
			$object = new $class($data);
			Database::create($object);
		}
		catch (Exception $e) {
			$failed++;
			Log::error('install.php recreateSQLDatabase() - Error while loading data for ' . $class . '.Line ' . $line_number . ' Exception: '.$e->getMessage());
			$failedNames .= '    - Line ' . $line_number . ' Exception: '.$e->getMessage().' <br>';
			continue;
		}
		$count++;
	} // foreach line

	// Return with count
	$total = $count + $failed;
	$return = 'Loaded: '.$count.' / '.$total.' rows for ' . $class . '.<br>';
	if ($failed > 0) {
		$return .= '  - FAILED '.$failed.' / '.$total. ' rows for ' . $class . ' not loaded<br>' . $failedNames;
	}

	return $return;
} //parseDataFileToInsertSQLData()

/*** 
 * Drops all tables and starts a fresh copy of the DB
 * @input: $_POST['schema-file-name'] specifying schema
 * @return: {string} result to print
 ***/
function recreateCQLDatastore() {
	$return = '';

	// Read from Schema file
	if (!isset($_POST['schema-file-name']) || strlen($_POST['schema-file-name']) === 0) {
		return '[ERROR] Schema file not specified.';
	}
	$file_path = API_ROOT. '/install/' .$_POST['schema-file-name'];
	if (!file_exists($file_path)) {
		return '[ERROR] ' . $_POST['schema-file-name'] . ' does not exist.<br>';
	}
	$cql_template = file_get_contents($file_path);

	// Render template to populate table settings
	$cql = renderTemplate($cql_template, array(
		'table-prefix' => CASSANDRA_TABLE_PREFIX,
	));

	// Remove comments
	$cql = preg_replace('!/\*.*?\*/!s', '', $cql);
	$cql = preg_replace('/\n\s*\n/', "\n", $cql);

	// Init arrays to store all queries to execute
	$create_queries = array();
	$create_table_names = array();
	$index_queries = array();
	$index_table_names = array();
	$index_column_names = array();

	// Find all queries
	$tokens = explode(';', $cql);
	foreach ($tokens as $i => $q) {
		$q = trim($q);

		// Ignore empty queries
		if (strlen($q) === 0) {
			continue;
		}

		// Ignore poorly formed queries
		if (startsWith(strtoupper($q), 'CREATE TABLE')) {
			// Find table name of this query
			if (preg_match('/CREATE TABLE (.+?)\(/i', $q, $group)) {
				$table_name = $group[1];
			}
			else {
				$table_name = '-- unable to parse --';
			}

			// Form and store queries to execute later
			array_push($create_queries, $q);
			array_push($create_table_names, $table_name);
		}
		else if (startsWith(strtoupper($q), 'CREATE INDEX ON')) {
			// Find table name of this query
			if (preg_match('/CREATE INDEX ON (.+?)\(/i', $q, $group)) {
				$table_name = trim($group[1]);
			}
			else {
				$table_name = '-- unable to parse --';
			}

			// Find index column name of this query
			if (preg_match('/\((.+?)\)/', $q, $group)) {
				$column_name = trim($group[1]);
			}
			else {
				$column_name = '-- unable to parse --';
			}

			// Form and store queries to execute later
			array_push($index_queries, $q);
			array_push($index_table_names, $table_name);
			array_push($index_column_names, $column_name);
		}
		else {
			$return .= '[WARNING] Ignored unexpected query (Length: '. strlen($q) .'):<br>'.
			'----------------------------<br>'.
			$q.'<br>'.
			'----------------------------<br>';
		}			
	}

	// Drop existing keyspace
	$start_time = getTimeInMs();
	$return .= '----- Recreating CQL keyspace -----<br>';
	$cluster = Cassandra::cluster() // Use a separate handler because we need to recreate the Database
		->withContactPoints(CASSANDRA_HOST)
		->withPort(CASSANDRA_PORT)
		->build();
	$session = $cluster->connect();
	$session->execute(new Cassandra\SimpleStatement('DROP KEYSPACE IF EXISTS ' . CASSANDRA_KEYSPACE));
	$session->execute(new Cassandra\SimpleStatement('CREATE KEYSPACE ' . CASSANDRA_KEYSPACE . ' WITH REPLICATION = {\'class\' : \'SimpleStrategy\', \'replication_factor\' : 1 }'));
	$return .= ' (Done in: ' . (getTimeInMs() - $start_time) . 'ms)<br>';
	$return .= '<br>';

	// Create tables
	$start_time = getTimeInMs();
	$return .= '----- Creating CQL tables -----<br>';
	foreach ($create_queries as $i => $q) {
		$result = Datastore::query($q);
		if ($result === FALSE) {
			$return .= '[FAILURE] Unable to create CQL Table '.$create_table_names[$i].': DB query failure<br>';
		}
		else {
			$return .= 'CQL Table created: '.$create_table_names[$i].'<br>';
		}
	}
	$return .= ' (Done in: ' . (getTimeInMs() - $start_time) . 'ms)<br>';
	$return .= '<br>';

	// Create secondary indices
	$start_time = getTimeInMs();
	$return .= '----- Creating CQL indices -----<br>';
	foreach ($index_queries as $i => $q) {
		$result = Datastore::query($q);
		if ($result === FALSE) {
			$return .= '[FAILURE] Unable to create index on column '. $index_column_names[$i] .' for CQL Table '.$index_table_names[$i].': DB query failure<br>';
		}
		else {
			$return .= 'CQL Index created - '.$index_table_names[$i] . ' ('. $index_column_names[$i] .')<br>';
		}
	}
	$return .= ' (Done in: ' . (getTimeInMs() - $start_time) . 'ms)<br>';
	$return .= '<br>';

	// Load CQL data
	$start_time = getTimeInMs();
	$return .= '----- Loading CQL data -----<br>';
	foreach ($create_table_names as $table_name) {
		$table_name_without_prefix = trim(substr($table_name, strlen(CASSANDRA_TABLE_PREFIX)));

		// Check if we have CSV doc to load
		$data_file_path = API_ROOT. '/install/data-cql/doc.' . $table_name_without_prefix . '.csv';
		if (file_exists($data_file_path)) {
			$return .= parseDataFileToInsertCQLData($data_file_path, $table_name_without_prefix, 'csv-doc', $start_time);
			continue;
		}

		// Check if we have CSV of img to load
		$data_file_path = API_ROOT. '/install/data-cql/img.' . $table_name_without_prefix . '.csv';
		if (file_exists($data_file_path)) {
			$return .= parseDataFileToInsertCQLData($data_file_path, $table_name_without_prefix, 'csv-img', $start_time);
			continue;
		}

		// Check if we have CSV to load
		$data_file_path = API_ROOT. '/install/data-cql/' . $table_name_without_prefix . '.csv';
		if (file_exists($data_file_path)) {
			$return .= parseDataFileToInsertCQLData($data_file_path, $table_name_without_prefix, 'csv', $start_time);
			continue;
		}

		// Check if we have json to load
		$data_file_path = API_ROOT. '/install/data-cql/' . $table_name_without_prefix . '.json';
		if (file_exists($data_file_path)) {
			$return .= parseDataFileToInsertCQLData($data_file_path, $table_name_without_prefix, 'json', $start_time);
			continue;
		}
	} // for each table name
	$datalist_file_paths = glob(API_ROOT. '/install/data-cql/datalist.*.json');
	foreach ($datalist_file_paths as $datalist_file_path) {
		$data_name = substr($datalist_file_path , strlen(API_ROOT. '/install/data-cql/datalist.'), -5); // remove dot extension (5 chars)
		//Log::debug('install.php recreateCQLDatastore() - found ' . $data_name);

		$return .= parseDataFileToInsertCQLData($datalist_file_path, $data_name, 'json-data-list', $start_time);
	}

	$return .= ' (Done in: ' . (getTimeInMs() - $start_time) . 'ms)<br>';
	$return .= '<br>';

	$return .= ' ----- All Done. ----- <br>';
	return $return;
} //recreateCQLDatastore()

/**
 * Loads data into the CQL DB. Only used to load additional data on top of those already auto-loaded during creation
 * @input: $_FILES['file']
 *         $_POST['table-name', 'file-format', 'data-list-name']
 */
function loadCQLData() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}

	if ($_POST['table-name'] === 'DataList') {
		$data_name = $_POST['data-list-name'];
	}
	else {
		$data_name = $_POST['table-name'];
	}
	$format = $_POST['file-format'];

	$start_time = getTimeInMs();
	$return .= '----- Loading CQL data -----<br>';
	$return .= parseDataFileToInsertCQLData($_FILES['file']['tmp_name'], $data_name, $format, $start_time);
	$return .= ' (Done in: ' . (getTimeInMs() - $start_time) . 'ms)<br>';
	$return .= '<br>';

	$return .= ' ----- All Done. ----- <br>';
	return $return;
} //loadCQLData()

/**
 * Common procedure called by recreateSQLDatabase() and loadSQLData()
 * @param: {string} data_file_path - path to data file
 *         {string} name - table name of objects to load, or data list name
 *         {string} format - ['json', 'json-data-list', 'csv', 'csv-img', 'csv-doc']
 *         {int} start_time
 * @return: {string} human-readable results
 */
function parseDataFileToInsertCQLData($data_file_path, $data_name, $format, $start_time) {
	$return = '';

	// Init counter
	$count = 0;
	$failed = 0;
	$failedNames = '';

	if ($format === 'json') {
		$table_name = $data_name;

		// Parse into array
		$contents = str_replace("\r\n", "\n", file_get_contents($data_file_path));
		$objects = json_decode($contents, TRUE);
		if (!is_array($objects)) {
			$return .= 'Error: unable to parse JSON file.<br>';
			return $return;
		}
		else {
			$return .= '  - ' . count($objects) .  ' ' . $data_name . ' objects parsed.<br>';
		}

		// Add object into table
		foreach ($objects as $j => $data) {
			$data['createdTime'] = $start_time;
			$data['lastUpdateTime'] = $start_time;
				
			try {
				Datastore::set($table_name, $data);	
			}
			catch (Exception $e) {
				$failed++;
				$failedNames .= '- Line ' . $j . '(DB Error: ' . $e->getMessage() . ')<br>';
				continue;
			}

			$count++;
		}
	} // json
	else if ($format === 'json-data-list') {
		$table_name = 'DataList';

		// Parse into JSON
		$contents = str_replace("\r\n", "\n", file_get_contents($data_file_path));
		$objects = json_decode($contents, TRUE);
		if (!is_array($objects)) {
			$return .= 'Error: unable to parse JSON file.<br>';
			return $return;
		}
		else {
			$return .= '  - ' . count($objects) .  ' ' . $data_name . ' objects parsed.<br>';
		}
		$json = json_encode($objects);
		if (!isset($objects)) {
			$return .= 'Error: unable to parse JSON file.<br>';
			return $return;
		}
		
		// Add datalist into table
		$data = array(
			'name' => $data_name,
			'json' => $json,
			'createdTime' => $start_time,
			'lastUpdateTime' => $start_time
		);
		try {
			Datastore::set($table_name, $data);
		}
		catch (Exception $e) {
			$failed++;
			$failedNames .= '(DB Error: ' . $e->getMessage() . ')<br>';
		}

		$count++;
	} // json data list
	else if ($format === 'csv') {
		$table_name = $data_name;

		//Parse each line
		$lines = file($data_file_path);
		foreach ($lines as $i => $line) {
			// Ignore comment lines and empty lines
			if (startsWith($line, '//') || trim($line) === '') {
				continue;
			}

			// Parse this line (make empty values NULL)
			$arr = str_getcsv($line);
			foreach ($arr as $j => $val) {
				$arr[$j] = trim($val);
				if (strlen($arr[$j]) === 0) {
					$arr[$j] = NULL;
				}
			}

			// First row is headers
			if (!isset($headers)) {
				$headers = $arr;
				continue;
			}

			// Form object
			$data = array();
			foreach ($headers as $j => $key) {
				$data[$key] = $arr[$j];

				$data['createdTime'] = $start_time;
				$data['lastUpdateTime'] = $start_time;
			}
			try {
				Datastore::set($table_name, $data);
			}
			catch (Exception $e) {
				$failed++;
				$failedNames .= '- Line ' . $i . '(DB Error: ' . $e->getMessage() . ')<br>';
				continue;
			}

			$count++;
		}  // foreach line
	} // csv
	else if ($format === 'csv-img') {
		$table_name = $data_name;

		//Parse each line
		$lines = file($data_file_path);
		foreach ($lines as $i => $line) {
			// Ignore comment lines and empty lines
			if (startsWith($line, '//') || trim($line) === '') {
				continue;
			}

			// Parse this line (make empty values NULL)
			$arr = str_getcsv($line);
			foreach ($arr as $j => $val) {
				$arr[$j] = trim($val);
				if (strlen($arr[$j]) === 0) {
					$arr[$j] = NULL;
				}
			}

			// First row is headers
			if (!isset($headers)) {
				$headers = $arr;
				continue;
			}

			// Form object
			$data = array();
			foreach ($headers as $j => $key) {
				if (endsWith($key,'ID')) {
					$data[$key] = (int) $arr[$j]; // IDs must be casted to int 
				} 
				else {
					$data[$key] = $arr[$j];
				}

				// Also add blob, width, height
				if ($key === 'blob') {
					$blob = file_get_contents(dirname($data_file_path).'/img/' . $data[$key]);

					$size = getimagesizefromstring($blob);
					$data['blob'] = $blob;
					$data['width'] = $size[0];
					$data['height'] = $size[1];
				}

				$data['createdTime'] = $start_time;
				$data['lastUpdateTime'] = $start_time;
			}
			try {
				Datastore::set($table_name, $data);
			}
			catch (Exception $e) {
				$failed++;
				$failedNames .= '- Line ' . $i . '(DB Error: possible duplicate. Check log file.)<br>';
				continue;
			}

			$count++;
		}  // foreach line
	} // csv-img
	else if ($format === 'csv-doc') {
		$table_name = $data_name;

		//Parse each line
		$lines = file($data_file_path);
		foreach ($lines as $i => $line) {
			// Ignore comment lines and empty lines
			if (startsWith($line, '//') || trim($line) === '') {
				continue;
			}

			// Parse this line (make empty values NULL)
			$arr = str_getcsv($line);
			foreach ($arr as $j => $val) {
				$arr[$j] = trim($val);
				if (strlen($arr[$j]) === 0) {
					$arr[$j] = NULL;
				}
			}

			// First row is headers
			if (!isset($headers)) {
				$headers = $arr;
				continue;
			}

			// Form object
			$data = array();
			foreach ($headers as $j => $key) {
				if (endsWith($key,'ID')) {
					$data[$key] = (int) $arr[$j]; // IDs must be casted to int 
				} 
				else {
					$data[$key] = $arr[$j];
				}

				// Add blob
				if ($key === 'blob') {
					$doc_path = dirname($data_file_path).'/doc/' . $data[$key];
					$data['blob'] = file_get_contents($doc_path);
				}

				$data['createdTime'] = $start_time;
				$data['lastUpdateTime'] = $start_time;
			}
			try {
				Datastore::set($table_name, $data);
			}
			catch (Exception $e) {
				$failed++;
				$failedNames .= '- Line ' . $i . '(DB Error: possible duplicate. Check log file.)<br>';
				continue;
			}

			$count++;
		}  // foreach line
	} // csv-doc

	// Return with count
	$total = $count + $failed;
	$return .= 'Loaded: '.$count.' / '.$total.' rows for ' . $table_name . '<br>';
	if ($failed > 0) {
		$return .= 'FAILED '.$failed.' / '.$total.' rows for ' . $table_name . ' not loaded<br>' . $failedNames;
	}

	return $return;
} // parseDataFileToInsertCQLData()

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Installer: <?php echo PROJECT_NAME;?></title>
<link rel="stylesheet" href="assets/reset.css" />
<link rel="stylesheet" href="assets/common.css" />
</head>
<body>
<div id="site">
	<nav>
		<a href="/info.php">Info</a>
		<a href="/install.php" class="current">Installer</a>
		<a href="/test.php">Tester</a>
	</nav>
	<section>
		<details open="open">
			<summary><h2>Initialise Data stores</h2></summary>
			<form id="recreate-sql-tables-form" method="post" enctype="multipart/form-data">
				<h3>SQL Schema</h3>
				<input type="text" name="schema-file-name" value="schema.sql" />
				<button type="submit" name="action" value="recreateSQLDatabase">Recreate SQL Database</button>
			</form>
			<form id="recreate-cql-tables-form" method="post" enctype="multipart/form-data">
				<h3>CQL Schema</h3>
				<input type="text" name="schema-file-name" value="schema.cql" />
				<button type="submit" name="action" value="recreateCQLDatastore">Recreate CQL Datastore</button>
			</form>
			<br>

			<form id="load-sql-data" method="post" enctype="multipart/form-data">
				<h3>SQL Data</h3>
				<input type="file" name="file" id="load-sql-data-file">
				<select name="file-format" id="load-sql-data-file-format">
					<option value="csv">CSV</option>
				</select>
				<input type="text" name="class-name" id="load-sql-data-class-name" placeholder="Table (without <?php echo MYSQL_TABLE_PREFIX;?> prefix), e.g. Member" />
				<button type="submit" name="action" value="loadSQLData">Load SQL Data</button>
			</form>
			<form id="load-cql-data" method="post" enctype="multipart/form-data">
				<h3>CQL Data</h3>
				<input type="file" name="file" id="load-cql-data-file">
				<select name="file-format" id="load-cql-data-file-format">
					<option value="json">JSON</option>
					<option value="json-data-list">JSON Data List</option>
					<option value="csv">CSV</option>
					<option value="csv-img">CSV of Images</option>
					<option value="csv-doc">CSV of Docs</option>
				</select>
				<input type="text" name="table-name" id="load-cql-data-table-name" placeholder="Table (without <?php echo CASSANDRA_TABLE_PREFIX;?> prefix), e.g. Setting" />
				<input type="text" name="data-list-name" id="load-cql-data-data-list-name" placeholder="Data list name, e.g. countries" disabled="disabled" />
				<button type="submit" name="action" value="loadCQLData">Load CQL Data</button>
			</form>
		</details>
	
		<details open="open">
			<summary><h2>Result:</h2></summary>
			<button class="clear-result-button">Clear Results</button>
			<section id="result"><?php echo $result;?></section>
			<button class="clear-result-button">Clear Results</button>
		</details>
	</section>
</div>
<script>
	/* details-summary-polyfill-1.0.2.min.js */
	(function(h){function f(){function a(b){var c,d,e,f;d=b.length;if(0!==d)for(c=0;c<d;c++)e=b[c],f=e.nodeName.toUpperCase(),"DETAILS"===f&&k(e),a(e.childNodes)}var b,d;document.head.insertAdjacentHTML("afterbegin",'<br><style>details,summary{display: block;}details>summary:focus{outline:1px solid blue;}details>summary::before{content:"\u25ba";margin-right: 5px;}details[open]>summary::before{content:"\u25bc";}</style>');var c=document.getElementsByTagName("details"),e=document.getElementsByTagName("summary");b=0;for(d=c.length;b<d;b++)k(c[b]);b=0;for(d=e.length;b<d;b++)e[b].tabIndex="0";h.MutationObserver&&(new MutationObserver(function(b){b.forEach(function(b){a(b.addedNodes)})})).observe(document.body,{childList:!0,subtree:!0});document.body.addEventListener("click",n,!0);document.body.addEventListener("keydown",p,!0);document.removeEventListener("DOMContentLoaded",f,!1)}function l(a){for(var b=document.body;a!==b;){if("SUMMARY"===a.nodeName.toUpperCase())return a.parentElement;a=a.parentElement}return null}function k(a){for(var b,d=0,c=a.children.length;d<c;d++)if("SUMMARY"===a.children[d].nodeName.toUpperCase()){b=a.children[d];break}b||(b=document.createElement("summary"),b.innerHTML="Details");a.insertBefore(b,a.firstChild);b.tabIndex="0";g(a)}function m(a){a.hasAttribute("open")?a.removeAttribute("open"):a.setAttribute("open","open");g(a);var b=new Event("toggle");a.dispatchEvent(b)}function g(a){var b,d,c;if(a.hasAttribute("open"))for(b=1,d=a.children.length;b<d;b++){c=a.children[b];var e=c.getAttribute("data-details-open-display");c.style.display=e?e:""}else for(b=1,d=a.children.length;b<d;b++)c=a.children[b],c.hasAttribute("data-details-open-display")||(c.style.display?c.setAttribute("data-details-open-display",c.style.display):c.setAttribute("data-details-open-display","")),c.style.display="none"}function p(a){var b=l(a.target);null===b||13!==a.keyCode&&32!==a.keyCode||(a.preventDefault(),m(b))}function n(a){a=l(a.target);null!==a&&m(a)}"open"in document.createElement("details")||(Object.defineProperty(h.Element.prototype,"open",{get:function(){return"nodeName"in this&&"DETAILS"==this.nodeName.toUpperCase()?this.hasAttribute("open"):void 0},set:function(a){"nodeName"in this&&"DETAILS"==this.nodeName.toUpperCase()&&(a?this.setAttribute("open","open"):this.removeAttribute("open"),g(this))}}),"complete"!==document.readyState?document.addEventListener("DOMContentLoaded",f,!1):f())})(this);
</script>
<script>
	/***************************
	 * Script for data loaders *
	 ***************************/

	// DOM Elements
	var dom_load_sql_data_file = document.getElementById('load-sql-data-file');
	var dom_load_sql_data_class_name = document.getElementById('load-sql-data-class-name');
	var dom_load_sql_data_file_format = document.getElementById('load-sql-data-file-format');
	var dom_load_cql_data_file = document.getElementById('load-cql-data-file');
	var dom_load_cql_data_table_name = document.getElementById('load-cql-data-table-name');
	var dom_load_cql_data_data_list_name = document.getElementById('load-cql-data-data-list-name');
	var dom_load_cql_data_file_format = document.getElementById('load-cql-data-file-format');
	var dom_result = document.getElementById('result');
	var dom_clear_result_buttons = document.getElementsByClassName('clear-result-button');

	// Bind Clear Results Button
	for (var i=0;i<dom_clear_result_buttons.length; i++) {
		dom_clear_result_buttons[i].addEventListener('click', function(e) {
			dom_result.innerHTML = '';
		});
	}

	// Bind Form - SQL Data
	dom_load_sql_data_file.addEventListener('change', function(e) {
		var fullPath = dom_load_sql_data_file.value;
		if (fullPath) {
			var tmp = fullPath.split('.');
			var class_name = tmp[tmp.length - 2];
			dom_load_sql_data_class_name.value = class_name.charAt(0).toUpperCase() + class_name.slice(1);;
		}
		else {
			dom_load_sql_data_class_name.value = '';
		}
	});

	// Bind Form - CQL Data
	dom_load_cql_data_file.addEventListener('change', function(e) {
		var fullPath = dom_load_cql_data_file.value;
		if (fullPath) {
			var tmp = fullPath.split('.');
			if (tmp.length === 2) {
				var table_name = tmp[0].charAt(0).toUpperCase() + tmp[0].slice(1);
				var ext = tmp[1];
				dom_load_cql_data_table_name.value = table_name;
				dom_load_cql_data_file_format.value = ext.toLowerCase();
				dom_load_cql_data_data_list_name.disabled = true;
				dom_load_cql_data_data_list_name.value = '';
			} // 2 tokens: {table} . {format}
			else if (tmp.length === 3) {
				if (tmp[0] === 'datalist') {
					var list_name = tmp[1];
					dom_load_cql_data_file_format.value = 'json-data-list';
					dom_load_cql_data_table_name.value = 'DataList';
					dom_load_cql_data_data_list_name.disabled = false;
					dom_load_cql_data_data_list_name.value = list_name;
				}
				else if (tmp[0] === 'img') {
					var table_name = tmp[1].charAt(0).toUpperCase() + tmp[1].slice(1);
					dom_load_cql_data_file_format.value = 'csv-img';
					dom_load_cql_data_table_name.value = table_name;
					dom_load_cql_data_data_list_name.disabled = true;
					dom_load_cql_data_data_list_name.value = '';
				}
				else if (tmp[0] === 'doc') {
					var table_name = tmp[1].charAt(0).toUpperCase() + tmp[1].slice(1);
					dom_load_cql_data_file_format.value = 'csv-doc';
					dom_load_cql_data_table_name.value = table_name;
					dom_load_cql_data_data_list_name.disabled = true;
					dom_load_cql_data_data_list_name.value = '';	
				}
			} // 3 tokens: datalist or img
		}
		else {
			dom_load_cql_data_table_name.value = '';
			dom_load_cql_data_file_format.value = 'csv';
		}
	});

	dom_load_cql_data_file_format.addEventListener('change', function(e) {
		if (dom_load_cql_data_file_format.value === 'json-data-list') {
			dom_load_cql_data_data_list_name.disabled =false;
			dom_load_cql_data_table_name.value = 'DataList';
		}
		else {
			dom_load_cql_data_data_list_name.disabled =true;
		}
	});
</script>
</body>
</html>
