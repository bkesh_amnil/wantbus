<?php
/****
 * Backend Framework v2.1.0 (Edited)
 * ========================
 *
 * Script that automatically runs in the background periodically. Useful for:
 * - stale data cleanups
 * - file refreshes (from datastore)
 * - error checks
 * - system health notifications to webmaster
 *
 * Invoked by the OS cron service
 * - to be set up in the user's crontab
 ***/
try {
	require __DIR__ . '/init.php';

	// Include classes that will definitely be used
	require API_ROOT.'/classes/HTTPResponse.php';
	require API_ROOT.'/classes/persistence/Log.php';

	Log::info('cron.php - starting...');

	$stats_text = '';

	// Tasks: refresh files on this server
	$domain_labels = array(
		'www'
	);
	$refresh_methods = array(
		'InternalAPI::attemptRefreshHTMLPages',
		'InternalAPI::attemptRefreshDocuments',
		'InternalAPI::attemptRefreshImages',
		'InternalAPI::attemptRefreshDatalists',
		'InternalAPI::attemptRefreshTherapyImages',
		'InternalAPI::attemptRefreshCategoryImages',
		'InternalAPI::attemptRefreshArticleImages',
		'InternalAPI::attemptRefreshAccoladeImages',
		'InternalAPI::attemptRefreshAccoladeDocuments'
	);

	// Refresh files for each sub-domain
	foreach ($domain_labels as $domain_label) {
		$stats_text .= '- ' . $domain_label . ":\n";

		// Go through each refresh method
		foreach ($refresh_methods as $method) {
			$results = call_user_func($method, $domain_label);
			if (!$results['is-refreshed']) {
				continue;
			}

			$stats_text .= '  • '.$method."\n";
			$created_count = count($results['files-created']);
			$updated_count = count($results['files-updated']);
			$deleted_count = count($results['files-deleted']);
			if ($created_count) {
				$stats_text .= "    - " . $created_count . " files created: " . array2csv($results['files-created']) . "\n";
			}
			if ($updated_count) {
				$stats_text .= "    - " . $updated_count . " files updated: " . array2csv($results['files-updated']) . "\n";
			}
			if ($deleted_count) {
				$stats_text .= "    - " . $deleted_count . " files deleted: " . array2csv($results['files-deleted']) . "\n";
			}
		} // for each refresh method
	} // for each domain

	Log::info('cron.php - completed.'."\n".$stats_text);
}
catch (Exception $e) {
	Log::fatal('cron.php error - ' . $e->getMessage() . "\n" . $e->getTraceAsString());

	// Return error and terminate
	$response = new HTTPResponse(500);
	$response->setData(array('error' => $e->getMessage()));
	$response->deliver();
	exit(1);
} //catch
